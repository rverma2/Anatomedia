DELIMITER $$

USE `anatomedia_test`$$

DROP TRIGGER /*!50032 IF EXISTS */ `insert_anatcustomer`$$

CREATE
    /*!50017 DEFINER = 'srtuser'@'localhost' */
    TRIGGER `insert_anatcustomer` AFTER INSERT ON `customer` 
    FOR EACH ROW BEGIN
 CALL AnatomediaSRT.Anatimmediatecustomersync(new.id,"N");
    END;
$$

DELIMITER ;