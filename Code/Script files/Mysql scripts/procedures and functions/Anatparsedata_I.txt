DELIMITER $$

USE `AnatomediaSRT`$$

DROP PROCEDURE IF EXISTS `Anatparsedata_I`$$

CREATE DEFINER=`srtuser`@`localhost` PROCEDURE `Anatparsedata_I`(executionid INT,rowcount INT,curfiledate DATETIME, OUT parsecounter INT)
BEGIN
DECLARE rawtablecount INT;
DECLARE parsetablecount INT;
SELECT COUNT(1) INTO rawtablecount FROM AnatomediaSRT.Anatdailyrawlogs;
IF rawtablecount = rowcount
THEN
UPDATE AnatomediaSRT.Anatexecutionhistory SET currentprocess="parsing"  WHERE executionhistoryid = executionid;
SELECT COUNT(1) INTO parsetablecount FROM AnatomediaSRT.Anatparsedlogdata WHERE DATE(logdate) = DATE(curfiledate);
IF parsetablecount <> rawtablecount
THEN
DELETE FROM AnatomediaSRT.Anatparsedlogdata WHERE DATE(logdate) = DATE(curfiledate);
INSERT INTO AnatomediaSRT.Anatparsedlogdata (ipaddress,logdate,requeststring,calledFrom,webbrowser,CODE,size,customername,sessionid) SELECT LEFT( rawlogdata,INSTR(rawlogdata,' ')) AS "IPAddress",STR_TO_DATE(CONCAT(SUBSTRING( rawlogdata,INSTR(rawlogdata,'[')+1,11),' ',SUBSTRING(rawlogdata,INSTR(rawlogdata,'+')-9,8)), '%d/%b/%Y %H:%i:%s') AS "MediaLogDate",SUBSTRING_INDEX(SUBSTRING_INDEX(rawlogdata, '"', 2),'"',-1) AS "RequestString",SUBSTRING_INDEX(SUBSTRING_INDEX(rawlogdata, '"', 4),'"',-1) AS "calledfrom",SUBSTRING_INDEX(SUBSTRING_INDEX(rawlogdata, '"', 6),'"',-1) AS "webbrowser",SUBSTRING_INDEX(TRIM(SUBSTRING_INDEX(SUBSTRING_INDEX(rawlogdata, '"', 3),'"',-1)),' ',1) AS "code" ,SUBSTRING_INDEX(TRIM(SUBSTRING_INDEX(SUBSTRING_INDEX(rawlogdata, '"', 3),'"',-1)),' ',-1) AS "size",REPLACE(SUBSTRING_INDEX(SUBSTRING_INDEX(`rawlogdata`, '"', 8),'"',-1),"+"," ") AS "customername", SUBSTRING_INDEX(SUBSTRING_INDEX(`rawlogdata`, '"', 10),'"',-1) "sessionid" FROM AnatomediaSRT.Anatdailyrawlogs;
SET parsecounter = 1;
END IF;
-- SELECT COUNT(1) INTO parsetablecount FROM AnatomediaSRT.Anatparsedlogdata where DATE(logdate) = DATE(curfiledate);
-- IF rawtablecount = parsetablecount
-- then
-- set parsecounter = 1;
-- else
-- delete from AnatomediaSRT.Anatparsedlogdata WHERE DATE(logdate) = DATE(curfiledate);
--  set parsecounter = 0;
-- end if;
ELSE
SET parsecounter = 0;
END IF;
END$$

DELIMITER ;