DELIMITER $$

USE `AnatomediaSRT`$$

DROP PROCEDURE IF EXISTS `AnatGetCustomerGroup_S`$$

CREATE DEFINER=`srtuser`@`localhost` PROCEDURE `AnatGetCustomerGroup_S`()
BEGIN
SELECT customergroupid,customergroup FROM AnatomediaSRT.AnatCustomerGroup 
UNION ALL
SELECT 0, 'ALL' ORDER BY customergroupid,customergroup;
END$$

DELIMITER ;