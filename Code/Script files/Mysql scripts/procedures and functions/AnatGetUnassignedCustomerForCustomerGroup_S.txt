DELIMITER $$

USE `AnatomediaSRT`$$

DROP PROCEDURE IF EXISTS `AnatGetUnassignedCustomerForCustomerGroup_S`$$

CREATE DEFINER=`srtuser`@`localhost` PROCEDURE `AnatGetUnassignedCustomerForCustomerGroup_S`(CustomerGroupId INT)
SELECT DISTINCT c.id,c.customername FROM AnatomediaSRT.AnatCustomer c LEFT JOIN AnatomediaSRT.AnatCustomerGroupCustomerMap cm ON cm.CustomerGroupID= CustomerGroupId AND  c.id = cm.customerid  WHERE CustomerStatus='Active' and cm.customerid IS NULL ORDER BY c.customername$$

DELIMITER ;