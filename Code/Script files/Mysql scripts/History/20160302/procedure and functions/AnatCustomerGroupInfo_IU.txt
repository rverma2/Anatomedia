DELIMITER $$

USE `AnatomediaSRT`$$

DROP PROCEDURE IF EXISTS `AnatCustomerGroupInfo_IU`$$

CREATE DEFINER=`srtuser`@`localhost` PROCEDURE `AnatCustomerGroupInfo_IU`(customergroupname VARCHAR(1000),custgroupid INT(10),customersid VARCHAR(100),flag CHAR(1))
BEGIN
DECLARE groupid INT(10);
DECLARE sDelimiter VARCHAR(5);
DECLARE sItem VARCHAR(200);
DECLARE rtrnval VARCHAR(200);
SET sDelimiter =',';
SET rtrnval ="";
CREATE TEMPORARY TABLE IF NOT EXISTS tmpcustomerid (id INT(10));
CREATE TEMPORARY TABLE IF NOT EXISTS tmpgroupmapping(tmpcustomerid INT(10),mapcustomerid INT(10),customergroupid INT (10));
 
WHILE INSTR(customersid,sDelimiter) <> 0 DO
	SET sItem=TRIM(SUBSTRING(customersid,1,INSTR(customersid,sDelimiter)-1));
	SET customersid=TRIM(SUBSTRING(customersid,INSTR(customersid,sDelimiter)+LENGTH(sDelimiter),LENGTH(customersid)));
  IF LENGTH(sItem) > 0 THEN
	INSERT INTO tmpcustomerid VALUES(sItem);
  END IF;
 END WHILE;
 IF LENGTH(customersid) > 0 THEN 
	INSERT INTO tmpcustomerid VALUES(customersid);
 END IF;
IF UPPER(flag) = "U" THEN
SET groupid = custgroupid;
	INSERT INTO tmpgroupmapping (tmpcustomerid,mapcustomerid,customergroupid) SELECT t.id,cm.customerid,groupid FROM tmpcustomerid t LEFT JOIN AnatCustomerGroupCustomerMap cm ON t.id = cm.customerid AND cm.customergroupid =groupid WHERE cm.customerid IS NULL;
	INSERT INTO tmpgroupmapping (tmpcustomerid,mapcustomerid,customergroupid) SELECT rt.id ,cm.customerid,groupid FROM tmpcustomerid rt RIGHT OUTER JOIN AnatCustomerGroupCustomerMap cm ON rt.id = cm.customerid WHERE cm.customergroupid =groupid AND rt.id IS NULL;
	IF EXISTS (SELECT 1 FROM tmpgroupmapping)
	THEN
	INSERT INTO AnatCustomerGroupCustomerMap (customergroupid,customerid) SELECT customergroupid,tmpcustomerid FROM tmpgroupmapping WHERE mapcustomerid IS NULL;
	DELETE m FROM AnatCustomerGroupCustomerMap m INNER JOIN tmpgroupmapping tm ON m.customerid =tm.mapcustomerid WHERE m.customergroupid =groupid AND tm.tmpcustomerid IS NULL;
	UPDATE AnatomediaSRT.AnatCustomerGroup SET CustomerGroupModifiedDate = CURRENT_TIMESTAMP WHERE customergroupid =custgroupid;
	SELECT * FROM tmpgroupmapping;
	END IF;
	IF EXISTS (SELECT 1 FROM AnatomediaSRT.AnatCustomerGroup WHERE customergroupid = custgroupid AND UPPER(customergroup) <> UPPER(customergroupname))
	THEN
	UPDATE AnatomediaSRT.AnatCustomerGroup SET customergroup = customergroupname,CustomerGroupModifiedDate = CURRENT_TIMESTAMP WHERE customergroupid =custgroupid;
	END IF;
	
ELSEIF UPPER(flag) = "I" THEN
	INSERT INTO AnatomediaSRT.AnatCustomerGroup (customergroup) VALUES (customergroupname);
	SELECT LAST_INSERT_ID() INTO groupid;
	INSERT INTO AnatomediaSRT.AnatCustomerGroupCustomerMap (customergroupid,customerid) SELECT groupid,id FROM tmpcustomerid;
END IF;	
DROP TABLE tmpcustomerid;
DROP TABLE tmpgroupmapping;
	
END$$

DELIMITER ;