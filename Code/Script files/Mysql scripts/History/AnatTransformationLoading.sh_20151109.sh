#!/bin/bash
curdate=$(date +"%Y%m%d")
filepath="/home/sushant/"
fileinitname="anatlog"
filepathlen=21
firstfileprocess=""
lastsuccessfileprocessed=""
curfileprocess=""
curfileprocessdt=""
nextfiletoprocessdt=""
ctrlfilewrittencount=""
executionid=""
lastfileprocessed=$(mysql -se "SELECT fileprocessed FROM AnatomediaSRT.Anatexecutionhistory WHERE executionstatus = 's' ORDER BY executionenddt DESC LIMIT 1;")

#if [ $? = 1 ]
#then
#echo "error"
#else
#echo "no error"
#fi

controlfiletask () {
     controlfilename=$filepath"control"$2."txt"
     if [ -f $controlfilename ]
     then
	counter=""
	rowsread=`awk '{print $2}' $controlfilename`
	rowswritten=`awk '{print $4}' $controlfilename`
	ctrlstatus=`awk '{print $6}' $controlfilename`
	filename=`awk '{print $8}' $controlfilename`
	comments=`awk '{print $10}' $controlfilename`
	if mysql -e "CALL AnatomediaSRT.Anatcontrolfileinfo_I(1,'$controlfilename',$rowsread,$rowswritten,$ctrlstatus,$filename,$comments,'$curfileprocess',@counter);"	
	then 
		mysql -e "LOAD DATA LOCAL INFILE '$file' INTO TABLE AnatomediaSRT.Anatdailyrawlogs FIELDS ENCLOSED BY '' TERMINATED BY '' (rawlogdata);"
		mysqlwarnings=$(mysql -e "show warnings;")
		mysqlerrors=$(mysql -e "show errors;")
		if [ "$mysqlwarnings" = "" ] || [ "$mysqlerrors" = "" ]
		then
			executionid=$(mysql -se "SELECT MAX(executionhistoryid) FROM AnatomediaSRT.Anatexecutionhistory WHERE fileprocessed = '$curfileprocess';")
			if mysql -e "CALL AnatomediaSRT.Anatparsedata_I($executionid,$rowswritten,'$curfileprocessdt',@parsecounter);"
			then
				if mysql -e "CALL AnatomediaSRT.AnatDailyInfo_I($executionid,'$curfileprocessdt');"
				then
					return 1
				else
					return 0
				fi
			else
				return 0
			fi
		else
			mysql -e "update AnatomediaSRT.Anatexecutionhistory set executionstatus=\"f\" where fileprocessed='$curfileprocess';"
			return 0
		fi
	else
		return 0
	fi
     else
	echo "no file found $controlfilename"

	mysql -e "INSERT INTO AnatomediaSRT.Anatexecutionhistory(FileProcessed,currentprocess,comments,executionenddt,executionstatus) VALUES('$curfileprocess','raw_data','no control file found. $controlfilename',CURRENT_TIMESTAMP,'f');"
#	mysql -e "update AnatomediaSRT.Anatexecutionhistory set comments='no control file found. $controlfilename'  where fileprocessed='$curfileprocess';"
	return 0
     fi
}



for file in $filepath$fileinitname*
do	
	curfileprocess=$file
	curfileprocessdt=${file:$filepathlen}
	if [ "$lastfileprocessed" = ""  ]
	then 
		if  [ ${file:$filepathlen} -lt $curdate ]
		then
			if [[ "$firstfileprocess" = "" ]] || [[ "$nextfiletoprocessdt" = ${file:$filepathlen} ]]
			then
				controlfiletask 1 ${file:$filepathlen}
				returnval=$?
				[ $returnval = 0 ] && break
				
				echo "file processed ${file:$filepathlen}"

				firstfileprocess=$file
				nextfiletoprocessdt=$(date --date="${file:$filepathlen} + 1 day" +"%Y%m%d")
			else
				break
			fi
		fi
	else
		#echo $lastfileprocessed
		if [ ${file:$filepathlen} -le ${lastfileprocessed:$filepathlen} ]; then
		continue
		fi


		if [ "$nextfiletoprocessdt" = "" ]
                then
                        nextfiletoprocessdt=$(date --date="${lastfileprocessed:$filepathlen} + 1 day" +"%Y%m%d")
                else
                        nextfiletoprocessdt=$(date --date="$nextfiletoprocessdt + 1 day" +"%Y%m%d")
                fi


		if [[ ${file:$filepathlen} -gt ${lastfileprocessed:$filepathlen} ]]  && [[ ${file:$filepathlen} -lt $curdate ]]
		then
			if [ "$nextfiletoprocessdt" = ${file:$filepathlen} ]
                        then
				controlfiletask 1 ${file:$filepathlen}
                                returnval=$?
                                [ $returnval = 0 ] && break

				echo "file processed $nextfiletoprocessdt"
			else
				break
			fi
		else
			break
		fi
	fi
done
