DELIMITER $$

USE `AnatomediaSRT`$$

DROP PROCEDURE IF EXISTS `Anatcontrolfileinfo_I`$$

CREATE DEFINER=`srtuser`@`localhost` PROCEDURE `Anatcontrolfileinfo_I`(executionid INT,controlfilename VARCHAR(500),rowread INT,rowwritten INT,ctrlfilestatus VARCHAR(1),logfileprocessed VARCHAR(500),enddate DATETIME,current_file VARCHAR(500),OUT counter INT)
BEGIN	
	DECLARE executionhistoryid INT;
	DECLARE controlid INT;
	
	TRUNCATE AnatomediaSRT.Anatdailyrawlogs;
	
	INSERT INTO AnatomediaSRT.Anatexecutionhistory(FileProcessed,currentprocess) VALUES(current_file,'raw_data');
	SELECT LAST_INSERT_ID() INTO executionhistoryid;
	IF executionhistoryid > 0 THEN
		INSERT INTO AnatomediaSRT.Anatcontrolfileinfo(executionhistoryid,controlfilename,rowsread,controlfilestatus,logfileprocessed,rowswritten,jobenddate)	VALUES(executionhistoryid,controlfilename,rowread,ctrlfilestatus,logfileprocessed,rowwritten,enddate);
	SELECT LAST_INSERT_ID() INTO controlid;
	IF controlid > 0 THEN
	SET counter = 1;
	ELSE
	SET counter = 0;
	UPDATE AnatomediaSRT.Anatexecutionhistory SET comments='error while updating info in DB. ' WHERE executionhistoryid=executionid;
	END IF;
	ELSE
	SET counter =0;
	END IF;
END$$

DELIMITER ;