

#/bin/bash
DB_DUMP_DIR=/srt/data/                                                                          # Log file home location path
LogTimestamp=/srt/log/TimeStamp.txt                                                            # Location of Timestamp file
LogAnalysisPath=/srt/log/path.txt                                                                  # Path of that central file where final analysis file location is stored.
LogErrorPath=/srt/log/MaintenanceErrorLog.txt                                                  # Location of Eroor log file
KEEP=15                                                                                         # Max files to be kept
FileDeleted=8                                                                                        
if [ -d "$DB_DUMP_DIR" ]
then
find ${DB_DUMP_DIR} -type f -printf '%T@ %p\n' | sort -n > $LogTimestamp                       # Sorted Timestamp information of oldest file
find ${DB_DUMP_DIR} -type f -printf '%T@ %p\n' | sort -n  | head --lines=-${KEEP} | while read date filename ;
do
find $filename -mtime +16 -delete                                                               # file to be kept and deleted in number of days
done
if [ $? = 0 ]
then
b=$(echo "Excuted on $(date +%Y-%m-%d-%T) All files deleted before $FileDeleted Days")
d="$b"
echo $d >> $LogAnalysisPath                                                                   # location where Log Analysis Result
else
echo "There is some error while running the script $(date +%Y-%m-%d))" >> $LogErrorPath
fi
else
echo "path not exist $(date +%Y-%m-%d-%T))" >> $LogErrorPath
fi
