#!/bin/bash
if [ -z $1 ]
then
curdate=`date -d "i day ago" +%d/%h/%Y`                                                          # curdate variable take previous day from current date.
timing=`date -d "i day ago" +%Y%m%d`                                                             # timing variable for log date display.
else
curdate=$(date --date="$1 " +"%d/%h/%Y")                                                         # curdate variable take mentioned day log.
timing=$(date --date="$1 " +"%Y%m%d")                                                            # timing variable for  mentioned log date display.
fi                                                                                               
FilteredLogLocation=/srt/data/processanatlog                                                  # Location of filtered Log file.
RenameFileLocation=/srt/data/anatlog                                                          # Location of Renamed Log file.
LogStatusLocation=/srt/data/control                                                           # Location of Status Log file.
ErrorLogFile=/srt/log/errorlog                                                           # Location of Error Log file.
LogFileLocation=/srt/data/clickstream                                                     # Location of original Log file.
if [ -f $LogFileLocation ]                                                               # check path for original log file.
then
cat $LogFileLocation | grep "$curdate"  > $FilteredLogLocation$timing                        # Extract from original log file.
if [ $? = 0 ]
then
        q=`cat $FilteredLogLocation$timing |wc -l`                                        # Line count of log file.
        if [ $? = 0 ]
        then
        p=`cat $LogFileLocation| grep $curdate |wc -l`                                           # Line count for comparing the filtered text.
                if [ $? = 0 ]
                then
                        if [ $p -eq $q    ]                                                   # Comparing line counts .
                        then
                                status="'s'"
                        else
                                status="'f'"
                        fi
echo $status
echo "rows_written: $p rows_read: $q status: $status file_name: 'anatlog$timing' Job_end_time: $(date +%Y-%m-%d" "%T) " > $LogStatusLocation$timing    #  show sucess status.
                fi
        fi
else
echo "rows_written: 0 rows_read: 0 status: 'f' file_name: 'anatlog$timing' Job_end_time: $(date +%Y-%m-%d" "%T) " > $LogStatusLocation$timing          # show failure status.
fi
else
echo "The above mentioned log path is wrong $(date +%Y-%m-%d-%T)" >> $ErrorLogFile                                                                     # Show Error status.
fi
mv $FilteredLogLocation$timing $RenameFileLocation$timing                                                                                              #Rename file.
