#!/bin/bash
#date=`date -d "i day ago" +05/Jul/2015`
if [ -z $1 ]
then
date=`date -d "i day ago" +%d/%h/%Y`                                                          # date variable take previous day from current date.
else
date=$(date --date="$1 " +"%d/%h/%Y")                                                         # date variable take mentioned day log.
fi
timing=`date "+%Y%m%d"`                                                                       # timing variable for current date display.
now=$(date +"%T")                                                                             # now variable to show current time.
FilteredLogLocation=/root/sag/processanatlog                                                  # Location of filtered Log file.
RenameFileLocation=/root/sag/anatlog                                                          # Location of renamed Log file.
LogStatusLocation=/root/sag/control                                                           # Location of Status Log file.
ErrorLogFile=/home/test/errorlog.txt                                                          # Location of Error Log file.
LogFileLocation=/home/test/newclickstream                                                     # Location of original Log file.
if [ -f $LogFileLocation ]
then
cat $LogFileLocation | grep "$date"  > $FilteredLogLocation$timing.txt                        # Extract from original log file.
if [ $? = 0 ]
then
        q=`cat $FilteredLogLocation$timing.txt |wc -l`                                        # Line count of log file.
echo $q
        if [ $? = 0 ]
        then
        p=`cat $LogFileLocation| grep $date |wc -l`                                           # Line count for comparing the filtered text.
echo $p
                if [ $? = 0 ]
                then
                        if [ $p -eq $q    ]                                                   # Comparing line counts .
                        then
                                status="'s'"
                        else
                                status="'f'"
                        fi
echo $status
echo "rows_written: $p rows_read: $q status: $status file name: 'processanatlog$timing.txt' Job_end_time: $timing $now " > $LogStatusLocation$timing.txt    #  show sucess status.
                fi
        fi
else
echo "rows_written: 0 rows_read: 0 status: 'f' file name: 'processanatlog$timing.txt' Job_end_time: $timing $now " > $LogStatusLocation$timing.txt          # show failure status.
fi
else
echo "The above mentioned log path is wrong $(date +%Y-%m-%d-%T)" >> $ErrorLogFile                                                                          # Show Error status.
fi
mv $FilteredLogLocation$timing.txt $RenameFileLocation$timing.txt                               #Rename log file.