#/bin/bash
DB_DUMP_DIR=/opt/test  # log file location
KEEP=7    # max files to be kept
find ${DB_DUMP_DIR} -type f -printf '%T@ %p\n' | sort -n > /var/log/amit.txt
find ${DB_DUMP_DIR} -type f -printf '%T@ %p\n' | sort -n  | head --lines=-${KEEP} | while read date filename ;
do
find $filename -mtime +8 -delete  # file to be kept in number of days
done
a=$(echo "file deleted on $(date +%Y-%m-%d)")
DD=`cat /var/log/amit.txt |head -1 | awk '{print $1}'`
b=$(echo "Last folder deleted was $(date -d @$DD)")
c=$(echo $?)
d="$a $b $c"
echo $d >> /var/log/result.txt # location where data record is stored

