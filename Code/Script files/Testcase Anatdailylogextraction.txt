                                       Test case for LogFetch Script
                                  ==========================================
 
1. Fetch previous day logs from the main log file.
2. If there are no previous day logs it will return blank in output file.
3. It will count the number of lines for the Filtered log file and compare with another filterd log file.
4. It will check the status of successful and failure based on the Line mathch between the two filtered file.
5. Show status of the Log Fetch in status file with rows read and rows written along with time.
6. If the location of log file is wrong it will return an error log path doesn't exist.