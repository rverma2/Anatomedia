<?php
error_reporting(E_ERROR);
session_start();
require_once(dirname(__FILE__).'/connection.php');
//$db = new rp_Db;


// defined for http
define('HTTP','http://'.$_SERVER['SERVER_NAME']);
define('BASEURL','http://'.$_SERVER['SERVER_NAME'].'/anatomedia');
define('UPLOADURL',BASEURL.'/uploads');
define('ADMINURL',BASEURL.'/admin');
define('CSSURL',BASEURL.'/css');
define('JSURL',BASEURL.'/js');
define('IMAGEURL',BASEURL.'/images');
// defined for https
define('DOCUMENTROOT',$_SERVER['DOCUMENT_ROOT'].'/anatomedia');
define('UPLOADDIR',DOCUMENTROOT.'/uploads');
define('ANATOMEDIADOCUMENTROOT',$_SERVER['DOCUMENT_ROOT'].'/anatomedia/config');

define('CLASSPATH',DOCUMENTROOT.'/lib');
define('PERPAGE',10);
define('INVALIDCUSTOMER',1);
?>