DELIMITER $$

USE `AnatomediaSRT`$$

DROP PROCEDURE IF EXISTS `AnatGetCustomerName_S`$$

CREATE DEFINER=`srtuser`@`localhost` PROCEDURE `AnatGetCustomerName_S`()
SELECT DISTINCT id,CONCAT(CustomerName,",",customertype,",",customerstatus) AS "customerinfo" FROM AnatomediaSRT.AnatCustomer WHERE customername NOT LIKE '%.%' ORDER BY CustomerName$$

DELIMITER ;