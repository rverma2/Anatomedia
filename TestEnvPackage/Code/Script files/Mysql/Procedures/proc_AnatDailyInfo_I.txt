DELIMITER $$

USE `AnatomediaSRT`$$

DROP PROCEDURE IF EXISTS `AnatDailyInfo_I`$$

CREATE DEFINER=`srtuser`@`localhost` PROCEDURE `AnatDailyInfo_I`(executionid INT,curfiledate DATETIME)
BEGIN
DECLARE counter INT;
DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
BEGIN
    ROLLBACK;
    -- EXIT PROCEDURE;
END;
UPDATE AnatomediaSRT.Anatexecutionhistory SET currentprocess="daily stats"  WHERE executionhistoryid = executionid;
SELECT COUNT(1) INTO counter FROM AnatomediaSRT.Anatdailystatsummary WHERE DATE(dailystatdate) = DATE(curfiledate);
DROP TEMPORARY TABLE IF EXISTS tmpdailyinfo;
CREATE TEMPORARY TABLE IF NOT EXISTS tmpdailyinfo AS SELECT i.customerid,i.customername,DATE(p.logdate),SUBSTRING_INDEX(SUBSTRING_INDEX(p.`requeststring`, '/', 4),'/',-1) module,COUNT(p.parsedlogdataid) AS pagecount FROM AnatomediaSRT.Anatparsedlogdata p INNER JOIN AnatomediaSRT.AnatCustomer i INNER JOIN AnatomediaSRT.Anatmodule m WHERE m.modulename = SUBSTRING_INDEX(SUBSTRING_INDEX(p.`requeststring`, '/', 4),'/',-1) AND p.ipaddress = i.ipaddress AND p.requeststring LIKE '%.html%' AND `code`= 200 AND DATE(p.logdate) = DATE(curfiledate) GROUP BY i.customerid,i.customername,DATE(p.logdate),SUBSTRING_INDEX(SUBSTRING_INDEX(p.`requeststring`, '/', 4),'/',-1);
START TRANSACTION;
IF counter > 0
THEN
DELETE FROM AnatomediaSRT.Anatdailystatsummary WHERE DATE(dailystatdate) = DATE(curfiledate);
-- DELETE FROM AnatomediaSRT.Anatdailysessionsummary WHERE DATE(dailysessiondate) = DATE(curfiledate);
DELETE FROM AnatomediaSRT.Anatdailysessionsummaryonget WHERE DATE(dailysessiondate) = DATE(curfiledate);
END IF;
INSERT INTO AnatomediaSRT.Anatdailystatsummary (customerid,customername,dailystatdate,module,pagecount) SELECT * FROM AnatomediaSRT.tmpdailyinfo;
-- INSERT INTO AnatomediaSRT.Anatdailysessionsummary(customerid,customername,dailysessiondate,sessioncount) SELECT i.customerid,i.customername,DATE(p.logdate), COUNT(DISTINCT sessionid) sessioncount FROM AnatomediaSRT.Anatparsedlogdata p INNER JOIN AnatomediaSRT.AnatCustomer i  WHERE `code`= 200 and p.ipaddress = i.ipaddress and DATE(p.logdate) = DATE(curfiledate) GROUP BY i.customerid;
INSERT INTO AnatomediaSRT.Anatdailysessionsummaryonget(customerid,customername,dailysessiondate,sessioncount) SELECT c.customerid,c.customername,DATE(p.logdate), COUNT(p.requeststring) FROM AnatomediaSRT.Anatparsedlogdata p INNER JOIN AnatomediaSRT.AnatCustomer c WHERE requeststring = 'GET /Amedia/Anatmain.html HTTP/1.1' AND `code`= 200 AND p.ipaddress = c.ipaddress AND DATE(p.logdate) = DATE(curfiledate) GROUP BY c.customername;
COMMIT;
END$$

DELIMITER ;