
---Parsed Dailystats Datewise,Codewise
SELECT DATE(logdate) "Logdate",`code`,COUNT(parsedlogdataid) AS "htmlcount" 
FROM Anatparsedlogdata  INNER JOIN AnatomediaSRT.Anatmodule m 
WHERE m.modulename = SUBSTRING_INDEX(SUBSTRING_INDEX(`requeststring`, '/', 4),'/',-1) AND ipaddress LIKE '129.78.%'
 AND requeststring LIKE '%.html%' AND `code` IN (200,304) 
  GROUP BY DATE(logdate),`code`
 
 
 ---
 SELECT `CustomerName`,`dailystatdate`,SUM(`pagecount`) "HTML Pages"
 FROM `Anatdailystatsummary` group by `CustomerName`,`dailystatdate` 
 order by `CustomerName`,`dailystatdate` Asc


 
 --having pages but not session-
 SELECT DISTINCT(st.dailystatdate),st.customerid,st.CustomerName 
 FROM Anatdailystatsummary st LEFT JOIN Anatdailysessionsummaryonget sn 
 ON st.customerid = sn.customerid and date(st.dailystatdate) =date(sn.dailysessiondate)
 WHERE sn.customerid IS NULL
 

-having Session but not page-----

SELECT DISTINCT(st.dailystatdate),st.customerid,st.CustomerName 
FROM Anatdailystatsummary st RIGHT JOIN Anatdailysessionsummaryonget sn
 ON st.customerid = sn.customerid and date(st.dailystatdate) =date(sn.dailysessiondate) 
WHERE st.customerid IS NULL


SELECT `module` ,`dailystatdate`,`pagecount` FROM `Anatdailystatsummary` where `CustomerName`='THE UNIVERSITY OF SYDNEY' 
UNION 

SELECT '' as `module` ,`dailystatdate`,SUM(`pagecount`) as  `pagecount` FROM `Anatdailystatsummary` where `CustomerName`='THE UNIVERSITY OF SYDNEY' group by dailystatdate


SELECT `dailysessiondate`,`sessioncount` FROM `Anatdailysessionsummaryonget` where `CustomerName`='THE UNIVERSITY OF SYDNEY' order by dailysessiondate