<?php
define('DOCUMENTROOT','/var/www/html/anatomedia');
require_once('var/www/html/anatomedia/config/connect.php');
require_once('/var/www/html/anatomedia/lib/customer.php');
require_once('/var/www/html/anatomedia/lib/user.php');
require_once('/var/www/html/anatomedia/lib/customerip.php');
require_once('/var/www/html/anatomedia/lib/blacklist.php');
require_once('/var/www/html/anatomedia/lib/log.php');
require_once('/var/www/html/anatomedia/lib/admincommon.php');
require_once('/var/www/html/anatomedia/lib/pagination.php');
require_once('/var/www/html/anatomedia/lib/userlog.php');

$customer = new Customer;    
$oAdminCommon = new AdminCommon;
$arr_IP="";
$ipstart="";
//$startDateTime=mktime(0,0,0,date('m'),date('d')-1,date('Y'));
//$endDateTime=strtotime(date('2015-02-27'));

$endDateTime=mktime(23,59,59,date('m'),date('d')-1,date('Y'));

$oCutomers=$customer->get_customers_by_exp($endDateTime);
foreach($oCutomers as $oCutomer)
{
		$oCutomer['id']= isset($oCutomer['id'])?trim($oCutomer['id']):0;
		if($oCutomer['sStatus']=='Active'){
			$oCutomer['sStatus'] = 'Inactive';
			$iModifyForUserId = $oCutomer['id'];
			$oAdminCommon->do_log(8,$iModifyForUserId,2);
		}
		else{
			$oCutomer['sStatus'] = 'Active';
			$iModifyForUserId = $oCutomer['id'];
			$oAdminCommon->do_log(7,$iModifyForUserId,2);
		}
	
		/*if($oCutomer['sIpOption']=='IP' || $oCutomer['sIpOption']=='Subnet Mask')
		{
			$customer->blacklistip_htaccess($oCutomer['sCustomerIP']);
		}
		if($oCutomer['sIpOption']=='In Range')
		{	
			$iprangevalue=str_replace(","," ",$oCutomer['sCustomerIP']);
			$customer->blacklistip_htaccess($iprangevalue);
		}*/
		if($oCutomer['sIpOption']=='IP' )
		{
			$customer->blacklistip_htaccess($oCutomer['sCustomerIP']);
		}
		if($oCutomer['sIpOption']=='Multiple IP' || $oCutomer['sIpOption']=='Subnet Mask')
		{
			$iprangevalue=str_replace(","," ",$oCutomer['sCustomerIP']);
			$customer->blacklistip_htaccess($iprangevalue);
		}
		if($oCutomer['sIpOption']=='In Range')
		{
			$arr_IP=explode("-",$oCutomer['sCustomerIP']);
			for($i=0;$i<count($arr_IP);$i++)
			{
				$pattern="/(\d+).(\d+).(\d+).(\d+)/i";
				$replacement='$1.$2.$3.*';
				$strtValue_repl='$3';
				$ip_string_repl='$1.$2.';
				$iprangevalue= preg_replace($pattern, $replacement, $arr_IP[$i]);
				$ipstart[]=preg_replace($pattern, $strtValue_repl, $arr_IP[$i]);
				$ip_string=preg_replace($pattern, $ip_string_repl, $arr_IP[$i]);
			}
			for($j=$ipstart[0];$j<=$ipstart[1]; $j++)
			{
				$iprangevalue= $ip_string."".$j;
				$customer->blacklistip_htaccess($iprangevalue);
			}
			$ipstart="";
		}
		$customer->change_status($oCutomer);
}

?>