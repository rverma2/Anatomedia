<?php
require_once(dirname(__FILE__).'/../config/connect.php');
// Pull in the NuSOAP code  
define('DOCUMENTROOT',dirname(__FILE__).'/..');
define('CLASSPATH',dirname(__FILE__).'/../lib');
require_once(CLASSPATH.'/user.php');
require_once(CLASSPATH.'/customer.php');
require_once(CLASSPATH.'/customerip.php');
require_once(CLASSPATH.'/blacklist.php');
require_once(CLASSPATH.'/log.php');
require_once(CLASSPATH.'/admincommon.php');
require_once(CLASSPATH.'/pagination.php');
require_once(CLASSPATH.'/userlog.php');

$oLog = new Log; 
$customer = new Customer;    
$oAdminCommon = new AdminCommon;

//$startDateTime=mktime(0,0,0,date('m'),date('d')-1,date('Y'));
$startDateTime=strtotime(date('2014-06-26'));
$endDateTime=$startDateTime + 86399;
//$endDateTime=mktime(23,59,59,date('m'),date('d')-1,date('Y'));


$query1=" where (iErrorCodeReturned=6 or iErrorCodeReturned=12) and sDateTime >='".$startDateTime."' and sDateTime<='".$endDateTime."' GROUP BY iCustomerID";
$aLogDetails = $oLog->get_all_logs('',$query1);
foreach($aLogDetails as $aLogDetail)
{
	$query=" where (iErrorCodeReturned=6 or iErrorCodeReturned=12) and sDateTime >='".$startDateTime."' and sDateTime<='".$endDateTime."' AND iCustomerID=".$aLogDetail['iCustomerID']."";
	$aLogs = $oLog->get_all_logs('',$query);
	$string= "Partner,IP Address,Asset Name,Response Date,Completion Data,Request Date,Request Call,Student ID,Response Status,Error Code,Error Description\n";
	foreach($aLogs as $aLog)
		{
			$partner=$customer->get_customer_by_id($aLog['iCustomerID']);
			if($aLog['iErrorCodeReturned']==0) $status='Successful';
					else  $status='Failed';             
				 $string.= $aLog['sCustName'].",";
				 $string.= $aLog['sCustomerIP'].",";
				 $string.= $aLog['sAssetName'].",";               
				 $string.= $oAdminCommon->get_date_format($aLog['sDateTime']).",";
				 $string.= $oLog->get_completion_data($aLog['sCompletionData']).",";
				 $string.= $oAdminCommon->get_date_format($aLog['sDateTime']).",";
				 $string.= $aLog['sRequestCall'].",";
				 $string.= $aLog['iStudentId'].",";
				 $string.= $status.",";
				 $string.= $aLog['iErrorCodeReturned'].",";               
				 $string.= $aLog['sErrorDescription'];                
				 $string.= "\n";

		$filenm='Failed_Request_'.$aLog["sCustName"].'.csv';
		file_put_contents(dirname(__FILE__).'/'.$filenm, $string);

		$subject = "Failed Request for today-".date('Y-m-d',mktime(0,0,0,date('m'),date('d')-1,date('Y')))."";
			
			$from_name=$_SERVER['SERVER_NAME']."\r\n";
			$to=$partner[0]['sCustEmail'];
			//$to='swapnilamritkar@gmail.com';
			$from_mail="From: noreplay@mheducation.com\r\n";
			$replyto="From: noreplay@mheducation.com\r\n";
			$message = "Dear Customers,\r\n\r\nYou are receiving this message because you are using the Lead 21 Web Services provided by McGraw-Hill Education.In the asset calls, there are returned activity data that are not received by your end or we do not receive acknowledgement from your end. Therefore, we have attached the records of today for your reference.\r\n\r\nPlease feel free to contact us if you have further inquiries.\r\n\r\nYours Sincerely,\r\nMcGraw-Hill Education Service Support Team\r\n";
			$path="";
			$fileatt_type = 'text/csv';
			$file = dirname(__FILE__).'/'.$filenm;
			$file_size = filesize($file);
			$handle = fopen($file, "r");
			$content = fread($handle, $file_size);
			fclose($handle);
			$content = chunk_split(base64_encode($content)); 
			$uid = md5(uniqid(time()));
			$name = basename($file);

			if(mail_attachment($to, $subject, $message,$filenm,$from_name,$from_mail))
			{
				echo "mail send ... OK"; // or use booleans here
			//	unlink(dirname(__FILE__).'/'.$filenm);
			}
			else 
			{
			   echo "mail send ... ERROR!";
			}
		}//foreach
}//foreach
function mail_attachment($mailto, $subject, $message,$file,$from_name,$from_mail)
{
    $fh = fopen($file, "r");
    $content = chunk_split( base64_encode( fread($fh, filesize( $file ))));
    fclose($fh);
    $uid = uniqid(time(),true);
    //$toCc="mhessupport@mheducation.com";
    $filename = basename($file);
    $header = "From: McGraw-Hill Education Service Support Team <noreplay@mheducation.com>\r\n";
    $header .= "CC: ".$toCc."\r\n";
	$header .= "BCC: swapnilamritkar@gmail.com\r\n";
    $header .= "MIME-Version: 1.0\r\n";
    $header .= "Content-Type: multipart/mixed; boundary=\"{$uid}\"\r\n\r\n";
    $header .= "This is a multi-part message in MIME format.\r\n";
    $header .= "--{$uid}\r\n";
    $header .= "Content-type:text/plain; charset=iso-8859-1\r\n";
    $header .= "Content-Transfer-Encoding: 8bit\r\n\r\n";
    $header .= "{$message}\r\n\r\n";
    $header .= "--{$uid}\r\n";
    $header .= "Content-Type: text/csv; name=\"{$filename}\"\r\n"; // use different content types here
    $header .= "Content-Transfer-Encoding: base64\r\n";
    $header .= "Content-Disposition: attachment; filename=\"{$filename}\"\r\n\r\n";
    $header .= $content."\r\n\r\n";
    $header .= "--".$uid."--";
    return mail($mailto, $subject, "", $header);
}
?>