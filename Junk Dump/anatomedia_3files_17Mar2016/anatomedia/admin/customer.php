<?php
	require_once('../php/Logger.php');
	Logger::configure('logconfig.xml');
	$logger = Logger::getLogger('myLogger');
	require_once('../config/config.php');
    require_once(CLASSPATH.'/user.php');
    require_once(CLASSPATH.'/customer.php');
    require_once(CLASSPATH.'/admincommon.php');
    require_once(CLASSPATH.'/pagination.php');
    require_once(CLASSPATH.'/customerip.php');
    $oUser = new User;
    $oCustomer= new Customer;
    $page =  isset($_REQUEST['page'])&& $_REQUEST['page']>0?trim((int)$_REQUEST['page']-1):0;
    $error = '';
    if(!$oUser->user_loggedin()){
        header("location:".'login.php');
    }
    $error='';
    $sUserName ='';
    $oAdminCommon = new AdminCommon;    
    $action = isset($_REQUEST['action'])? $_REQUEST['action']:'main';
    switch($action){
        case 'customerip':
            $customerid =  isset($_REQUEST['customerid'])?(int)trim($_REQUEST['customerid']):0;
            $aCustomerDetails=$oCustomer->get_customer_by_id($customerid);
            $oCustomerIp = new CustomerIp;
            $aCustomerIpDetails = $oCustomerIp->get_all_ip('where iCustomerId='.$customerid);
            include "view/customerip.html";
            break;
        case 'addcustomerip':
            $customerid =  isset($_REQUEST['customerid'])?(int)trim($_REQUEST['customerid']):0;
            $aCustomerDtl=$oCustomer->get_customer_by_id($customerid);
            $oCustomerIp = new CustomerIp;
            if($_POST){
                $aCustomerDetails['sIpAddress'] = trim($_REQUEST['sCustomerIP']);
                $aCustomerDetails['iCustomerId'] = $customerid;               
                $oCustomerIp->save_customer_ip($aCustomerDetails);
				//$oCustomerIp->save_customer_ip($aCustomerDetails);
                $oAdminCommon->do_log(11,$customerid,2);
                header("location:".ADMINURL.'/customer.php?action=customerip&customerid='.$aCustomerDetails['iCustomerId']);
            }
            else
                include "view/addcustomerip.html";
            break;
        case 'deletecustomerip':
            $customerid =  isset($_REQUEST['customerid'])?(int)trim($_REQUEST['customerid']):0;           
            $id =  isset($_REQUEST['id'])?(int)trim($_REQUEST['id']):0;
            $oCustomerIp = new CustomerIp;
            $aData =  $oCustomerIp->delete_customer_ip($id,$customerid);
            $oAdminCommon->do_log(13,$customerid,2);          
            header("location:".ADMINURL.'/customer.php?action=customerip&customerid='.$customerid);
            break;
        case 'main':
            $perpage =  isset($_REQUEST['perpage'])?(int)trim($_REQUEST['perpage']):PERPAGE;
            $query_string_temp= '&page='.($page+1).'&perpage='.$perpage; 
            $orderby =  isset($_REQUEST['orderby'])?$_REQUEST['orderby']:'id';
            $orderbytype = isset($_REQUEST['ordertype'])?$_REQUEST['ordertype']:'desc';
            $class = $orderby;
            
			$partnertype='asc';
            $ipaddresstype ='asc';
            $emailidtype ='asc';
            $expirydatetype ='asc';
            $failedallowedtype ='asc';
            $totalfailedtype ='asc';
            $lastrequestdatetype ='asc';
            $customertatustype ='asc';    
			$subscriptiontype='asc';		
            
			if($orderby=='customer'){
                $orderby = 'sCustName';
                $partnertype= ($orderbytype=='asc')?'desc':'asc';
            }
			else if($orderby=='type'){
                $orderby = 'sSubsTrial';
                $subscriptiontype= ($orderbytype=='asc')?'desc':'asc';
            }
            else if($orderby=='ip'){
                $orderby = 'sCustomerIP';
                $ipaddresstype= ($orderbytype=='asc')?'desc':'asc';
            }
            else if($orderby=='emailid'){
                $orderby = 'sCustEmail';
                $emailidtype= ($orderbytype=='asc')?'desc':'asc';
            }
            else if($orderby=='expirydate'){
                $orderby = 'sEndDate';
                $expirydatetype= ($orderbytype=='asc')?'desc':'asc';
            }
            else if($orderby=='customertatus'){
                $orderby = 'sStatus';
                $customertatustype = ($orderbytype=='asc')?'desc':'asc';
            }
            $query_string = "&orderby=".$orderby."&ordertype=".$orderbytype;
            $oAdminCommon = new AdminCommon;    
            $offset = $page*$perpage;
            $limit = "limit $offset,".$perpage;
            $total = $oCustomer->record_count();//for pagination
            $acustomerDetails = $oCustomer->get_all_customer($limit,$orderby,$orderbytype);
			if($total>0){
                $oPagination= new Pagination;
                $sPaginationLink = $oPagination->paginate( $perpage,$page+1,'customer.php?action=main'.$query_string.'&perpage='.$perpage.'&page=',$total);
            }
            include "view/customer.html";
            break;
        case 'addcustomer':   
            $aData['sCustName'] = '';
            $aData['sCustEmail'] = '';        
            $aData['sCustomerIP'] = '';
			$aData['sStartDate'] = '';
            $aData['sEndDate'] = '';
			$aData['sIpOption']='';
            include "view/addcustomer.html";
            break;
        case 'editcustomer':
            $aData['id'] = isset($_REQUEST['id'])?trim((int)$_REQUEST['id']):0;
            $oCustomerDetails = $oCustomer->get_single_customer($aData['id']);   
            $aData['sCustName'] = isset($_POST['sCustName'])?trim($_POST['sCustName']):$oCustomerDetails['sCustName'];
            $aData['sCustEmail'] = isset($_POST['sCustEmail'])?trim($_POST['sCustEmail']):$oCustomerDetails['sCustEmail'];
			$aData['sStartDate'] = isset($_POST['sStartDate'])?trim($_POST['sStartDate']):$oAdminCommon->onlydate($oCustomerDetails['sStartDate']);
            $aData['sEndDate'] = isset($_POST['sEndDate'])?trim($_POST['sEndDate']):$oAdminCommon->onlydate($oCustomerDetails['sEndDate']);
            $aData['sCustomerIP'] = isset($_POST['sCustomerIP'])?trim($_POST['sCustomerIP']):$oCustomerDetails['sCustomerIP'];
			$aData['sSubsTrial'] =  isset($_POST['sSubsTrial'])?trim($_POST['sSubsTrial']):$oCustomerDetails['sSubsTrial'];
			$aData['sIpOption'] =  isset($_POST['sIpOption'])?trim($_POST['sIpOption']):$oCustomerDetails['sIpOption'];
			$aData['sCustomerIP'] =  isset($_POST['sCustomerIP'])?trim($_POST['sCustomerIP']):$oCustomerDetails['sCustomerIP'];
            $aData['sStatus'] =  isset($_POST['sStatus'])?trim($_POST['sStatus']):$oCustomerDetails['sStatus'];
			
            include "view/editcustomer.html";
            break;
        case 'updatecustomer':
            $aData['id'] = isset($_REQUEST['id'])?trim((int)$_REQUEST['id']):0;
            if(!isset($_POST) || !$aData['id']){
                header("location:".ADMINURL.'/customer.php');
            }
            $oCustomerDetails = $oCustomer->get_single_customer($aData['id']);            
            $aData['sCustName'] = isset($_POST['sCustName'])?trim($_POST['sCustName']):$oCustomerDetails['sCustName'];
            $aData['sCustEmail'] = isset($_POST['sCustEmail'])?trim($_POST['sCustEmail']):$oCustomerDetails['sCustEmail'];
			$aData['sStartDate'] = isset($_POST['sStartDate'])?trim($_POST['sStartDate']):$oCustomerDetails['sStartDate'];
            $aData['sEndDate'] = isset($_POST['sEndDate'])?trim($_POST['sEndDate']):$oCustomerDetails['sEndDate'];
			$aData['sSubsTrial'] =  isset($_POST['sSubsTrial'])?trim($_POST['sSubsTrial']):$oCustomerDetails['sSubsTrial'];
			$aData['sIpOption'] =  isset($_POST['sIpOption'])?trim($_POST['sIpOption']):$oCustomerDetails['sIpOption'];
            $aData['sCustomerIP'] = isset($_POST['sCustomerIP'])?trim($_POST['sCustomerIP']):$oCustomerDetails['sCustomerIP'];
            $aData['sStatus'] =  isset($_POST['sStatus'])?trim($_POST['sStatus']):$oCustomerDetails['sStatus'];
			$error = $oCustomer->validate($aData);
			if($error){
                $page = $page>0?$page-1:0;
                include "view/editcustomer.html";
            }
            else{
                $oCustomer->update_customer($aData);
                $iModifyForUserId =$aData['id'];
                $oAdminCommon->do_log(6,$iModifyForUserId,2);
				$logger->warn("Customer Data is updated ".serialize($aData) ."by ".$_SESSION['UserName']);
				header("location:".ADMINURL.'/customer.php?page='.$page);
            }
            break;
        case 'savecustomer';          
            $aData['sCustName'] = isset($_POST['sCustName'])?trim($_POST['sCustName']):'';
            $aData['sCustEmail'] = isset($_POST['sCustEmail'])?trim($_POST['sCustEmail']):'';
			$aData['sStartDate'] = isset($_POST['sStartDate'])?trim($_POST['sStartDate']):'';
            $aData['sEndDate'] = isset($_POST['sEndDate'])?trim($_POST['sEndDate']):'';
            $aData['sCustomerIP'] = isset($_POST['sCustomerIP'])?trim($_POST['sCustomerIP']):'';
			$aData['sIpOption'] = isset($_POST['sIpOption'])?trim($_POST['sIpOption']):'';
			$aData['sStatus'] = isset($_POST['sStatus'])?trim($_POST['sStatus']):'';
			$aData['sSubsTrial']= isset($_POST['sSubsTrial'])?trim($_POST['sSubsTrial']):'';
			
			$error = $oCustomer->validate($aData);
            if($error){
                include "view/addcustomer.html";
            }
            else{
                $oCustomer->save_customer($aData);
                $iModifyForUserId = mysql_insert_id();
				$oAdminCommon->do_log(5,$iModifyForUserId,2);
				$logger->warn("Customer Data is saved ".serialize($aData) ."by ".$_SESSION['UserName']);
                header("location:".ADMINURL.'/customer.php');
            }
            break;
        case 'changestatus':        
            $aData['id']= isset($_REQUEST['id'])?trim($_REQUEST['id']):0;
            $oCustomerDetails = $oCustomer->get_single_customer($aData['id']);
            if($oCustomerDetails['sStatus']=='Active'){
                $aData['sStatus'] = 'Inactive';
                $iModifyForUserId = $aData['id'];
                $oAdminCommon->do_log(8,$iModifyForUserId,2);
            }
            else{
                $aData['sStatus'] = 'Active';
                $iModifyForUserId = $aData['id'];
                $oAdminCommon->do_log(7,$iModifyForUserId,2);
            }
            $oCustomer->change_status($aData);
            header("location:".ADMINURL.'/customer.php?page='.($page+1));
            break;
        case 'default':
            include "view/login.html";
            break;  
    }
    
    
?>