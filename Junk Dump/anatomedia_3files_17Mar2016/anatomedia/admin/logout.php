<?php
	require_once('../config/config.php');
	// Pull in the NuSOAP code	
	require_once(CLASSPATH.'/user.php');
	require_once(CLASSPATH.'/admincommon.php');
	$oUser = new User;
	$oCommon = new AdminCommon;
	if($_SESSION)
		$oCommon->do_log(10,$_SESSION['userID']);
	$oUser->logout();
	
?>