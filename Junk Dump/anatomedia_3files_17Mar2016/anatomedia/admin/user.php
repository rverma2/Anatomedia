	<?php
	require_once('../config/config.php');
	// Pull in the NuSOAP code	
	require_once(CLASSPATH.'/user.php');
	require_once(CLASSPATH.'/admincommon.php');
	require_once(CLASSPATH.'/pagination.php');
	$oUser = new User;	
	$page =  isset($_REQUEST['page'])&& $_REQUEST['page']>0?trim((int)$_REQUEST['page']-1):0; 
	$error = '';
	if(!$oUser->user_loggedin()){
		header("location:".'login.php');
	}
	$error='';
	$sUserName ='';	
	$oAdminCommon = new AdminCommon;
	$action = isset($_REQUEST['action'])? $_REQUEST['action']:'main';
	switch($action){
		case 'main':
			$perpage =  isset($_REQUEST['perpage'])?(int)trim($_REQUEST['perpage']):PERPAGE;
			$query_string_temp= '&page='.($page+1).'&perpage='.$perpage; 
			$orderby =  isset($_REQUEST['orderby'])?$_REQUEST['orderby']:'id';
			$orderbytype = isset($_REQUEST['ordertype'])?$_REQUEST['ordertype']:'desc';
			$class = $orderby;
			$usernametype ='asc';
			$firstnametype ='asc';
			$lastnametype ='asc';
			$emailidtype ='asc';
			$contacttype ='asc';
			$creationdatetype ='asc';
			$lastlogintype ='asc';
			$adminstatustype ='asc';					
			if($orderby=='username'){
				$orderby = 'sUserName';
				$usernametype= ($orderbytype=='asc')?'desc':'asc';
			}
			else if($orderby=='firstname'){
				$orderby = 'sFirstName';
				$firstnametype= ($orderbytype=='asc')?'desc':'asc';
			}
			else if($orderby=='lastname'){
				$orderby = 'sLastName';
				$lastnametype= ($orderbytype=='asc')?'desc':'asc';
			}
			else if($orderby=='emailid'){
				$orderby = 'sCustEmail';
				$emailidtype= ($orderbytype=='asc')?'desc':'asc';
			}
			else if($orderby=='contact'){
				$orderby = 'sContact';
				$contacttype= ($orderbytype=='asc')?'desc':'asc';
			}
			else if($orderby=='creationdate'){
				$orderby = 'sCreatedOn';
				$creationdatetype= ($orderbytype=='asc')?'desc':'asc';
			}
			else if($orderby=='lastlogin'){
				$orderby = 'sLastLoginDateTime';
				$lastlogintype= ($orderbytype=='asc')?'desc':'asc';
			}			
			else if($orderby=='adminstatus'){
				$orderby = 'sStatus';
				$adminstatustype= ($orderbytype=='asc')?'desc':'asc';
			}			
			$oAdminCommon = new AdminCommon;
			$query_string = "&orderby=".$orderby."&ordertype=".$orderbytype;			
			$offset = $page*$perpage;
			$limit = "limit $offset,".$perpage;
			$total = $oUser->record_count();//for pagination
			$aUserDetails = $oUser->get_all_users($limit,$orderby,$orderbytype);
			if($total>0){
				$oPagination= new Pagination;
				$sPaginationLink = $oPagination->paginate($perpage,$page+1,'user.php?action=main'.$query_string.'&page=',$total);
			}
			include "view/user.html";
			break;
		case 'adduser':	
			$aData['sUserName'] = '';
			$aData['sFirstName'] = '';
			$aData['sLastName'] = '';
			$aData['sCustEmail'] = '';
			$aData['sContact'] = '';			
			include "view/adduser.html";
			break;
		case 'edituser':
			$aData['id'] = isset($_REQUEST['id'])?trim((int)$_REQUEST['id']):0;
			$oUserDetails = $oUser->get_single_user($aData['id']);			
			$aData['sUserName'] = $oUserDetails['sUserName'];
			$aData['sFirstName'] = $oUserDetails['sFirstName'];
			$aData['sLastName'] = $oUserDetails['sLastName'];
			$aData['sCustEmail'] = $oUserDetails['sCustEmail'];
			$aData['sContact'] = $oUserDetails['sContact'];	
			$aData['sPassword'] = $oUserDetails['sPassword'];
			$aData['sStatus'] = $oUserDetails['sStatus'];			
			//$sStatus = ucfirst($oUserDetails['sStatus']);
			$sLastLogin = $oUserDetails['sLastLoginDateTime'];
			$cancel_url = str_replace('edituser','main',$_SERVER['REQUEST_URI']);
			include "view/edituser.html";
			break;
		case 'updateuser':
			$aData['id'] = isset($_REQUEST['id'])?trim((int)$_REQUEST['id']):0;
			if(!isset($_POST) || !$aData['id']){
				header("location:".ADMINURL.'/user.php');
			}
			$oUserDetails = $oUser->get_single_user($aData['id']);			
			$aData['sUserName'] = isset($_POST['sUserName'])?trim($_POST['sUserName']):$oUserDetails['sUserName']; 
			$aData['sFirstName'] =  isset($_POST['sFirstName'])?trim($_POST['sFirstName']):$oUserDetails['sFirstName'];
			$aData['sLastName'] =  isset($_POST['sLastName'])?trim($_POST['sLastName']):$oUserDetails['sLastName'];
			$aData['sCustEmail'] =  isset($_POST['sCustEmail'])?trim($_POST['sCustEmail']):$oUserDetails['sCustEmail'];
			$aData['sContact'] =  isset($_POST['sContact'])?trim($_POST['sContact']):$oUserDetails['sContact'];	
			$aData['sPassword'] =  isset($_POST['sPassword'])?trim($_POST['sPassword']):$oUserDetails['sPassword'];
			$aData['sStatus'] =  isset($_POST['sStatus'])?trim($_POST['sStatus']):$oUserDetails['sStatus'];
			$sConfirmPassword = isset($_POST['sConfirmPassword'])?trim($_POST['sConfirmPassword']):'';
			$sStatus = ucfirst($oUserDetails['sStatus']);
			$sLastLogin = $oUserDetails['sLastLoginDateTime'];
			$error = $oUser->validate($aData,$sConfirmPassword);
			if($error){				
				$page = $page>0?$page-1:0;
				include "view/edituser.html";
			}
			else{
				$oUser->update_user($aData);
				$iModifyForUserId = $aData['id'];
				$oAdminCommon->do_log(2,$iModifyForUserId);
				header("location:".ADMINURL.'/user.php?page='.$page);
			}
			break;
		case 'saveuser';
			$aData['sUserName'] = isset($_POST['sUserName'])?trim($_POST['sUserName']):'';
			$aData['sPassword'] = isset($_POST['sPassword'])?trim($_POST['sPassword']):'';
			$sConfirmPassword = isset($_POST['sConfirmPassword'])?trim($_POST['sConfirmPassword']):'';
			$aData['sFirstName'] = isset($_POST['sFirstName'])?trim($_POST['sFirstName']):'';
			$aData['sLastName']= isset($_POST['sLastName'])?trim($_POST['sLastName']):'';
			$aData['sCustEmail'] = isset($_POST['sCustEmail'])?trim($_POST['sCustEmail']):'';
			$aData['sContact'] = isset($_POST['sContact'])?trim($_POST['sContact']):'';			
			$error = $oUser->validate($aData,$sConfirmPassword);
			if($error){
				include "view/adduser.html";
			}
			else{
				$oUser->save_user($aData);	
				$iModifyForUserId = mysql_insert_id();
				$oAdminCommon->do_log(1,$iModifyForUserId);
				header("location:".ADMINURL.'/user.php');
			}
			break;
		case 'changestatus':		
			$aData['id']= isset($_REQUEST['id'])?trim($_REQUEST['id']):0;
			$oUserDetails = $oUser->get_single_user($aData['id']);
			if($oUserDetails['sStatus']=='active'){
				$aData['sStatus'] = 'inactive';
				$iModifyForUserId = $aData['id'];
				$oAdminCommon->do_log(4,$iModifyForUserId);
			}
			else{
				$aData['sStatus'] = 'active';
				$iModifyForUserId = $aData['id'];
				$oAdminCommon->do_log(3,$iModifyForUserId);
			}
			$oUser->update_user($aData);
			header("location:".ADMINURL.'/user.php?page='.($page+1));
			break;
		case 'default':
			include "view/login.html";
			break;	
	}
	
	
?>