<?php
	require_once('../config/config.php');
	// Pull in the NuSOAP code	
	require_once(CLASSPATH.'/user.php');
	require_once(CLASSPATH.'/admincommon.php');
	$oUser = new User;
	if($oUser->user_loggedin()){
		header("location: ".ADMINURL.'/index.php');
	}
	$error='';
	$sUserName ='';	
	$action = isset($_REQUEST['action'])? $_REQUEST['action']:'main';
	switch($action){
		case 'main':
			include "view/login.html";
			break;
		case 'login';
			$sUserName = isset($_POST['sUserName'])?$_POST['sUserName']:'';
			$sPassword = isset($_POST['sPassword'])?$_POST['sPassword']:'';
			$error = $oUser->login($sUserName,$sPassword);			
			if($error){				
				include "view/login.html";
			}
			else{
				$oCommon = new AdminCommon;
				$oCommon->do_log(9,$_SESSION['userID']);				
				header("location:".ADMINURL."/customer.php");
			}
			break;
		case 'default':
			include "view/login.html";
			break;	
	}
	
	
?>