<?php
    require_once('../config/config.php');
    // Pull in the NuSOAP code  
    require_once(CLASSPATH.'/user.php');
    require_once(CLASSPATH.'/customer.php');
    require_once(CLASSPATH.'/admincommon.php');
    require_once(CLASSPATH.'/pagination.php');
    $oUser = new User;
    if(!$oUser->user_loggedin()){
        header("location:".ADMINURLS.'/login.php');
    }
    $oCustomer = new Customer;
    $page =  isset($_REQUEST['page'])&& $_REQUEST['page']>0?trim((int)$_REQUEST['page']-1):0; 
    $oAdminCommon = new AdminCommon;    
    $offset = $page*PERPAGE;
    $limit = "limit $offset,".PERPAGE;
    //$total = $oCustomer->failed_request_count('where iNoOfFailedRequest>0');//for pagination
   // $aCustomerDetails = $oCustomer->get_all_failed_request_customer($limit);
    
    include "view/dashboard.html";  
?>