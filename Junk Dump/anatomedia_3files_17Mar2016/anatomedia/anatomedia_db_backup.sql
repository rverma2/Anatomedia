-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 23, 2015 at 01:38 PM
-- Server version: 5.6.24
-- PHP Version: 5.5.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `anatomedia`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `id` int(11) NOT NULL,
  `sCustName` varchar(150) DEFAULT NULL,
  `sCustEmail` varchar(150) DEFAULT NULL,
  `sSubsTrial` enum('Trial','Subscribed') NOT NULL DEFAULT 'Trial',
  `sStartDate` varchar(20) DEFAULT NULL,
  `sEndDate` varchar(20) DEFAULT NULL,
  `sIpOption` varchar(20) DEFAULT NULL,
  `sCustomerIP` varchar(150) DEFAULT NULL,
  `sStatus` enum('Inactive','Active') NOT NULL DEFAULT 'Active',
  `sCreatedOn` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `sCustName`, `sCustEmail`, `sSubsTrial`, `sStartDate`, `sEndDate`, `sIpOption`, `sCustomerIP`, `sStatus`, `sCreatedOn`) VALUES
(1, 'Murdoch University 1', 'chakradhar.meshram@aptaracorp.com', 'Trial', '1441317600', '1443564000', 'In Range', '134.115.2.112-134.115.2.127', 'Active', '1441358553');

-- --------------------------------------------------------

--
-- Table structure for table `customer_log`
--

CREATE TABLE IF NOT EXISTS `customer_log` (
  `id` int(11) NOT NULL,
  `IcustId` int(11) DEFAULT NULL,
  `IuserId` int(11) DEFAULT NULL,
  `IuserAction` int(2) DEFAULT NULL,
  `DmodifiedDate` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `sUserName` varchar(50) NOT NULL,
  `sPassword` varchar(150) NOT NULL,
  `sFirstName` varchar(50) DEFAULT NULL,
  `sLastName` varchar(50) DEFAULT NULL,
  `sStatus` enum('Y','N') DEFAULT 'Y',
  `sLastLoginDateTime` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `sUserName`, `sPassword`, `sFirstName`, `sLastName`, `sStatus`, `sLastLoginDateTime`) VALUES
(1, 'admin', 'Admin@123', 'Admin', 'A', 'Y', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_actions`
--

CREATE TABLE IF NOT EXISTS `user_actions` (
  `id` int(11) NOT NULL,
  `sAction` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_actions`
--

INSERT INTO `user_actions` (`id`, `sAction`) VALUES
(1, 'Admin added'),
(2, 'Admin updated'),
(3, 'Admin activated'),
(4, 'Admin deactivated'),
(5, 'Customer added'),
(6, 'Customer updated'),
(7, 'Customer activated'),
(8, 'Cusotmer deactivated'),
(9, 'Login'),
(10, 'Logout');

-- --------------------------------------------------------

--
-- Table structure for table `user_logs`
--

CREATE TABLE IF NOT EXISTS `user_logs` (
  `id` int(11) NOT NULL,
  `iUserActionId` int(2) DEFAULT NULL,
  `iModifyForUserId` int(11) DEFAULT NULL,
  `iModifiedByUserId` int(11) DEFAULT NULL,
  `iModifiedTye` int(2) DEFAULT NULL,
  `sModifyDateTime` varchar(20) DEFAULT NULL,
  `sUserActionDetails` text
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_logs`
--

INSERT INTO `user_logs` (`id`, `iUserActionId`, `iModifyForUserId`, `iModifiedByUserId`, `iModifiedTye`, `sModifyDateTime`, `sUserActionDetails`) VALUES
(1, 9, 1, 1, 1, '1437485155', NULL),
(2, 9, 1, 1, 1, '1438329937', NULL),
(3, 9, 1, 1, 1, '1440485655', NULL),
(4, 9, 1, 1, 1, '1441358503', NULL),
(5, 5, 1, 1, 2, '1441358553', NULL),
(6, 8, 1, 0, 2, '1441358911', NULL),
(7, 8, 1, 0, 2, '1441358978', NULL),
(8, 8, 1, 0, 2, '1441358979', NULL),
(9, 8, 1, 0, 2, '1441358980', NULL),
(10, 8, 1, 0, 2, '1441358980', NULL),
(11, 8, 1, 0, 2, '1441359209', NULL),
(12, 8, 1, 0, 2, '1441359272', NULL),
(13, 8, 1, 0, 2, '1441359299', NULL),
(14, 8, 1, 0, 2, '1441359312', NULL),
(15, 8, 1, 0, 2, '1441359349', NULL),
(16, 8, 1, 0, 2, '1441359379', NULL),
(17, 8, 1, 0, 2, '1441359395', NULL),
(18, 8, 1, 0, 2, '1441359396', NULL),
(19, 8, 1, 0, 2, '1441359396', NULL),
(20, 8, 1, 0, 2, '1441359396', NULL),
(21, 8, 1, 0, 2, '1441359430', NULL),
(22, 8, 1, 0, 2, '1441359439', NULL),
(23, 8, 1, 0, 2, '1441359492', NULL),
(24, 8, 1, 0, 2, '1441359561', NULL),
(25, 8, 1, 0, 2, '1441359598', NULL),
(26, 8, 1, 0, 2, '1441359606', NULL),
(27, 8, 1, 0, 2, '1441359612', NULL),
(28, 8, 1, 0, 2, '1441359620', NULL),
(29, 8, 1, 0, 2, '1441359680', NULL),
(30, 6, 1, 1, 2, '1441359892', NULL),
(31, 8, 1, 0, 2, '1441359901', NULL),
(32, 8, 1, 0, 2, '1441359909', NULL),
(33, 8, 1, 0, 2, '1441359941', NULL),
(34, 8, 1, 0, 2, '1441359969', NULL),
(35, 8, 1, 0, 2, '1441359976', NULL),
(36, 6, 1, 1, 2, '1441360034', NULL),
(37, 8, 1, 0, 2, '1441360041', NULL),
(38, 6, 1, 1, 2, '1441360096', NULL),
(39, 8, 1, 0, 2, '1441360098', NULL),
(40, 6, 1, 1, 2, '1441360171', NULL),
(41, 8, 1, 0, 2, '1441360173', NULL),
(42, 8, 1, 0, 2, '1441361186', NULL),
(43, 8, 1, 0, 2, '1441361312', NULL),
(44, 8, 1, 0, 2, '1441361314', NULL),
(45, 8, 1, 0, 2, '1441361314', NULL),
(46, 8, 1, 0, 2, '1441361314', NULL),
(47, 8, 1, 0, 2, '1441361352', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_log`
--
ALTER TABLE `customer_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_actions`
--
ALTER TABLE `user_actions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_logs`
--
ALTER TABLE `user_logs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `customer_log`
--
ALTER TABLE `customer_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_actions`
--
ALTER TABLE `user_actions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `user_logs`
--
ALTER TABLE `user_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=48;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
