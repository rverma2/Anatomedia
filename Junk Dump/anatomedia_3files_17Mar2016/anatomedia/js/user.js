$().ready(function() {

        $.validator.addMethod("validip", function(value, element) {
            return this.optional(element) || /^\d{1,3}[.]\d{1,3}[.]\d{1,3}[.]\d{1,3}$/i.test(value);
        }, "Please enter valid server ip");
        $.validator.addMethod("alphanumeric", function(value, element) {
            return this.optional(element) || /^[a-z0-9]+$/i.test(value);
        }, "Authentication Name must contain only letters and numbers");
        
    // validate signup form on keyup and submit
    $("#signupForm").validate({
         errorPlacement: function (error, element) {
            error.insertAfter(element);          
            element.next().removeClass('elementvalid').addClass('elementerror');           
        },
        success: function (label) {            
            label.removeClass('elementerror').addClass('elementvalid');             
        },
        highlight: function (element, errorClass, validClass) {           
            $(element).next().removeClass('elementvalid').addClass('elementerror');         
        },
        unhighlight: function (element, errorClass, validClass) {         
            $(element).next().removeClass('elementerror').addClass('elementvalid');         
        },
        rules: {
            sUserName: {
                required:{
                depends:function(){
                            $(this).val($.trim($(this).val()));
                            return true;
                        }
                },
                alphanumeric:true,
                minlength: 5,
                remote: {
                            url:'ajaxusercheck.php',                            
                            type: "post",
                            data:{
                             sUserName: function () {
                                
                                return $('#sUserName').val();
                             },
                             id: function () {
                                if($('#id')){
                                    return $('#id').val();
                                }
                                else
                                    return 0;
                             }
                            }
                            
                        }
            },
            sPassword: {
                required: {
                depends:function(){
                            $(this).val($.trim($(this).val()));
                            return true;
                        }
                },
                minlength: adminPassLength
            },
            sConfirmPassword: {
                required: {
                depends:function(){
                            $(this).val($.trim($(this).val()));
                            return true;
                        }
                },
                minlength: adminPassLength,
                equalTo: "#sPassword"
            },
            sFirstName: {
                required:{
                depends:function(){
                            $(this).val($.trim($(this).val()));
                            return true;
                        }
                }
            },
            sLastName: {
                required:{
                depends:function(){
                            $(this).val($.trim($(this).val()));
                            return true;
                        }
                }               
            },
            sContact:{
               number: true
            },
            sCustEmail: {
                required: {
                    depends:function(){
                            $(this).val($.trim($(this).val()));
                            return true;
                    }
                },
                email: true
            }
        },
        messages: { 
            sUserName:{
                remote:"User name already exists"
            },
            sConfirmPassword:{
                equalTo:"Password and confirm password does not match"
            }
        }
    });
}); 