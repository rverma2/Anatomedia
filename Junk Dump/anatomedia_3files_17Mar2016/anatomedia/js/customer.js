 function checkipv6(str) {
            var perlipv6regex = "^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$";
            var aeronipv6regex = "^\s*((?=.*::.*)(::)?([0-9A-F]{1,4}(:(?=[0-9A-F])|(?!\2)(?!\5)(::)|\z)){0,7}|((?=.*::.*)(::)?([0-9A-F]{1,4}(:(?=[0-9A-F])|(?!\7)(?!\10)(::))){0,5}|([0-9A-F]{1,4}:){6})((25[0-5]|(2[0-4]|1[0-9]|[1-9]?)[0-9])(\.(?=.)|\z)){4}|([0-9A-F]{1,4}:){7}[0-9A-F]{1,4})\s*$";

            var regex = "/" + perlipv6regex + "/";
            return (/^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$/.test(str));
}
function parseDate(val) {
  var b = val.split(/\D/);
  return new Date(b[2], --b[0], b[1]);
}
$().ready(function() {
		$.validator.addMethod("validipSub", function(value, element) {
			if($('#sIpOption').val() =='Multiple IP'){
				var returnvalue;
				var result = value.split(",");
				for (i = 0; i < result.length; i++) {
					var octet = '(?:25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]|[0-9])';
					if (result[i].split(":").length == 1) {
						var ip = '(?:' + octet + '\\.){3}' + octet;
						var ipRE = new RegExp('^' + ip + '$');
						returnvalue = ipRE.test(result[i]);
					}
					else {
						returnvalue = checkipv6(result[i]);
					}
					// var octet= '([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])'
					//var octet = '(?2[0-4]\d|25[0-5]|[01]?\d\d?).(?2[0-4]\d|25[0-5]|[01]?\d\d?).(?2[0-4]\d|25[0-5]|[01]?\d\d?).(?2[0-4]\d|25[0-5]|[01]?\d\d?)'
					//                var ip = '(?:' + octet + '\\.){3}' + octet;
					//                var ipRE = new RegExp('^' + ip + '$');
					if (returnvalue == false) {
						return returnvalue;
					}
				}
				return returnvalue;
				
			}
			else if($('#sIpOption').val() =='In Range')
			{
				var returnvalue;
				//alert("***********"+value);
				var result1 = value.split("-");
				//alert(result1.toString());
					if(result1.length > 2)
					{
					return false;
					}
					else 
					{
					var split_arr_ = new Array();
					for (i = 0; i < result1.length; i++){
						split_arr_[i] = new Array();
						split_arr_[i]=result1[i].split(".");
					}
					
					if(split_arr_[0][0]!= split_arr_[1][0] || split_arr_[0][1]!= split_arr_[1][1] )
					{
					return false;
					}
					for (i = 0; i < result1.length; i++) {
						var octet = '(?:25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]|[0-9])';
						if (result1[i].split(":").length == 1) {
							var ip = '(?:' + octet + '\\.){3}' + octet;
							var ipRE = new RegExp('^' + ip + '$');
							returnvalue = ipRE.test(result1[i]);
						}
						else {
							returnvalue = checkipv6(result1[i]);
						}
						// var octet= '([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])'
						//var octet = '(?2[0-4]\d|25[0-5]|[01]?\d\d?).(?2[0-4]\d|25[0-5]|[01]?\d\d?).(?2[0-4]\d|25[0-5]|[01]?\d\d?).(?2[0-4]\d|25[0-5]|[01]?\d\d?)'
						//                var ip = '(?:' + octet + '\\.){3}' + octet;
						//                var ipRE = new RegExp('^' + ip + '$');
						if (returnvalue == false) {
							return returnvalue;
						}
					}
				return returnvalue;
				}
				
			}
			else if($('#sIpOption').val() =='Subnet Mask')
			{
			var result1 = value.split(",");
			for (i = 0; i < result1.length; i++) {
				var octet = '(?:25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]|[0-9])';
				var result = result1[i].split("/");
				var ip = '(?:' + octet + '\\.){3}' + octet;
				var ipRE = new RegExp('^' + ip + '$');
				returnvalue = ipRE.test(result[0]);
				if(result.length != 2)
				{
					return false;
				}
				if (returnvalue == false) {
					return returnvalue;
				}
			}
			return returnvalue;
				//return this.optional(element) || /^\d{1,3}[.]\d{1,3}[.]\d{1,3}[.]\d{1,3}[/]\d{1,2}$/i.test(value);
			}
			else if($('#sIpOption').val() =='IP')
			{
				var octet = '(?:25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]|[0-9])';
				var ip = '(?:' + octet + '\\.){3}' + octet;
				var ipRE = new RegExp('^' + ip + '$');
				returnvalue = ipRE.test(value);
				return returnvalue;
				
			//	return this.optional(element) || /^\d{1,3}[.]\d{1,3}[.]\d{1,3}[.]\d{1,3}$/i.test(value);
			}
		    }, "Please enter valid server ip");
	    $.validator.addMethod("alphanumeric", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9.\s]+$/i.test(value);
        }, "Customer Name must contain only letters and numbers");
		
		$.validator.addMethod("validDate", function(value, element) {
		if($('#sStartDate').val()==undefined || $('#sStartDate').val()== null)
			{
				var today = new Date();
				today.setHours(0,0,0,0);
				if (parseDate(value) < today) 
					{ return false;} else { return true;}
			}
		else
			{
				if(parseDate(value) < parseDate($('#sStartDate').val()))
					{ return false;	} else { return true;}
			}
		},'Please select a proper date.');
       // validate signup form on keyup and submit
    $("#signupForm").validate({ 
   
        errorPlacement: function (error, element) {
            error.insertAfter(element);          
            element.next().removeClass('elementvalid').addClass('elementerror');           
        },
        success: function (label) {            
            label.removeClass('elementerror').addClass('elementvalid');             
        },
        highlight: function (element, errorClass, validClass) {           
            $(element).next().removeClass('elementvalid').addClass('elementerror');         
        },
        unhighlight: function (element, errorClass, validClass) {         
            $(element).next().removeClass('elementerror').addClass('elementvalid');         
        },
        rules: {
            sCustName: {
                required:{
                    depends:function(){
                        $(this).val(($(this).val()));
                        return true;
                    }
                },
                alphanumeric:true,
                minlength: 5,
                remote: {
                            url:'ajaxcheck.php',                            
                            type: "post",
                            data:{
                             sCustName: function () {
                                return $('#sCustName').val();
                             },
                             id: function () {
                                if($('#id')){
                                    return $('#id').val();
                                }
                                else
                                    return 0;
                             }
                            }
                            
                        }
            },
            sCustEmail: {
                required: {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
                email: true
            },
             sCustomerIP:{
                required:{
                depends:function(){
                    $(this).val($.trim($(this).val()));
					return true;
                }
            },
			    validipSub:true
            },
			sStartDate: {
                required: true
            },
			sEndDate: {
			required:{
                 depends:function(){
                    $(this).val($.trim($(this).val()));
					return true;
                }
			},
			validDate: true
			},
        messages: { 
            sCustName:{
                remote:"Customer name already exists"
            }
            
        }
	}
    });
    $('.valid').add('<label>okay fine</label>');
}); 