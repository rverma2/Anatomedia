$().ready(function() {      
    // validate signup form on keyup and submit
    $("#signupForm").validate({ 
   
         errorPlacement: function (error, element) {
            error.insertAfter(element);          
            element.next().removeClass('elementvalid').addClass('elementerror');           
        },
        success: function (label) {            
            label.removeClass('elementerror').addClass('elementvalid');             
        },
        highlight: function (element, errorClass, validClass) {           
            $(element).next().removeClass('elementvalid').addClass('elementerror');         
        },
        unhighlight: function (element, errorClass, validClass) {         
            $(element).next().removeClass('elementerror').addClass('elementvalid');         
        },
        rules: {
            sessionTimeout: {
                required:{
                depends:function(){
                            $(this).val($.trim($(this).val()));
                            return true;
                        }
                },
                digits:true                 
            },
            assetPathRoot: {
                required:{
                depends:function(){
                            $(this).val($.trim($(this).val()));
                            return true;
                        }
                }                               
            },
            assetPathHttp: {
                required: {
                depends:function(){
                            $(this).val($.trim($(this).val()));
                            return true;
                        }
                }              
            },
            adminPassLength: {
                required:{
                depends:function(){
                            $(this).val($.trim($(this).val()));
                            return true;
                        }
                },
                digits:true                 
            },
            customerPassLength: {
                required:{
                depends:function(){
                            $(this).val($.trim($(this).val()));
                            return true;
                        }
                },
                digits:true                 
            },
            invalidIpCount:{
                 required:{depends:function(){
                            $(this).val($.trim($(this).val()));
                            return true;
                        }
                },
                digits:true
            },
            hashKeyLength: {
                required:{
                depends:function(){
                            $(this).val($.trim($(this).val()));
                            return true;
                        }
                },
                digits:true                 
            }           
        }
    }); 
}); 