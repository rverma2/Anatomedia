<?php	
	require_once(DOCUMENTROOT.'/config/connect.php');	
	class SiteSetting{
		var $sTableName='sitesettings';		
		var $db;
		function __construct()
		{
			$this->db = new db;
		}	
		function update_sitesetting($aData){			
			//$db = new db;			
			return $this->db->update($this->sTableName,$aData);
		}
		
		function get_sitesetting($id){
			//$db = new db;
			$query = "select * from ".$this->sTableName." where id=$id limit 1";
			$aData = $this->db->fetch_object($query);			
			if($aData)
				return $aData[0];
			else
				return '';
		}	
		
	}
