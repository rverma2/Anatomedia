<?php	
	require_once(DOCUMENTROOT.'/config/connect.php');
	class User{	
		function save_user($aUserDetails){
			$aUserDetails['sCreatedOn']=time();
			$db = new db;
			return $db->insert('users',$aUserDetails);
		}
		function update_user($aUserDetails,$flag=0){			
			$db = new db;
			if($_SESSION['userID']==$aUserDetails['id'] && $flag==0){ //then change welcome msg
				$_SESSION['firstName'] = $aUserDetails['sFirstName'];
				$_SESSION['lastName'] = $aUserDetails['sLastName'];
			}
			return $db->update('users',$aUserDetails);
			
		}
		function login($sUserName,$sPassword){
			$db = new db;
			$row = $db->query("Select id,sUserName,sFirstName,sLastName,sStatus from user where sUserName='".$sUserName."' and sPassword='".$sPassword."' limit 1");
			$error ='';
			
			if(mysql_num_rows($row)>0){
				$result = mysql_fetch_object($row);
				if($result->sStatus=='Y'){
					$_SESSION['userID'] = $result->id;
					$_SESSION['UserName']=	$result->sUserName;
					$_SESSION['firstName'] = $result->sFirstName;
					$_SESSION['lastName'] = $result->sLastName;
					$aUserData['id']=$result->id;
					$aUserData['sLastLoginDateTime']=time();
					$this->update_user($aUserData,1);
					header("location:".ADMINURL.'/dashboard.php');
				}//if
				else{
					$error ='Account deactivated, contact administrator.';
				}							
			}
			else
				$error='Username/Password is invalid., Please Try Again.';
			return $error;
		}
		function logout(){
			session_destroy();
			header("location: login.php");
		}
		function is_user_loggedin(){
			if(isset($_SESSION['userID']))
				header("location:".DOCUMENTROOT.'/customer.php');
			else
				header("location: login.php");
		}
		function user_loggedin(){
			if(isset($_SESSION['userID']))
				return true;
			else
				return false;
		}
		function get_all_users($limit=null,$orderby,$orderbytype){
			$db = new db;
			$query = "select * from users order by $orderby $orderbytype $limit";
			$aUserDetails = $db->fetch_object($query);
			return $aUserDetails;
		}
		function record_count($where=null){
			$db = new db;
			$query = "select count(*) as cnt from users $where";
			$aUserDetails = $db->fetch_object($query);
			if($aUserDetails)
				return $aUserDetails[0]['cnt'];
			else
				return 0;			
		}
		function get_single_user($id){
			$db = new db;
			$query = "select * from users where id=$id limit 1";
			$aUserDetails = $db->fetch_object($query);			
			if($aUserDetails)
				return $aUserDetails[0];
			else
				return '';
		}
		function is_user_exists($sUserName,$id=0){
			$db = new db;
			$query = "select id from users where sUserName='".$sUserName."'";
			if($id!=0){
				$query.= " and id!=".$id;
			}			
			$aUserDetails = $db->fetch_object($query);
			if($aUserDetails)
				return true;
			else
				return false;
		}
		function validate($aData,$sConfirmPassword=''){
			$error='';
			if(!isset($_POST))
				header("location:".ADMINURL.'/user.php');
			if(isset($aData['id'])){
				$id=$aData['id'];
			}
			else{
				$id=0;
			}
			if(empty($aData['sUserName'])){
				$error="Please Enter User Name";
				return $error;
			}
			else if( !ctype_alnum ($aData['sUserName'])){
				$error="Only alphanumeric character allowed for user name";
				return $error;
			}
			else if($this->is_user_exists($aData['sUserName'],$id)){
				$error="User alredy exists";
				return $error;
			}
			else if(empty($aData['sPassword'])){
				$error="Please Enter Password";
				return $error;
			}
			
			if(empty($sConfirmPassword)){
				$error="Please Enter Confirm Password";
				return $error;
			}
			else if($sConfirmPassword!=$aData['sPassword']){
				$error="Password and Confirm Password does not match";
				return $error;
			}
			
			else if(empty($aData['sFirstName'])){
				$error="Please Enter First Name";
				return $error;
			}
			else if(empty($aData['sLastName'])){
				$error="Please Enter Last Name";
				return $error;
			}
			else if(empty($aData['sCustEmail'])){
				$error="Please Enter Email Id";
				return $error;
			}
			if (!filter_var($aData['sCustEmail'], FILTER_VALIDATE_EMAIL)) {
			    $error="Please Enter Valid Email Id";
				return $error;
			}
			return $error;
		}
	}
