<?php 

    require_once(DOCUMENTROOT.'/config/connect.php');
    class Customer{
        var $sTableName='customer';      
        var $db;
		
    
        function __construct()
        {
            $this->db = new db;
        }
        function save_customer($aCustomerDetails){
            $aCustomerDetails['sCreatedOn']=time();
			$aCustomerDetails['sStartDate'] = strtotime($aCustomerDetails['sStartDate']);
			$aCustomerDetails['sEndDate'] = strtotime($aCustomerDetails['sEndDate']);
				if($aCustomerDetails['sStatus']=='Active' && $aCustomerDetails['sStartDate'] < time())
				{
						if($aCustomerDetails['sIpOption']=='IP')
						{
							$this->addto_htaccess($aCustomerDetails['sCustomerIP']);
						}
						if($aCustomerDetails['sIpOption']=='Multiple IP' || $aCustomerDetails['sIpOption']=='Subnet Mask')
						{
							$iprangevalue=str_replace(","," ",$aCustomerDetails['sCustomerIP']);
							$this->addto_htaccess($iprangevalue);
						}
						if($aCustomerDetails['sIpOption']=='In Range')
						{
							$arr_IP=explode("-",$aCustomerDetails['sCustomerIP']);
							for($i=0;$i<count($arr_IP);$i++)
							{
								$pattern="/(\d+).(\d+).(\d+).(\d+)/i";
								$replacement='$1.$2.$3.*';
								$strtValue_repl='$3';
								$ip_string_repl='$1.$2.';
								$iprangevalue= preg_replace($pattern, $replacement, $arr_IP[$i]);
								$ipstart[]=preg_replace($pattern, $strtValue_repl, $arr_IP[$i]);
								$ip_string=preg_replace($pattern, $ip_string_repl, $arr_IP[$i]);
							}
							for($j=$ipstart[0];$j<=$ipstart[1]; $j++)
							{
								$iprangevalue= $ip_string."".$j;
								$this->addto_htaccess($iprangevalue);
							}
							$ipstart="";
						}
				}
				else
				{
					if($aCustomerDetails['sIpOption']=='IP' || $aCustomerDetails['sIpOption']=='Subnet Mask')
						{
							$this->blacklistip_htaccess($aCustomerDetails['sCustomerIP']);
						}
						if($aCustomerDetails['sIpOption']=='Multiple IP' || $aCustomerDetails['sIpOption']=='Subnet Mask')
						{
							$iprangevalue=str_replace(","," ",$aCustomerDetails['sCustomerIP']);
							$this->blacklistip_htaccess($iprangevalue);
						}
						if($aCustomerDetails['sIpOption']=='In Range')
						{
							$arr_IP=explode("-",$aCustomerDetails['sCustomerIP']);
							for($i=0;$i<count($arr_IP);$i++)
							{
								$pattern="/(\d+).(\d+).(\d+).(\d+)/i";
								$replacement='$1.$2.$3.*';
								$strtValue_repl='$3';
								$ip_string_repl='$1.$2.';
								$iprangevalue= preg_replace($pattern, $replacement, $arr_IP[$i]);
								$ipstart[]=preg_replace($pattern, $strtValue_repl, $arr_IP[$i]);
								$ip_string=preg_replace($pattern, $ip_string_repl, $arr_IP[$i]);
							}
							for($j=$ipstart[0];$j<=$ipstart[1]; $j++)
							{
								$iprangevalue= $ip_string."".$j;
								$this->blacklistip_htaccess($iprangevalue);
							}
							$ipstart="";
						}
					
				}
			return $this->db->insert($this->sTableName,$aCustomerDetails);
        }
        function update_customer($aCustomerDetails){            
            //$db = new db;
			$userDet=$this->get_customer_by_id($aCustomerDetails['id']);
			$userDet1=array_shift($userDet);
			$aCustomerDetails['sStartDate'] = strtotime($aCustomerDetails['sStartDate']);
			$aCustomerDetails['sEndDate'] = strtotime($aCustomerDetails['sEndDate']);
			echo $aCustomerDetails['sIpOption']."****".$aCustomerDetails['sCustomerIP']."******".$userDet1['sIpOption']."*****".$userDet1['sCustomerIP'];
			$arr_IP="";
			$ipstart="";
			if($aCustomerDetails['sStatus']=='Active' && $aCustomerDetails['sStartDate'] < time()) {
				
				if($userDet1['sCustomerIP'] != $aCustomerDetails['sCustomerIP'])
				{
					
					if($userDet1['sIpOption']=='IP' )
					{
						$this->blacklistip_htaccess($userDet1['sCustomerIP']);
					}
					if($userDet1['sIpOption']=='Multiple IP' || $userDet1['sIpOption']=='Subnet Mask')
					{
						$iprangevalue=str_replace(","," ",$userDet1['sCustomerIP']);
						$this->blacklistip_htaccess($iprangevalue);
					}
					if($userDet1['sIpOption']=='In Range')
					{
						$arr_IP=explode("-",$userDet1['sCustomerIP']);
						
						for($i=0;$i<count($arr_IP);$i++)
						{
							$pattern="/(\d+).(\d+).(\d+).(\d+)/i";
							$replacement='$1.$2.$3.*';
							$strtValue_repl='$3';
							$ip_string_repl='$1.$2.';
							$iprangevalue= preg_replace($pattern, $replacement, $arr_IP[$i]);
							$ipstart[]=preg_replace($pattern, $strtValue_repl, $arr_IP[$i]);
							$ip_string=preg_replace($pattern, $ip_string_repl, $arr_IP[$i]);
						}
						for($j=$ipstart[0];$j<=$ipstart[1]; $j++)
						{
							$iprangevalue= $ip_string."".$j;
							$this->blacklistip_htaccess($iprangevalue);
						}
						$ipstart="";
					}
				}
				if($aCustomerDetails['sIpOption']=='IP' )
				{
					$this->addto_htaccess($aCustomerDetails['sCustomerIP']);
				}
				if($aCustomerDetails['sIpOption']=='Multiple IP' || $aCustomerDetails['sIpOption']=='Subnet Mask')
				{
					$iprangevalue=str_replace(","," ",$aCustomerDetails['sCustomerIP']);
					$this->addto_htaccess($iprangevalue);
				}
				if($aCustomerDetails['sIpOption']=='In Range')
				{
					$arr_IP=explode("-",$aCustomerDetails['sCustomerIP']);
					for($i=0;$i<count($arr_IP);$i++)
					{
						$pattern="/(\d+).(\d+).(\d+).(\d+)/i";
						$replacement='$1.$2.$3.*';
						$strtValue_repl='$3';
						$ip_string_repl='$1.$2.';
						$iprangevalue= preg_replace($pattern, $replacement, $arr_IP[$i]);
						$ipstart[]=preg_replace($pattern, $strtValue_repl, $arr_IP[$i]);
						$ip_string=preg_replace($pattern, $ip_string_repl, $arr_IP[$i]);
					}
					for($j=$ipstart[0];$j<=$ipstart[1]; $j++)
					{
						$iprangevalue= $ip_string."".$j;
						$this->addto_htaccess($iprangevalue);
					}
					$ipstart="";
				}
			}
			else
			{
				/*if(strpos($aCustomerDetails['sCustomerIP'],",")===false)
				{
					$this->blacklistip_htaccess($aCustomerDetails['sCustomerIP']);
				}
				else 
				{
					$csutIP=str_replace(","," ",$aCustomerDetails['sCustomerIP']);
					$this->blacklistip_htaccess($csutIP);
				}*/
				if($userDet1['sCustomerIP']  != $aCustomerDetails['sCustomerIP'])
				{
					if($userDet1['sIpOption']=='IP' )
					{
						$this->blacklistip_htaccess($userDet1['sCustomerIP']);
					}
					if($userDet1['sIpOption']=='Multiple IP' || $userDet1['sIpOption']=='Subnet Mask')
					{
						$iprangevalue=str_replace(","," ",$userDet1['sCustomerIP']);
						$this->blacklistip_htaccess($iprangevalue);
					}
					if($userDet1['sIpOption']=='In Range')
					{
						$arr_IP=explode("-",$userDet1['sCustomerIP']);
						for($i=0;$i<count($arr_IP);$i++)
						{
							$pattern="/(\d+).(\d+).(\d+).(\d+)/i";
							$replacement='$1.$2.$3.*';
							$strtValue_repl='$3';
							$ip_string_repl='$1.$2.';
							$iprangevalue= preg_replace($pattern, $replacement, $arr_IP[$i]);
							$ipstart[]=preg_replace($pattern, $strtValue_repl, $arr_IP[$i]);
							$ip_string=preg_replace($pattern, $ip_string_repl, $arr_IP[$i]);
						}
						for($j=$ipstart[0];$j<=$ipstart[1]; $j++)
						{
							$iprangevalue= $ip_string."".$j;
							$this->blacklistip_htaccess($iprangevalue);
						}
						$ipstart="";
					}
				}
				if($aCustomerDetails['sIpOption']=='IP' )
				{
					$this->blacklistip_htaccess($aCustomerDetails['sCustomerIP']);
				}
				if($aCustomerDetails['sIpOption']=='Multiple IP' || $aCustomerDetails['sIpOption']=='Subnet Mask')
				{
					$iprangevalue=str_replace(","," ",$aCustomerDetails['sCustomerIP']);
					$this->blacklistip_htaccess($iprangevalue);
				}
				if($aCustomerDetails['sIpOption']=='In Range')
				{
					$arr_IP=explode("-",$aCustomerDetails['sCustomerIP']);
					for($i=0;$i<count($arr_IP);$i++)
					{
						$pattern="/(\d+).(\d+).(\d+).(\d+)/i";
						$replacement='$1.$2.$3.*';
						$strtValue_repl='$3';
						$ip_string_repl='$1.$2.';
						$iprangevalue= preg_replace($pattern, $replacement, $arr_IP[$i]);
						$ipstart[]=preg_replace($pattern, $strtValue_repl, $arr_IP[$i]);
						$ip_string=preg_replace($pattern, $ip_string_repl, $arr_IP[$i]);
					}
					for($j=$ipstart[0];$j<=$ipstart[1]; $j++)
					{
						$iprangevalue= $ip_string."".$j;
						$this->blacklistip_htaccess($iprangevalue);
					}
					$ipstart="";
				}
				
			}
			return $this->db->update($this->sTableName,$aCustomerDetails);
        }
        function change_status($aCustomerDetails){            
            //$db = new db;
            //$aCustomerDetails['sEndDate'] = strtotime($aCustomerDetails['sEndDate']);
            return $this->db->update($this->sTableName,$aCustomerDetails);
        }
        function get_all_customer($limit=null,$orderby,$orderbytype){
            //$db = new db;
			$query = "select * from ".$this->sTableName." order by $orderby $orderbytype $limit";
            $aUserDetails = $this->db->fetch_object($query);
			return (object)$aUserDetails;
			
        }
		function get_customers_by_exp($date){
            //$db = new db;
			$query = "select * from ".$this->sTableName." where sEndDate <=$date AND sStatus='Active'";
            $aUserDetails = $this->db->fetch_object($query);
			return (object)$aUserDetails;
			
        }
		
		function get_customers_by_start($date){
            //$db = new db;
			$query = "select * from ".$this->sTableName." where sStartDate=$date";
            $aUserDetails = $this->db->fetch_object($query);
			return (object)$aUserDetails;
			
        }
		
        function get_customer_by_id($id=0){
            //$db = new db;
            $query = "select * from ".$this->sTableName." where id=$id limit 1";
            $aUserDetails = $this->db->fetch_object($query);
            return $aUserDetails;
        }
        
        function record_count($where=null){
            //$db = new db;
            $query = "select count(*) as cnt from ".$this->sTableName." $where";
            $aUserDetails = $this->db->fetch_object($query);
            if($aUserDetails)
                return $aUserDetails[0]['cnt'];
            else
                return 0;           
        }
        function get_single_customer($id){
            //$db = new db;
            $query = "select * from ".$this->sTableName." where id=$id limit 1";
            $aUserDetails = $this->db->fetch_object($query);            
            if($aUserDetails)
                return $aUserDetails[0];
            else
                return '';
        }
        function is_customer_exists($sAuthenticateName,$id=0){
            //$db = new db;
            $query = "select id from customer where sCustName='".$sAuthenticateName."'";
            if($id!=0){
                $query.= " and id!=".$id;
            }           
            $aCustomerDetails = $this->db->fetch_object($query);
            if($aCustomerDetails)
                return true;
            else
                return false;
        }
        function is_valid_date($sDate){
            $date = DateTime::createFromFormat('m/d/Y', $sDate);
            $date_errors = DateTime::getLastErrors();
            if ($date_errors['warning_count'] + $date_errors['error_count'] > 0) {
                return false;
            }
            return true;
        }
		function blacklistip_htaccess($ip)
        {
            $htaccess = ANATOMEDIADOCUMENTROOT.'/.htaccess';
			$hash_exists_deny='';
            if(file_exists($htaccess))
            {
                $contents = file_get_contents($htaccess, TRUE);
                // Lets search the htaccess file to see if there is already a ban in place.
				$exists_deny = stripos($contents, "$ip");
            }
			if($exists_deny)
            {
                $searchstr="Allow from {$ip}";
                $replacestr="";
                // again block the ip
                $contents=str_replace($searchstr ,$replacestr,$contents);
				file_put_contents($htaccess, $contents) OR exit('Cannot append rule to '.$htaccess);
            }
            return true;
        }
		
		function addto_htaccess($ip)
        {
		
            $htaccess = ANATOMEDIADOCUMENTROOT.'/.htaccess';
			$hash_exists_deny='';
            if(file_exists($htaccess ))
            {
                $contents = file_get_contents($htaccess, TRUE);
                // Lets search the htaccess file to see if there is already a ban in place.
                $exists_deny = !stripos($contents, "$ip");
                if(!(strpos($contents, 'Allow from {$ip}')===false)){
                    $hash_exists_deny=1;
                }
            }
            if($hash_exists_deny)
            {
                $searchstr="Allow from {$ip}";
                $replacestr=" ";
                // again block the ip
                $contents=str_ireplace($searchstr ,$replacestr,$contents);
                file_put_contents($htaccess, $contents) OR exit('Cannot append rule to '.$htaccess);
            }
            else if($exists_deny)
            {
				// Here we just pull some details we can use later.
				$date   = date('Y-m-d H:i:s');
				$uri    = htmlspecialchars($_SERVER['REQUEST_URI'], ENT_QUOTES);
				$agent  = htmlspecialchars($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES);
				//$agent  = str_replace(array("\n", "\r"), '', $agent);
				//$ban =  "\n\r\n\r # The IP below was banned on $date for trying to access {$uri}\n\r";
				//$ban .= "\n\r # Agent: {$agent}\n\r";
				$ban = "\nAllow from {$ip}";
				file_put_contents($htaccess, $ban, FILE_APPEND) OR exit('Cannot append rule to '.$htaccess);
            }
            return true;
        }
		
        function validate($aData,$sConfirmPassword=''){
            $error='';
            if(!isset($_POST))
                header("location:".ADMINURL.'/customer.php'); 
            if(isset($aData['id'])){
                $id=$aData['id'];
            }
            else{
                $id=0;
            }
            if(empty($aData['sCustName'])){
                $error="Please Enter Customer Name";
                return $error;
            }
                  
            else if($this->is_customer_exists($aData['sCustName'],$id)){
                $error="Customer already exists";
                return $error;
            }
            else if(empty($aData['sCustEmail'])){
                $error="Please Enter Email Id";
                return $error;
            }
            else if (!filter_var($aData['sCustEmail'], FILTER_VALIDATE_EMAIL)) {
                $error="Please Enter Valid Email Id";
                return $error;
            }
            return $error;
        }
    }
