<?php
	require_once(DOCUMENTROOT.'/config/connect.php');
	class AdminCommon{
		function get_date_format($timestamp){
			if($timestamp)
				return date('m/d/Y H:i:s',$timestamp);
			else
				return '';
		}
		function onlydate($timestamp){
			if($timestamp)
				return date('m/d/Y',$timestamp);
			else
				return '';
		}
		function count_failed_request($iCustomerId,$sCustomerIP){
			$db = new Db;
			$query = "select count(*) as cnt from ".'activities'." where iErrorCodeReturned>0 and iCustomerId=".$iCustomerId." and sCustomerIP='".$sCustomerIP."'";
			$aBlacklistDetails = $db->fetch_object($query);
			if($aBlacklistDetails)
				return $aBlacklistDetails[0]['cnt'];
			else
				return 0;
		}
		function do_log($iUserActionId,$iModifyForUserId,$iModifiedTye=1){			
			$aData['iUserActionId']=$iUserActionId;
			$aData['iModifiedByUserId']=$_SESSION['userID'];
			$aData['iModifyForUserId']=$iModifyForUserId;
			$aData['iModifiedTye']=$iModifiedTye;
			$aData['sModifyDateTime']=time();			
			$db = new db;
			return $db->insert('user_logs',$aData);		
		}
		
	}
?>