<?php   
    require_once(DOCUMENTROOT.'/config/connect.php');
	set_time_limit(0);
    class Log{
        var $sTableName='logs';     
        var $db;
        function __construct()
        {
            $this->db = new db;
        }   
        function get_all_logs($limit=null,$where=null){
            //$db = new db;
            //$query = "select l.*,v.sCustName,a.sAssetName,a.sCustomerIP from ".$this->sTableName." l ,customer v,activities a where l.iCustomerId=v.id and l.iActivityId=a.id $limit ";
            $query = " select * from logdetails $where ". $limit;          
            $aLogDetails = $this->db->fetch_object($query);
            return $aLogDetails;
        }
        function record_count($where=null){
            //$db = new db;
            $query = "select count(*) as cnt from ".'logdetails'." $where";
            $aLogDetails = $this->db->fetch_object($query);
            if($aLogDetails)
                return $aLogDetails[0]['cnt'];
            else
                return 0;           
        }
        function get_single_log($id){
            //$db = new db;
            $query = "select * from ".$this->sTableName." where id=$id limit 1";
            $aLogDetails = $this->db->fetch_object($query);         
            if($aLogDetails)
                return $aLogDetails[0];
            else
                return '';
        }
        function get_activity($activityId){
            //$db = new db;
            $query = "select * from ".'activities'." where id=$activityId limit 1";
            $aActivityDetails = $this->db->fetch_object($query);            
            if($aActivityDetails){
                $str="Customer ID:".$aActivityDetails[0]['iCustomerID']."; Customer Password:".$aActivityDetails[0]['sCustomerPassword']."; Base64 Hashkey: ".$aActivityDetails[0]['sMD5hash'];
                return $str;
            }
            else
                return '';
        }
        function get_completion_data($str=''){
            $aData = unserialize($str);
            $implode_str = '';
            foreach($aData as $key=>$value){
                $implode_str.="$key -> ".$value." ;";
            }
            return $implode_str;
        }
        function update_blacklist($aBlacklistDetails){          
            //$db = new db;         
            return $this->db->update($this->sTableName,$aBlacklistDetails);
        }
		function get_activity_start_time($activity){          
            //$db = new db;        
			$query = "select sDateTime from ".'activities'." where id=$activity LIMIT 1";
            $aActivityStartDetails = $this->db->fetch_object($query); 
			if($aActivityStartDetails)
                return $aActivityStartDetails[0]['sDateTime'];
            else
                return '';
        }
		
        function partner_dropdown($id=0){
            $query = "Select id,sCustName from customer order by sCustName asc";
            $aCustomerDetails = $this->db->fetch_object($query);
            $option = "<option value=''>All...</option>";
            if($aCustomerDetails){
                foreach($aCustomerDetails as $aCustomerDetail){
                    if($id==$aCustomerDetail['id'])
                        $selected="selected='selected'";
                    else
                        $selected='';
                    $option.="<option $selected value='".$aCustomerDetail['id']."'>".$aCustomerDetail['sCustName']."</option>";
                }
            }
            return $option;
        }
        function error_code_dropdown($id=0){
            $aId = explode(',',$id);
            $query = "Select iErrorCode,sName from error_codes";
            $aErrorDetails = $this->db->fetch_object($query);       
            if($aId[0]!='')
                $option = "<option  value=''>All...</option>";
            else
                $option = "<option selected='selected' value=''>All...</option>";
            if($aErrorDetails){
                foreach($aErrorDetails as $aErrorDetail){
                    if(in_array($aErrorDetail['iErrorCode'],$aId))
                        $selected="selected='selected'";
                    else
                        $selected='';
                    $option.="<option $selected value='".$aErrorDetail['iErrorCode']."'>".$aErrorDetail['sName']."</option>";
                }
            }
            return $option;
        }
        
    }
