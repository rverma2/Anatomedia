<?php
class Pagination{
	function selectboxforpage($lastpage,$page){
		$option = '';
		for($i=1;$i<=$lastpage;$i++){
			if($page==$i)
				$selected = "selected='selected'";
			else
				$selected='';
			$option.= "<option $selected value='$i'>".$i."</option>";
		}
		return $option;
	}
	function selectboxforperpage($per_page){
		$option = '';
		//echo $per_page;//die;
		for($i=10;$i<=50;$i=$i+10){
			if($per_page==$i)
				$selected = "selected='selected'";
			else
				$selected='';
			$option.= "<option $selected value='$i'>".$i."</option>";
		}
		return $option;
	}
	function paginate($per_page = 10, $page = 1, $url = '', $total){

		$adjacents = "2";		
		$page = ($page == 0 ? 1 : $page);
		$start = ($page - 1) * $per_page;
		$redirect_url = $url.$page;
		$prev = $page - 1;
		$next = $page + 1;
		$lastpage = ceil($total/$per_page);
		$lpm1 = $lastpage - 1;

		$pagination = "";
		//echo $lastpage < 7 + ($adjacents * 2);die;
		if($lastpage > 1)
		{
			$pagination .= "<ul class='pagination'>";
			$pagination .= "<li class='details1'><span class='lispan'>Per Page</span><select onchange='redirectforperpage(\"$url\",this.value)' style='width:60px;float:right'>".$this->selectboxforperpage($per_page)."</select></li>";
			$pagination .= "<li class='details1'><span class='lispan'>Page</span><select onchange='redirectforpage(\"$url\",this.value,$per_page)' style='width:60px;float:right;'>".$this->selectboxforpage($lastpage,$page)."</select></li>";
			$pagination .= "<li class='details'>Page $page of $lastpage</li>";
			if($page>1){
				$pagination.= "<li><a href='{$url}$prev'>Previous</a></li>";
			}else{
				$pagination.= "<li><a class='current'>Previous</a></li>";
			}
			if ($lastpage < (7 + ($adjacents * 2)))
			{
				for ($counter = 1; $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<li><a class='current'>$counter</a></li>";
					else
						$pagination.= "<li><a href='{$url}$counter'>$counter</a></li>";
				}
			}
			elseif($lastpage > (5 + ($adjacents * 2)))
			{
				if($page < (1 + ($adjacents * 2)))
				{
					for ($counter = 1; $counter < (4 + ($adjacents * 2)); $counter++)
					{
						if ($counter == $page)
							$pagination.= "<li><a class='current'>$counter</a></li>";
						else
							$pagination.= "<li><a href='{$url}$counter'>$counter</a></li>";
					}
					$pagination.= "<li class='dot'>...</li>";
					$pagination.= "<li><a href='{$url}$lpm1'>$lpm1</a></li>";
					$pagination.= "<li><a href='{$url}$lastpage'>$lastpage</a></li>";
				}
			elseif(($lastpage - ($adjacents * 2)) > $page && $page > ($adjacents * 2))
			{
				$pagination.= "<li><a href='{$url}1'>1</a></li>";
				$pagination.= "<li><a href='{$url}2'>2</a></li>";
				$pagination.= "<li class='dot'>...</li>";
				for ($counter = ($page - $adjacents); $counter <= ($page + $adjacents); $counter++)
				{
					if ($counter == $page)
						$pagination.= "<li><a class='current'>$counter</a></li>";
					else
						$pagination.= "<li><a href='{$url}$counter'>$counter</a></li>";
				}
				$pagination.= "<li class='dot'>..</li>";
				$pagination.= "<li><a href='{$url}$lpm1'>$lpm1</a></li>";
				$pagination.= "<li><a href='{$url}$lastpage'>$lastpage</a></li>";
			}
			else
			{
				$pagination.= "<li><a href='{$url}1'>1</a></li>";
				$pagination.= "<li><a href='{$url}2'>2</a></li>";
				$pagination.= "<li class='dot'>..</li>";
				for ($counter = ($lastpage - (2 + ($adjacents * 2))); $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<li><a class='current'>$counter</a></li>";
					else
						$pagination.= "<li><a href='{$url}$counter'>$counter</a></li>";
				}
			}
		}

		if ($page < $counter - 1){
			$pagination.= "<li><a href='{$url}$next'>Next</a></li>";
			$pagination.= "<li><a href='{$url}$lastpage'>Last</a></li>";
		}else{
			$pagination.= "<li><a class='current'>Next</a></li>";
			 $pagination.= "<li><a class='current'>Last</a></li>";
		}
		$pagination.= "</ul>\n";
		}
		return $pagination;
	}
}//class
?>