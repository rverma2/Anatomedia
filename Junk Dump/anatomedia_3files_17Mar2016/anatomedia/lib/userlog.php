<?php   
    require_once(DOCUMENTROOT.'/config/connect.php');
    class UserLog{
        var $sTableName='userlogdetails';       
        var $db;
        function __construct()
        {
            $this->db = new db;
        }   
        function get_all_user_logs($limit=null,$where=null){
            //$db = new db;
            //$query = "select l.*,v.sCustName,a.sAssetName,a.sCustomerIP from ".$this->sTableName." l ,customer v,activities a where l.iCustomerId=v.id and l.iActivityId=a.id $limit ";
            $query = " select * from userlogdetails $where ". $limit;           
            $aLogDetails = $this->db->fetch_object($query);
            return $aLogDetails;
        }
        function record_count($where=null){
            //$db = new db;
            $query = "select count(*) as cnt from ".$this->sTableName." $where";
            $aLogDetails = $this->db->fetch_object($query);
            if($aLogDetails)
                return $aLogDetails[0]['cnt'];
            else
                return 0;           
        }
        function get_single_log($id){
            //$db = new db;
            $query = "select * from ".$this->sTableName." where id=$id limit 1";
            $aLogDetails = $this->db->fetch_object($query);         
            if($aLogDetails)
                return $aLogDetails[0];
            else
                return '';
        }
        function update_blacklist($aBlacklistDetails){          
            //$db = new db;         
            return $this->db->update($this->sTableName,$aBlacklistDetails);
        }
        function partner_dropdown($id=0){
            $query = "Select id,sCustName from customer order by sCustName asc";
            $aCustomerDetails = $this->db->fetch_object($query);
            $option = "<option value=''>All...</option>";
            if($aCustomerDetails){
                foreach($aCustomerDetails as $aCustomerDetail){
                    if($id==$aCustomerDetail['id'])
                        $selected="selected='selected'";
                    else
                        $selected='';
                    $option.="<option $selected value='".$aCustomerDetail['id']."'>".$aCustomerDetail['sCustName']."</option>";
                }
            }
            return $option;
        }
        function error_code_dropdown($id=0){
            $query = "Select iErrorCode,sName from error_codes";
            $aErrorDetails = $this->db->fetch_object($query);
            $option = "<option value=''>All...</option>";
            if($aErrorDetails){
                foreach($aErrorDetails as $aErrorDetail){
                    if($id==$aErrorDetail['iErrorCode'])
                        $selected="selected='selected'";
                    else
                        $selected='';
                    $option.="<option $selected value='".$aErrorDetail['iErrorCode']."'>".$aErrorDetail['sName']."</option>";
                }
            }
            return $option;
        }
        function user_dropdown($id=0){
            $query = "Select id,concat(sFirstName,' ',sLastName) as sUserName from users order by sFirstName asc";
            $aUserDetails = $this->db->fetch_object($query);
            $option = "<option value=''>All...</option>";
            if($aUserDetails){
                foreach($aUserDetails as $aUserDetail){
                    if($id==$aUserDetail['id'])
                        $selected="selected='selected'";
                    else
                        $selected='';
                    $option.="<option $selected value='".$aUserDetail['id']."'>".$aUserDetail['sUserName']."</option>";
                }
            }
            return $option;
        }
        function action_dropdown($id=0){
            $query = "Select id,sAction from user_actions order by sAction asc";
            $aActionDetails = $this->db->fetch_object($query);
            $option = "<option value=''>All...</option>";
            if($aActionDetails){
                foreach($aActionDetails as $aActionDetail){
                    if($id==$aActionDetail['id'])
                        $selected="selected='selected'";
                    else
                        $selected='';
                    $option.="<option $selected value='".$aActionDetail['id']."'>".$aActionDetail['sAction']."</option>";
                }
            }
            return $option;
        }

        function do_report_log($report,$val)
        {
            $aData['iUserActionId']=$report;
            $aData['iModifiedByUserId']=$_SESSION['userID'];
            $aData['iModifyForUserId']=$_SESSION['userID'];
            $aData['iModifiedTye']=1;
            $aData['sModifyDateTime']=time();
            $aData['sUserActionDetails']=$val;      
            $db = new db;
            return $db->insert('user_logs',$aData); 
        }
        
    }
