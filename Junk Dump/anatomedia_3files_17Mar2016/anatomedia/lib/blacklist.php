<?php	
	require_once(DOCUMENTROOT.'/config/connect.php');
	class Blacklist{
		var $sTableName='blacklists';		
		var $db;
		function __construct()
		{
			$this->db = new db;
		}	
		function get_all_blacklist($limit=null,$orderby,$ordertype){
			//$db = new db;
			$query = "select b.*,v.sCustName,vi.iNosFailedRequest from ".$this->sTableName." b inner join customer v on b.iCustomerId=v.id inner join customer_ip vi on b.sCustomerIP=vi.sIpAddress where b.iCustomerId=vi.iCustomerId  order by $orderby $ordertype $limit ";
			$aBlacklistDetails = $this->db->fetch_object($query);
			return $aBlacklistDetails;
		}
		function record_count($where=null){
			//$db = new db;
			$query = "select count(*) as cnt from ".$this->sTableName."$where";
			$aBlacklistDetails = $this->db->fetch_object($query);
			if($aBlacklistDetails)
				return $aBlacklistDetails[0]['cnt'];
			else
				return 0;			
		}
		function get_single_blacklist($id){
			//$db = new db;
			$query = "select * from ".$this->sTableName." where id=$id limit 1";
			$aUserDetails = $this->db->fetch_object($query);			
			if($aUserDetails)
				return $aUserDetails[0];
			else
				return '';
		}
		function update_blacklist($aBlacklistDetails){			
			//$db = new db;			
			return $this->db->update($this->sTableName,$aBlacklistDetails);
		}	
		
	}
