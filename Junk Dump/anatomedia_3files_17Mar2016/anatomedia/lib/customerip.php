<?php	
	require_once(DOCUMENTROOT.'/config/connect.php');
	class CustomerIp{
		var $sTableName='customer_ip';		
		var $db;
		function __construct()
		{
			$this->db = new db;
		}
		function save_customer_ip($aCustomerDetails){				
			return $this->db->insert($this->sTableName,$aCustomerDetails);
		}
		function update_customer_ip($aCustomerDetails){		
	
			return $this->db->update($this->sTableName,$aCustomerDetails);
		}
		function reset_customer_ip($aCustomerDetails){
			$query = "Update customer_ip set iNosFailedFromLast=0 where iCustomerId=".$aCustomerDetails['iCustomerId']." and sIpAddress='".$aCustomerDetails['sIpAddress']."'";
			return $this->db->query($query);
		}
		function get_all_ip($where){
			//$db = new db;
			$query = "select * from ".$this->sTableName." $where";
			$aCustomerip = $this->db->fetch_object($query);
			return $aCustomerip;
		}
		function get_one_ip($id=0,$customerid=0){
			//$db = new db;
			$query = "select * from ".$this->sTableName." where id=$id and iCustomerId=$customerid limit 1";
			$aUserDetails = $this->db->fetch_object($query);
			return $aUserDetails;
		}
		function delete_customer_ip($id=0,$customerid=0){
			//$db = new db;
			echo $query = "delete from ".$this->sTableName." where id=$id and iCustomerId=$customerid";
			return $this->db->query($query);
		}
		function get_all_failed_request_customer($limit=null){
			//$db = new db;
			$query = "select * from ".$this->sTableName." where iNoOfFailedRequest>0 order by iNoOfFailedRequest desc $limit ";
			$aUserDetails = $this->db->fetch_object($query);
			return $aUserDetails;
		}
		function failed_request_count($where=null){
			//$db = new db;
			$query = "select count(*) as cnt from ".$this->sTableName." $where";
			$aUserDetails = $this->db->fetch_object($query);
			if($aUserDetails)
				return $aUserDetails[0]['cnt'];
			else
				return 0;			
		}
		function record_count($where=null){
			//$db = new db;
			$query = "select count(*) as cnt from ".$this->sTableName." $where";
			$aUserDetails = $this->db->fetch_object($query);
			if($aUserDetails)
				return $aUserDetails[0]['cnt'];
			else
				return 0;			
		}
		function get_single_customer($id){
			//$db = new db;
			$query = "select * from ".$this->sTableName." where id=$id limit 1";
			$aUserDetails = $this->db->fetch_object($query);			
			if($aUserDetails)
				return $aUserDetails[0];
			else
				return '';
		}
		function is_customer_exists($sAuthenticateName,$id=0){
			//$db = new db;
			$query = "select id from customer where sCustName='".$sAuthenticateName."'";
			if($id!=0){
				$query.= " and id!=".$id;
			}			
			$aCustomerDetails = $this->db->fetch_object($query);
			if($aCustomerDetails)
				return true;
			else
				return false;
		}
		function is_valid_date($sDate){
			$date = DateTime::createFromFormat('m/d/Y', $sDate);
			$date_errors = DateTime::getLastErrors();
			if ($date_errors['warning_count'] + $date_errors['error_count'] > 0) {
				return false;
			}
			return true;
		}
		function validate($aData,$sConfirmPassword=''){
			$error='';
			if(!isset($_POST))
				header("location:".ADMINURL.'/customer.php');	
			if(isset($aData['id'])){
				$id=$aData['id'];
			}
			else{
				$id=0;
			}
			if(empty($aData['sCustName'])){
				$error="Please Enter Authentication Name";
				return $error;
			}
			else if( !ctype_alnum ($aData['sCustName'])){
				$error="Only alphanumeric character allowed for authentication name";
				return $error;
			}			
			else if($this->is_customer_exists($aData['sCustName'],$id)){
				$error="Customer alredy exists";
				return $error;
			}
			else if(empty($aData['sPassword'])){
				$error="Please Enter Password";
				return $error;
			}
			
			if(empty($sConfirmPassword)){
				$error="Please Enter Confirm Password";
				return $error;
			}
			else if($sConfirmPassword!=$aData['sPassword']){
				$error="Password and Confirm Password does not match";
				return $error;
			}
					
			else if(empty($aData['sCustEmail'])){
				$error="Please Enter Email Id";
				return $error;
			}
			else if (!filter_var($aData['sCustEmail'], FILTER_VALIDATE_EMAIL)) {
			    $error="Please Enter Valid Email Id";
				return $error;
			}
			else if(empty($aData['sHashKey'])){
				$error="Please Enter Hash Key String";
				return $error;
			}
			else if(empty($aData['sCustomerIP'])){
				$error="Please Enter Server IP";
				return $error;
			}
			else if(!filter_var($aData['sCustomerIP'],FILTER_VALIDATE_IP)) {
				$error="Please Enter Valid Server IP";
				return $error;
			}
			else if(empty($aData['iNosFailedRequestAllowed'])){
				$error="Please Enter Failed Request Allowed";
				return $error;
			}
			else if(empty($aData['sExpiryDate'])){
				$error="Please Enter Expiry Date";
				return $error;
			}
			else if(!$this->is_valid_date($aData['sExpiryDate'])){
				$error="Please Enter Valid Expiry Date(mm/dd/yyyy)";
				return $error;
			}
			return $error;
		}
	}
