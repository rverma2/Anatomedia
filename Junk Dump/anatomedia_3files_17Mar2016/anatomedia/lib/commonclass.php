<?php
    require_once(DOCUMENTROOT.'/config/connect.php');
    class Common{       
        function save_request($aRequest){
			//// commented for SLB TEST
			//if($aRequest['sCustomerIP'] == $_SERVER['REMOTE_ADDR']) {
            $db = new db;   
            $error = $this->check_for_error($aRequest);
            $aRequest['iErrorCodeReturned'] = 0;
            if($error){
                $aRequest['iErrorCodeReturned']=$error['error_code'];
                //$sError = $this->get_error_name($error['error_code']);
                $msg =  urlencode(LAUNCHURL.'/#.html/'.$error['error_code']);
                //check for black list
                
                if($error['error_code']==9){
                    if(!($this->validate_customer_name($aRequest['sCustomerName']  || $this->validate_customerid($iCustomerid)))) 
                    {
                        $aRequest['iCustomerId']=INVALIDCUSTOMER;
                        // Entry in customer table for blacklisting the unidentified parter
                        $aCustomerIPRow = $db->query("select * from customer_ip where iCustomerId=".INVALIDCUSTOMER." and sIpAddress='".$aRequest['sCustomerIP']."'");
                        
                        if(mysql_num_rows($aCustomerIPRow)>0)
                        {
                            $query = "Update customer_ip set iNosFailedFromLast=iNosFailedFromLast+1,iNosFailedRequest=iNosFailedRequest+1 where iCustomerId=".INVALIDCUSTOMER." and sIpAddress='".$aRequest['sCustomerIP']."'";
                            $db->query($query);  
                            $query = "select iNosFailedRequestAllowed from customer where id=".INVALIDCUSTOMER;
                            $aCustomerRow = $db->fetch_object($query);                        
                            $query = "select iNosFailedFromLast from customer_ip where sIpAddress='".$aRequest['sCustomerIP']."' and iCustomerId='".INVALIDCUSTOMER."'";
                            $aCustomerIpRow = $db->fetch_object($query);
                            if($aCustomerRow)
                            {                            
                                if($aCustomerIpRow[0]['iNosFailedFromLast']>=$aCustomerRow[0]['iNosFailedRequestAllowed'])
                                {//black list server ip
                                    //if ip already present in blacklist
									if(!empty($aRequest['sCustomerIP'])){
	                                    $query = "select id from blacklists where sCustomerIP='".$aRequest['sCustomerIP']."' and iCustomerId='".INVALIDCUSTOMER."'";
	                                    $aBlacklistRow = $db->fetch_object($query);
	                                    if($aBlacklistRow)
	                                    {//update
	                                        $aBlacklist = array(
	                                                            'id'=>$aBlacklistRow[0]['id'],
	                                                            'status'=>'block',
	                                                            'sBlackListedOn'=>time()
	                                                        );
	                                        $db->update('blacklists',$aBlacklist);
	                                    }
	                                    else
	                                    {//insert
	                                        $aBlacklist = array(
	                                                            'iCustomerId'=>INVALIDCUSTOMER,
	                                                            'sCustomerIP'=>$aRequest['sCustomerIP'],
	                                                            'status'=>'block',
	                                                            'sBlackListedOn'=>time()
	                                                        );
	                                        $db->insert('blacklists',$aBlacklist);
	                                    }
	                                    $this->blacklistip_htaccess($aRequest['sCustomerIP']); // add the blacklisted ip in .htaccess file
									}
                                }
                            }
                        }
                        else
                        {
                            $aUnidentifiedPart=array('sIpAddress'=>$aRequest['sCustomerIP'],'iCustomerId'=>INVALIDCUSTOMER);
                            $db->insert('customer_ip',$aUnidentifiedPart);
                        }
                    }
                    $iTotalInvalidIpCount = $this->get_invalid_ip_count($aRequest['sCustomerIP']);
                    if($iTotalInvalidIpCount==INVALIDIPCOUNT)//exit
                        return '';
                }
                else
                {
                    $query = "Update customer_ip set iNosFailedFromLast=iNosFailedFromLast+1,iNosFailedRequest=iNosFailedRequest+1 where iCustomerId=".$aRequest['iCustomerId']." and sIpAddress='".$aRequest['sCustomerIP']."'";
                    $db->query($query);     
                    if($error['error_code']!=7)
                    {
                        $query = "select iNosFailedRequestAllowed from customer where id=".$aRequest['iCustomerId'];
                        $aCustomerRow = $db->fetch_object($query);                        
                        $query = "select iNosFailedFromLast from customer_ip where sIpAddress='".$aRequest['sCustomerIP']."' and iCustomerId='".$aRequest['iCustomerId']."'";
                        $aCustomerIpRow = $db->fetch_object($query);
                        if($aCustomerRow)
                        {                            
                            if($aCustomerIpRow[0]['iNosFailedFromLast']>=$aCustomerRow[0]['iNosFailedRequestAllowed'])
                            {//black list server ip
                                //if ip already present in blacklist
                                $query = "select id from blacklists where sCustomerIP='".$aRequest['sCustomerIP']."' and iCustomerId='".$aRequest['iCustomerId']."'";
                                $aBlacklistRow = $db->fetch_object($query);
                                if($aBlacklistRow)
                                {//update
                                    $aBlacklist = array(
                                                        'id'=>$aBlacklistRow[0]['id'],
                                                        'status'=>'block',
                                                        'sBlackListedOn'=>time()
                                                    );
                                    $db->update('blacklists',$aBlacklist);
                                }
                                else
                                {//insert
                                    $aBlacklist = array(
                                                        'iCustomerId'=>$aRequest['iCustomerId'],
                                                        'sCustomerIP'=>$aRequest['sCustomerIP'],
                                                        'status'=>'block',
                                                        'sBlackListedOn'=>time()
                                                    );
                                    $db->insert('blacklists',$aBlacklist);
                                }
								if(!empty($aRequest['sCustomerIP'])){
									$this->blacklistip_htaccess($aRequest['sCustomerIP']); // add the blacklisted ip in .htaccess file
								}
                            }
                        }
                    }//if
                }
                //====================
            }
            else{//generate asset url               
                //$sHtmlFileName = $this->get_random_file_name();
                $randomstring=substr(str_shuffle('abcdefghijklmnopqrstuvwxyz'),0,5);
                $str = time().$randomstring;
                $sHtmlFileName = str_shuffle($str).'.html';
                $sAssetUrl = LAUNCHURL.'/'.$sHtmlFileName.'/0';
                $aRequest['sAssetUrl']=$sAssetUrl;
                $msg = urlencode($sAssetUrl);               
            }
           // echo "i am here....";
           
            $db->insert('activities',$aRequest);
            $iInserId = mysql_insert_id();
            //insert in log table
            if($this->validate_customer_name($aRequest['sCustomerName']  || $this->validate_customerid($iCustomerid)))
            {
                $aLog = array(
                        'iActivityId'=>$iInserId,
                        'iCustomerID'=>$aRequest['iCustomerId'],                        
                        'iErrorCodeReturned'=>$aRequest['iErrorCodeReturned'],
                        'sDateTime'=>time()
                    );
            }
            else
            {
                 $aLog = array(
                        'iActivityId'=>$iInserId,
                        'iCustomerID'=>INVALIDCUSTOMER,                        
                        'iErrorCodeReturned'=>$aRequest['iErrorCodeReturned'],
                        'sDateTime'=>time()
                    );
            }
            $aLog = array(
                        'iActivityId'=>$iInserId,
                        'iCustomerID'=>$aRequest['iCustomerId'],                        
                        'iErrorCodeReturned'=>$aRequest['iErrorCodeReturned'],
                        'sDateTime'=>time()
                    ); 
            $db->insert('logs',$aLog);
            $aCustomer = array('id'=>$aRequest['iCustomerId'],'sLastRequestOn'=>time());
            $db->update('customer',$aCustomer);
            return $msg; 
		// commented for SLB TEST
		/*}// if(check for serverip against the clientIP	
		else {
		$msg="You don\'t have permission to access this website";
		return $msg; 
		}*/
		
        }
        function get_invalid_ip_count($sCustomerIP=''){
            $db = new db;       
            $row = $db->query("Select count(*) as cnt from logdetails where iErrorCodeReturned=9 and sCustomerIP='".$sCustomerIP."'");      
            if(mysql_num_rows($row)>0){
                $result = mysql_fetch_object($row);             
                return $result->cnt;                
            }
            else
                return 0;
        }
        function blacklistip_htaccess($ip)
        {
            $htaccess = DOCUMENTROOT.'/.htaccess';
            if(file_exists($htaccess ))
            {
                $contents = file_get_contents($htaccess, TRUE);
                // Lets search the htaccess file to see if there is already a ban in place.
                $exists_deny = !stripos($contents, "$ip");
                if(!(strpos($contents, '#Deny from {$ip}')===false)){
                    $hash_exists_deny=1;
                }
            }
            if($hash_exists_deny)
            {
                $searchstr="#Deny from {$ip}";
                $replacestr="Deny from {$ip}";
                // again block the ip
                $contents=str_ireplace($searchstr ,$replacestr,$contents);
                file_put_contents($htaccess, $contents) OR exit('Cannot append rule to '.$htaccess);
            }
            else if($exists_deny)
            {
                        // Here we just pull some details we can use later.
                        $date   = date('Y-m-d H:i:s');
                        $uri    = htmlspecialchars($_SERVER['REQUEST_URI'], ENT_QUOTES);
                        $agent  = htmlspecialchars($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES);
                        $agent  = str_replace(array("\n", "\r"), '', $agent);
                        //$ban =  "\n\r\n\r # The IP below was banned on $date for trying to access {$uri}\n\r";
                        //$ban .= "\n\r # Agent: {$agent}\n\r";
                        $ban .= "\n\rDeny from {$ip}\n\r";
                       
                        file_put_contents($htaccess, $ban, FILE_APPEND) OR exit('Cannot append rule to '.$htaccess);
            }
            return true;
        }
        function get_random_file_name(){
            $randomstring=substr(str_shuffle('abcdefghijklmnopqrstuvwxyz'),0,5);
            $str = time().$randomstring;
            $htmlfile = str_shuffle($str).'.html';
            return $htmlfile; 
        }
        function get_error_name($iErrorCode){
            $db = new db;           
            $row = $db->query("Select sName from error_codes where iErrorCode=".$iErrorCode);       
            if(mysql_num_rows($row)>0){
                $result = mysql_fetch_object($row);             
                return $result->sName;              
            }
            else
                return '';
        }
        function check_for_error($aRequest){
            $error =array();
            $db = new db;
            $row_valid_ip = $db->query("Select id from customer_ip where iCustomerId='".$aRequest['iCustomerId']."' and sIpAddress='".$aRequest['sCustomerIP']."'"); 
            $row_server_ip = $db->query("Select id from blacklists where sCustomerIP='".$aRequest['sCustomerIP']."' and status='block'");
            $row_customer_name = $db->query("Select id from customer where sCustName='".$aRequest['sCustomerName']."'");
            $row_authenticate_customer = $db->query("Select id from customer where sCustName='".$aRequest['sCustomerName']."' and sPassword='".$aRequest['sCustomerPassword']."' and id=".$aRequest['iCustomerId']." AND sStatus='active'");
            $row_hashkey = $db->query("Select id,sHashKey from customer where sCustName='".$aRequest['sCustomerName']."' and sPassword='".$aRequest['sCustomerPassword']."'");
            if(mysql_num_rows($row_hashkey)>0){
                $result_hashkey = mysql_fetch_object($row_hashkey);
                $iCustomerId = $result_hashkey->id;
                $haskey = $result_hashkey->sHashKey;
            }
            $newhashkey=base64_encode($aRequest['iCustomerId'].$aRequest['sDateTime'].$haskey);
            //if(!$this->validate_Ip($aRequest['iCustomerId'],$aRequest['sCustomerIP']))
            if(!(mysql_num_rows($row_valid_ip)>0))
            {
                $error['error_code'] =9;
            }
            elseif(!(file_exists(ASSETROOT.'/'.$aRequest['sAssetName'].'/'.strtolower($aRequest['sAssetName']).'.html'))){//else if(!$this->validate_asset_name($aRequest['sAssetName'])){
                $error['error_code'] =1;
            }
            elseif((mysql_num_rows($row_server_ip)>0)) //else if(!$this->validate_server_ip($aRequest['sCustomerIP'])){
            {
                $error['error_code'] =7;
            }
            elseif(!(mysql_num_rows($row_customer_name)>0))//else if(!$this->validate_customer_name($aRequest['sCustomerName'])){
            {
                $error['error_code'] =3;
            }
            elseif(!(mysql_num_rows($row_authenticate_customer)>0)) //else if(!$this->authenticate_customer($aRequest['sCustomerName'],$aRequest['sCustomerPassword'],$aRequest['iCustomerId']))
            {
                $error['error_code'] =2;
            }           
            else if(!($newhashkey === $aRequest['sMD5hash'])) //else if(!$this->validate_base64_hashkey($aRequest['sCustomerName'],$aRequest['sCustomerPassword'],$aRequest['sMD5hash'],$aRequest['sDateTime']))
            {
                $error['error_code'] =8;
            }           
            /*else
            {
                $error['error_code'] = 0;
            }*/
            return $error; 
        }
        function validate_Ip($iCustomerid,$sCustomerIP){
            $db = new db;
            //echo "Select id from customer_ip where iCustomerId='".$iCustomerid."' and sIpAddress='".$sCustomerIP."'";
            $row = $db->query("Select id from customer_ip where iCustomerId='".$iCustomerid."' and sIpAddress='".$sCustomerIP."'"); 
            if(mysql_num_rows($row)>0){
                return true;
            }
            else
                return false;
        }
        function validate_base64_hashkey($sCustomerName,$sCustomerPassword,$sMD5hash,$sDateTime){
            $db = new db;           
            $row = $db->query("Select id,sHashKey from customer where sCustName='".$sCustomerName."' and sPassword='".$sCustomerPassword."'");     
            if(mysql_num_rows($row)>0){
                $result = mysql_fetch_object($row);
                $iVId = $result->id;
                $haskey = $result->sHashKey;
                if(base64_encode("$iVId".$sDateTime.$haskey)!=$sMD5hash){
                    return false;
                }
                else
                    return true;
                    
            }
            else
                return false;
        }
        function authenticate_customer($sCustomerName,$sCustomerPassword,$iCustomerId){
            $db = new db;           
            $row = $db->query("Select id from customer where sCustName='".$sCustomerName."' and sPassword='".$sCustomerPassword."' and id=".$iCustomerId." AND sStatus='active'");       
            if(mysql_num_rows($row)>0){
                return true;                
            }
            else
                return false;
        }
        function validate_server_ip($sCustomerIP){
            $db = new db;           
            $row = $db->query("Select id from blacklists where sCustomerIP='$sCustomerIP' and status='block'");     
            if(mysql_num_rows($row)>0){
                return false;               
            }
            else
                return true;
        }
        function validate_asset_name($sAssetName){          
            if(file_exists(ASSETROOT.'/'.$sAssetName.'/'.strtolower($sAssetName).'.html')){             
                return true;
            }
            else{               
                return false;
            }
        }
        function validate_customer_name($sCustomerName){
            $db = new db;           
            $row = $db->query("Select id from customer where sCustName='".$sCustomerName."'");           
            if(mysql_num_rows($row)>0){
                return true;                
            }
            else{ 
                return false;
            }
        }
        function validate_asset_url(){
        }
        function recieved_data(){
        }
        function sent_data(){
        }
        function validate_customerid($iCustomerId){
            $db = new db;
            $row = $db->query("Select id from partners where id='".$iCustomerId."'");
            if($row){
                return true;                
            }
            else
                return false;
        }
        function get_customer($iCustomerId=0){
        }
    }
