<?php
date_default_timezone_set("UTC");
define('DOCUMENTROOT','/var/www/html/anatomedia');
define('ANATOMEDIADOCUMENTROOT','/var/www/html/Amedia');
require_once('/var/www/html/anatomedia/config/connect.php');
require_once('/var/www/html/anatomedia/lib/customer.php');
require_once('/var/www/html/anatomedia/lib/user.php');
require_once('/var/www/html/anatomedia/lib/customerip.php');
require_once('/var/www/html/anatomedia/lib/blacklist.php');
require_once('/var/www/html/anatomedia/lib/log.php');
require_once('/var/www/html/anatomedia/lib/admincommon.php');
require_once('/var/www/html/anatomedia/lib/pagination.php');
require_once('/var/www/html/anatomedia/lib/userlog.php');

$customer = new Customer;    
$oAdminCommon = new AdminCommon;
$arr_IP="";
$ipstart="";
$startDateTime=mktime(0,0,0,date('m'),date('d'),date('Y'));

$oCutomers=$customer->get_customers_by_start($startDateTime);
foreach($oCutomers as $oCutomer)
{

		$oCutomer['id']= isset($oCutomer['id'])?trim($oCutomer['id']):0;
             if($oCutomer['sStatus']=='Active'){
                $iModifyForUserId = $oCutomer['id'];
                $oAdminCommon->do_log(8,$iModifyForUserId,2);
            }
            else
			{
                $oCutomer['sStatus'] = 'Active';
                $iModifyForUserId = $oCutomer['id'];
                $oAdminCommon->do_log(7,$iModifyForUserId,2);
            }
			if($oCutomer['sIpOption']=='IP' )
			{
				$customer->addto_htaccess($oCutomer['sCustomerIP']);
			}
			if($oCutomer['sIpOption']=='Multiple IP' || $oCutomer['sIpOption']=='Subnet Mask')
			{
				$iprangevalue=str_replace(","," ",$oCutomer['sCustomerIP']);
				$customer->addto_htaccess($iprangevalue);
			}
			if($oCutomer['sIpOption']=='In Range')
			{
				$arr_IP=explode("-",$oCutomer['sCustomerIP']);
				for($i=0;$i<count($arr_IP);$i++)
				{
					$pattern="/(\d+).(\d+).(\d+).(\d+)/i";
					$replacement='$1.$2.$3.*';
					$strtValue_repl='$3';
					$ip_string_repl='$1.$2.';
					$iprangevalue= preg_replace($pattern, $replacement, $arr_IP[$i]);
					$ipstart[]=preg_replace($pattern, $strtValue_repl, $arr_IP[$i]);
					$ip_string=preg_replace($pattern, $ip_string_repl, $arr_IP[$i]);
				}
				for($j=$ipstart[0];$j<=$ipstart[1]; $j++)
				{
					$iprangevalue= $ip_string."".$j;
					
					$customer->addto_htaccess($iprangevalue);
				}
				$ipstart="";
			}
			//$customer->change_status($oCutomer);
}

?>
