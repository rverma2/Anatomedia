<?php
class Db{
    var $host;
    var $dbuser;
    var $dbpass;
    var $db_name;
    function db(){
	$this->host='localhost';
        $this->dbuser='root';
        $this->dbpass='';
        $this->db_name='anatomedia';		
		
    }//constructor
    function connect(){
        $con = mysql_connect($this->host,$this->dbuser,$this->dbpass);
        mysql_select_db($this->db_name,$con) or die("Server is currently busy, please try again later");
        return $con;
    }
    function close($con){
        mysql_close($con);
    }
    function query($query){         
        $con = $this->connect();
        return mysql_query($query,$con);
    }
    function site_settings(){       
        $query="select * from sitesettings where id=1 limit 1";
        $row = $this->query($query);
        $aData = mysql_fetch_assoc($row);
        return $aData;
        
    }
    function insert($sTable,$aRecord){
        $con = $this->connect();
        $escapedfieldValues = array_map(create_function('$e', 'return mysql_real_escape_string(((get_magic_quotes_gpc()) ? stripslashes($e) : $e));'), array_values($aRecord));
        $sql = sprintf("INSERT INTO `%s` (%s) VALUES ('%s')", $sTable,implode(',',array_keys($aRecord)), implode("','",$escapedfieldValues));
		return mysql_query($sql,$con);
    }
    function update($sTable,$aRecord){
        $con = $this->connect();
        $cnt=0;
        $update_value = '';
        foreach($aRecord as $key=>$value){
            if($key !='id')
            {
                if($cnt==0){
                    $update_value.="`$key`="."'".mysql_real_escape_string($value)."'";
                }//if
                else{
                    $update_value.=",`$key`="."'".mysql_real_escape_string($value)."'";
                }               
                $cnt++;
            }
        }       
        $sql = sprintf("Update `%s` Set %s where id=%s", $sTable,$update_value,$aRecord['id']);
        return mysql_query($sql,$con);
    }
    function fetch_object($query){
        $con = $this->connect();
        $aResultData = array();
        $row_cnt = 0;
        $result= mysql_query($query);       
        if(mysql_num_rows($result)>0){
            while($row = mysql_fetch_object($result)){
                foreach($row as $key=>$value){
                    $aResultData[$row_cnt][$key] = $value;
                }
                $row_cnt++;
            }
        }//if
        return $aResultData;
    }
}
?>