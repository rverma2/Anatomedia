<?php
error_reporting(0);
session_start();
require_once(dirname(__FILE__).'/connect.php');
$db = new Db;


// defined for http
define('HTTP','http://'.$_SERVER['SERVER_NAME']);
define('BASEURL','http://'.$_SERVER['SERVER_NAME'].'/ana');
define('ADMINURL',BASEURL.'/admin');
define('CSSURL',BASEURL.'/css');
define('JSURL',BASEURL.'/js');
define('IMAGEURL',BASEURL.'/images');
// defined for https
define('DOCUMENTROOT',$_SERVER['DOCUMENT_ROOT'].'/ana');
define('ANATOMEDIADOCUMENTROOT',$_SERVER['DOCUMENT_ROOT'].'/ana/config');

define('CLASSPATH',DOCUMENTROOT.'/lib');
define('PERPAGE',10);
define('INVALIDCUSTOMER',1);
?>