<?php 
require_once(DOCUMENTROOT.'/config/connect_report.php');
	class Report{
        var $db;
		
		function paginate($array, $pageSize, $page = 1)
		{
			$page = $page < 1 ? 1 : $page;
			$start = ($page - 1) * $pageSize;
			return array_slice($array, $start, $pageSize, true);
		}
		
		function pagination_html($totalPages){			
			if($totalPages>1){
			$PagingHtml = "<ul>";
				$st = (!empty($_REQUEST['search_type']) ? "&search_type=".$_REQUEST['search_type'] : "");
				$scb = ($_REQUEST['subscriber']!='' ? "&subscriber=".$_REQUEST['subscriber'] : "");
				$mf = (!empty($_REQUEST['month_from']) ? "&month_from=".$_REQUEST['month_from'] : "");
				$yf = (!empty($_REQUEST['year_from']) ? "&year_from=".$_REQUEST['year_from'] : "");
				$mt = (!empty($_REQUEST['month_to']) ? "&month_to=".$_REQUEST['month_to'] : "");
				$yt = (!empty($_REQUEST['year_to']) ? "&year_to=".$_REQUEST['year_to'] : "");
				$sd = (!empty($_REQUEST['sStartDate']) ? "&sStartDate=".$_REQUEST['sStartDate'] : "");
				$ed = (!empty($_REQUEST['sEndDate']) ? "&sEndDate=".$_REQUEST['sEndDate'] : "");
				$rsb = (!empty($_REQUEST['report_submit']) ? "&report_submit=".$_REQUEST['report_submit'] : "");
				$csts = $_REQUEST['customers'];
				if($_REQUEST['page']!=''){
					$csts = $_REQUEST['customers'];
				} else {
					$csts = implode(",", $csts);
				}
				$csts = (!empty($_REQUEST['customers']) ? "&customers=".$csts : "");
				$SearchSting = $st.$scb.$mf.$yf.$mt.$yt.$sd.$ed.$rsb.$csts;
				
				if(!empty($_REQUEST['page'])){
					$pcv = $_REQUEST['page'];
				} else {
					$pcv = 1;
				}

				$PagingHtml .= "<li><a href='report.php?page=1".$SearchSting."'>First</a></li>";
				if($pcv>1){
					$PagingHtml .= "<li><a href='report.php?page=".($pcv-1).$SearchSting."'>Prev</a></li>";
				}
				for($p=1;$p<=$totalPages;$p++){
					$CurrClass = ($p==$pcv ? 'current' : '');
					$purl = ($p==$pcv ? 'javascript:void(0);' : 'report.php?page='.$p.$SearchSting);
					$PagingHtml .= "<li class='".$CurrClass."'><a href='".$purl."'>".$p."</a></li>";
				}
				if($pcv<$totalPages){
					$PagingHtml .= "<li><a href='report.php?page=".($pcv+1).$SearchSting."'>Next</a></li>";
				}
				$PagingHtml .= "<li><a href='report.php?page=".$totalPages.$SearchSting."'>Last</a></li>";
			$PagingHtml .= "</ul>";
			} else {
				$PagingHtml = "";
			}
			
			return $PagingHtml;
		}
		
		
	}
    
