DELIMITER $$

USE `AnatomediaSRT`$$

DROP PROCEDURE IF EXISTS `AnatGetUnassignedCustomerForCustomerGroup_S`$$

CREATE DEFINER=`srtuser`@`localhost` PROCEDURE `AnatGetUnassignedCustomerForCustomerGroup_S`(CustomerGroupId INT)
SELECT DISTINCT c.id,c.customername FROM AnatomediaSRT.AnatCustomer c LEFT JOIN AnatomediaSRT.CustomerGroupCustomerMap cm ON cm.CustomerGroupID= CustomerGroupId AND  c.id = cm.anatcustomerid  WHERE   cm.anatcustomerid IS NULL ORDER BY c.customername$$

DELIMITER ;