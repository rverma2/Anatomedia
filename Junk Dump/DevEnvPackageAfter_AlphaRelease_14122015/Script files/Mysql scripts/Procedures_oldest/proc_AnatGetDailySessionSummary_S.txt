DELIMITER $$

USE `AnatomediaSRT`$$

DROP PROCEDURE IF EXISTS `AnatGetDailySessionSummary_S`$$

CREATE DEFINER=`srtuser`@`localhost` PROCEDURE `AnatGetDailySessionSummary_S`(custid VARCHAR(1000),startdate DATETIME, enddate DATETIME)
BEGIN
DECLARE groupid INT(10);
DECLARE sDelimiter VARCHAR(5);
DECLARE sItem VARCHAR(200);
DECLARE rtrnval VARCHAR(200);
SET sDelimiter =',';
SET rtrnval ="";
CREATE TEMPORARY TABLE IF NOT EXISTS tmpcustomerid (id INT(10));
 
WHILE INSTR(custid,sDelimiter) <> 0 DO
	SET sItem=TRIM(SUBSTRING(custid,1,INSTR(custid,sDelimiter)-1));
	SET custid=TRIM(SUBSTRING(custid,INSTR(custid,sDelimiter)+LENGTH(sDelimiter),LENGTH(custid)));
  IF LENGTH(sItem) > 0 THEN
	INSERT INTO tmpcustomerid VALUES(sItem);
  END IF;
 END WHILE;
 IF LENGTH(custid) > 0 THEN 
	INSERT INTO tmpcustomerid VALUES(custid);
 END IF;
 SELECT dailysessiondate,SUM(sessioncount) as sessioncount FROM AnatomediaSRT.Anatdailysessionsummaryonget WHERE customerid IN(SELECT id FROM  tmpcustomerid) AND dailysessiondate BETWEEN startdate AND enddate GROUP BY dailysessiondate;
DROP TABLE tmpcustomerid;
END$$

DELIMITER ;