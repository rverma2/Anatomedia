DELIMITER $$

USE `AnatomediaSRT`$$

DROP PROCEDURE IF EXISTS `AnatGetCustomerGroup_S`$$

CREATE DEFINER=`srtuser`@`localhost` PROCEDURE `AnatGetGroupCustomer_S`(id INT)
BEGIN
IF id = 0 THEN
SELECT DISTINCT c.id, c.customername FROM AnatomediaSRT.AnatCustomer c;
ELSE
SELECT DISTINCT c.id, c.customername FROM AnatomediaSRT.AnatCustomer c INNER JOIN AnatomediaSRT.CustomerGroupCustomerMap cm ON c.id = cm.anatcustomerid INNER JOIN AnatomediaSRT.CustomerGroup cg ON cg.customergroupid = cm.customergroupid  WHERE cg.customergroupid = id ORDER BY c.customername;
END IF;
END$$

DELIMITER ;