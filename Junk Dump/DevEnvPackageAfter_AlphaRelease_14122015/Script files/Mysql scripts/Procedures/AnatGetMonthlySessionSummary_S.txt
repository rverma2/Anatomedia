DELIMITER $$

USE `AnatomediaSRT`$$

DROP PROCEDURE IF EXISTS `AnatGetMonthlySessionSummary_S`$$

CREATE DEFINER=`srtuser`@`localhost` PROCEDURE `AnatGetMonthlySessionSummary_S`(custid VARCHAR(1000),MonthFrom INT(2), yearFrom INT(4),MonthTo INT(2), yearTo INT(4))
BEGIN
DECLARE strtdt VARCHAR(20);
DECLARE enddt VARCHAR(20);
DECLARE groupid INT(10);
DECLARE sDelimiter VARCHAR(5);
DECLARE sItem VARCHAR(200);
DECLARE rtrnval VARCHAR(200);
SET sDelimiter =',';
SET rtrnval ="";
CREATE TEMPORARY TABLE IF NOT EXISTS tmpcustomerid (id INT(10));
 
WHILE INSTR(custid,sDelimiter) <> 0 DO
	SET sItem=TRIM(SUBSTRING(custid,1,INSTR(custid,sDelimiter)-1));
	SET custid=TRIM(SUBSTRING(custid,INSTR(custid,sDelimiter)+LENGTH(sDelimiter),LENGTH(custid)));
  IF LENGTH(sItem) > 0 THEN
	INSERT INTO tmpcustomerid VALUES(sItem);
  END IF;
 END WHILE;
 IF LENGTH(custid) > 0 THEN 
	INSERT INTO tmpcustomerid VALUES(custid);
 END IF;
SET strtdt=CONCAT(01,",",MonthFrom,",",yearFrom);
SET enddt=CONCAT(01,",",MonthTo,",",yearTo);
-- SELECT MONTH(dailysessiondate) as 'month',year(dailysessiondate) as 'year',SUM(sessioncount) as 'sessioncount' FROM AnatomediaSRT.Anatdailysessionsummaryonget WHERE customerid = custid AND dailysessiondate BETWEEN STR_TO_DATE(strtdt,'%d,%m,%Y') AND LAST_DAY(STR_TO_DATE(enddt,'%d,%m,%Y')) GROUP BY MONTH(dailysessiondate), year(dailysessiondate);  ####old logic
SELECT MONTHNAME AS 'month',YEAR AS 'year',SUM(sessioncount) AS 'sessioncount' FROM AnatomediaSRT.Anatmonthlysessionsummary WHERE customerid IN(SELECT id FROM  tmpcustomerid)AND MONTHNAME BETWEEN MonthFrom AND MonthTo AND YEAR BETWEEN yearFrom AND yearTo GROUP BY MONTHNAME, YEAR;
DROP TABLE tmpcustomerid;
END$$

DELIMITER ;