#!/bin/bash
curdate=$(date +"%Y%m%d")
filepath="/srt/data/"
fileinitname="anatlog"
ctrlfileinitname="control"
filetoprocess=$filepath$ctrlfileinitname
filepathlen=${#filetoprocess}
firstfileprocess=""
lastsuccessfileprocessed=""
curfileprocess=""
curfileprocessdt=""
nextfiletoprocessdt=""
ctrlfilewrittencount=""
executionid=""
controlfilename=""
lastfileprocessed=$(mysql -usrtuser -panatlog -se "SELECT fileprocessed FROM AnatomediaSRT.Anatexecutionhistory WHERE executionstatus = 's' ORDER BY executionenddt DESC LIMIT 1;")

#if [ $? = 1 ]
#then
#echo "error"
#else
#echo "no error"
#fi

processfile () {							#funcation name
    # controlfilename=$filepath"control"$2
	curfileprocess=$filepath$fileinitname$1				#generating current file name
	if [ -f $curfileprocess ]					#checking, if file exits at destination folder
	then
	counter=""
	rowsread=`awk '{print $2}' $controlfilename`			#reading control file information
	rowswritten=`awk '{print $4}' $controlfilename`			#reading control file information
	ctrlstatus=`awk '{print $6}' $controlfilename`			#reading control file information
	#filename=`awk '{print $8}' $controlfilename`			#reading control file information
	jobenddate=`awk '{print $10}' $controlfilename`			#reading control file information
    d1=`awk '{print $10}' $controlfilename`
    t1=`awk '{print $11}' $controlfilename`
jobenddate=$d1" "$t1

	mysql -usrtuser -panatlog -e "CALL AnatomediaSRT.Anatpopulatecustomerinfo();"    #Refreshing anatcustomer information from production customer table. [04 dec 15] 

	
#Inserting into proceestime table
	mysql -usrtuser -panatlog -e "INSERT INTO AnatomediaSRT.Anatprocesstime(fileprocessed,currentprocess,executionstartdt,executionstatus) VALUES('$curfileprocess','rawdata',CURRENT_TIMESTAMP,'s');"
#calling Procedure Anatcontrolfileinfo_I 
	if mysql -usrtuser -panatlog -e "CALL AnatomediaSRT.Anatcontrolfileinfo_I('$controlfilename',$rowsread,$rowswritten,$ctrlstatus,$jobenddate,'$curfileprocess',@counter);"	
	then 
	  if [ "$ctrlstatus" = "'s'" ]		#checking control file status
	  then
#LOADING RAW DATA IN TABLE
		mysql -usrtuser -panatlog -e "LOAD DATA LOCAL INFILE '$curfileprocess' INTO TABLE AnatomediaSRT.Anatdailyrawlogs FIELDS ENCLOSED BY '' TERMINATED BY '' (rawlogdata);"
#checking if some error or warning occur during load process.
		mysqlwarnings=$(mysql -usrtuser -panatlog -e "show warnings;")
		mysqlerrors=$(mysql -usrtuser -panatlog -e "show errors;")
	mysql -usrtuser -panatlog -e "INSERT INTO AnatomediaSRT.Anatprocesstime(fileprocessed,currentprocess,executionstartdt,executionstatus) VALUES('$curfileprocess','rawdata',CURRENT_TIMESTAMP,'e');"
		if [ "$mysqlwarnings" = "" ] || [ "$mysqlerrors" = "" ]
		then
	mysql -usrtuser -panatlog  -e "INSERT INTO AnatomediaSRT.Anatprocesstime(fileprocessed,currentprocess,executionstartdt,executionstatus) VALUES('$curfileprocess','parsing',CURRENT_TIMESTAMP,'s');"
#Retrieving last inserted row id
			executionid=$(mysql -usrtuser -panatlog -se "SELECT MAX(executionhistoryid) FROM AnatomediaSRT.Anatexecutionhistory WHERE fileprocessed = '$controlfilename';")

#Calling Procedure Anatparsedata_I
			if mysql -usrtuser -panatlog -e "CALL AnatomediaSRT.Anatparsedata_I($executionid,$rowswritten,'$curfileprocessdt',@parsecounter);"
			then
	mysql -usrtuser -panatlog -e "INSERT INTO AnatomediaSRT.Anatprocesstime(fileprocessed,currentprocess,executionstartdt,executionstatus) VALUES('$curfileprocess','parsing',CURRENT_TIMESTAMP,'e');"
	mysql -usrtuser -panatlog -e "INSERT INTO AnatomediaSRT.Anatprocesstime(fileprocessed,currentprocess,executionstartdt,executionstatus) VALUES('$curfileprocess','daily stats',CURRENT_TIMESTAMP,'s');"
#calling Procedure AnatDailyInfo_I
				if mysql -usrtuser -panatlog -e "CALL AnatomediaSRT.AnatDailyInfo_I($executionid,'$curfileprocessdt');"
				then
	mysql -usrtuser -panatlog -e "INSERT INTO AnatomediaSRT.Anatprocesstime(fileprocessed,currentprocess,executionstartdt,executionstatus) VALUES('$curfileprocess','daily stats',CURRENT_TIMESTAMP,'e');"
	mysql -usrtuser -panatlog -e "INSERT INTO AnatomediaSRT.Anatprocesstime(fileprocessed,currentprocess,executionstartdt,executionstatus) VALUES('$curfileprocess','monthly rollup',CURRENT_TIMESTAMP,'s');"
#Calling Procedure AnatMonthlyRollup_I
					if mysql -usrtuser -panatlog -e "CALL AnatomediaSRT.AnatMonthlyRollup_I($executionid,'$curfileprocessdt');"
					then
mysql -usrtuser -panatlog -e "INSERT INTO AnatomediaSRT.Anatprocesstime(fileprocessed,currentprocess,executionstartdt,executionstatus) VALUES('$curfileprocess','monthly rollup',CURRENT_TIMESTAMP,'e');"
						return 1
					else
						return 0
					fi
				else
					return 0
				fi
			else
				return 0
			fi
		else
			mysql -usrtuser -panatlog -e "update AnatomediaSRT.Anatexecutionhistory set executionstatus=\"f\" where fileprocessed='$curfileprocess';"
			return 0
		fi
	  else
		return 0
	  fi
	else
		return 0
	fi
     else
	echo "no file found $curfileprocess"
	mysql -usrtuser -panatlog -e "INSERT INTO AnatomediaSRT.Anatexecutionhistory(FileProcessed,currentprocess,comments,executionenddt,executionstatus) VALUES('$controlfilename','raw_data','no file found. $controlfilename',CURRENT_TIMESTAMP,'f');"
	return 0
     fi
}



for file in $filepath$ctrlfileinitname*   				#processing each file, as defined by variables.
do	
	#curfileprocess=$file
	controlfilename=$file						#Control file name
	curfileprocessdt=${file:$filepathlen}				#date of file under process
	if [ "$lastfileprocessed" = ""  ]				#checking, If no successful processed file information in DB
	then 
		if  [ ${file:$filepathlen} -lt $curdate ]		#checking, if date of file under process is less then current date
		then
			if [[ "$firstfileprocess" = "" ]] || [[ "$nextfiletoprocessdt" = ${file:$filepathlen} ]]	#checking if files are in sequence.
			then
				processfile ${file:$filepathlen}	#calling function
				returnval=$?
				[ $returnval = 0 ] && break		#if returned value is 0 leave the loop
				
				echo "file processed ${file:$filepathlen}"

				firstfileprocess=$file			#initialising variable with current file processed.
				nextfiletoprocessdt=$(date --date="${file:$filepathlen} + 1 day" +"%Y%m%d")	#Generate expected next file date
			else
				break
			fi
		fi
	else								#If successful processed file information in DB
		#echo $lastfileprocessed
		if [ ${file:$filepathlen} -le ${lastfileprocessed:$filepathlen} ]; then		#ignore file which dates are less then last file process date.
		continue
		fi


		if [ "$nextfiletoprocessdt" = "" ]			 #Generate expected next file date
                then
                        nextfiletoprocessdt=$(date --date="${lastfileprocessed:$filepathlen} + 1 day" +"%Y%m%d")
                else
                        nextfiletoprocessdt=$(date --date="$nextfiletoprocessdt + 1 day" +"%Y%m%d")
                fi


		if [[ ${file:$filepathlen} -gt ${lastfileprocessed:$filepathlen} ]]  && [[ ${file:$filepathlen} -lt $curdate ]]  #checking, if date of file under process is less then current date and greater then lastfile processed.
		then
			if [ "$nextfiletoprocessdt" = ${file:$filepathlen} ]	#checking if files are in sequence.
                        then
				processfile ${file:$filepathlen}		#calling funcation
                                returnval=$?
                                [ $returnval = 0 ] && break			#if returned value is 0 leave the loop

				echo "file processed $nextfiletoprocessdt"
			else
				break
			fi
		else
			break
		fi
	fi
done
