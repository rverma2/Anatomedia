DELIMITER $$

USE `AnatomediaSRT`$$

DROP PROCEDURE IF EXISTS `AnatGetUnassignedCustomer_S`$$

CREATE DEFINER=`srtuser`@`localhost` PROCEDURE `AnatGetUnassignedCustomer_S`()
SELECT DISTINCT c.id,c.customername FROM AnatomediaSRT.AnatCustomer c LEFT JOIN AnatomediaSRT.AnatCustomerGroupCustomerMap cm ON c.id = cm.customerid WHERE cm.customerid IS NULL ORDER BY c.customername$$

DELIMITER ;