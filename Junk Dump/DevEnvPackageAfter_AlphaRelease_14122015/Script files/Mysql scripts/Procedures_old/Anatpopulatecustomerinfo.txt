DELIMITER $$

USE `AnatomediaSRT`$$

DROP PROCEDURE IF EXISTS `Anatpopulatecustomerinfo`$$

CREATE DEFINER=`srtuser`@`localhost` PROCEDURE `Anatpopulatecustomerinfo`()
BEGIN
TRUNCATE AnatomediaSRT.AnatCustomer;
INSERT INTO AnatomediaSRT.AnatCustomer (id,customername,customertype,customerstatus,ipaddressfrom,ipaddressto)
SELECT cust.id,cust.scustname,cust.ssubstrial,cust.sstatus,(CASE cust.sipoption 
		WHEN 'IP' THEN SUBSTRING_INDEX(SUBSTRING_INDEX(cust.scustomerip, ',', n), ',', -1) 
		WHEN 'In Range' THEN SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(cust.scustomerip, ',', n), ',', -1), '-', 1) 
		WHEN 'Subnet Mask' THEN SUBSTRING_INDEX((SELECT Anatgetipbymaskbit(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(cust.scustomerip, ',', n), ',', -1), '/', 1),SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(cust.scustomerip, ',', n), ',', -1), '/', -1))), '-', 1) 
		WHEN 'Multiple IP' THEN SUBSTRING_INDEX(SUBSTRING_INDEX(cust.scustomerip, ',', n), ',', -1)  END) AS 'fromip', 
		(CASE sipoption WHEN 'IP' THEN SUBSTRING_INDEX(SUBSTRING_INDEX(cust.scustomerip, ',', n), ',', -1)  
		WHEN 'In Range' THEN SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(cust.scustomerip, ',', n), ',', -1), '-', -1) 
		WHEN 'Subnet Mask' THEN SUBSTRING_INDEX((SELECT Anatgetipbymaskbit(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(cust.scustomerip, ',', n), ',', -1), '/', 1),SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(cust.scustomerip, ',', n), ',', -1), '/', -1))), '-', -1)
		WHEN 'Multiple IP' THEN SUBSTRING_INDEX(SUBSTRING_INDEX(cust.scustomerip, ',', n), ',', -1)  END) AS 'toip' FROM AnatomediaSRT.customer cust JOIN AnatomediaSRT.number num ON CHAR_LENGTH(cust.scustomerip) - CHAR_LENGTH(REPLACE(cust.scustomerip, ',', '')) >= num.n - 1;
		
END$$

DELIMITER ;