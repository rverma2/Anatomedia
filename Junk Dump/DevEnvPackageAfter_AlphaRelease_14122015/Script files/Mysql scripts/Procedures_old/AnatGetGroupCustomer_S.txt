DELIMITER $$

USE `AnatomediaSRT`$$

DROP PROCEDURE IF EXISTS `AnatGetGroupCustomer_S`$$

CREATE DEFINER=`srtuser`@`localhost` PROCEDURE `AnatGetGroupCustomer_S`(id INT)
BEGIN
IF id = 0 THEN
SELECT DISTINCT c.id, c.customername FROM AnatomediaSRT.AnatCustomer c;
ELSE
SELECT DISTINCT c.id, c.customername FROM AnatomediaSRT.AnatCustomer c INNER JOIN AnatomediaSRT.AnatCustomerGroupCustomerMap cm ON c.id = cm.customerid INNER JOIN AnatomediaSRT.AnatCustomerGroup cg ON cg.customergroupid = cm.customergroupid  WHERE cg.customergroupid = id ORDER BY c.customername;
END IF;
END$$

DELIMITER ;