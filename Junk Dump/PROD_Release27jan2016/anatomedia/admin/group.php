<?php
	error_reporting(E_ALL);
	require_once('../php/Logger.php');
	Logger::configure('logconfig.xml');
	$logger = Logger::getLogger('myLogger');
	require_once('../config/config_report.php');

    $page =  isset($_REQUEST['page'])&& $_REQUEST['page']>0?trim((int)$_REQUEST['page']-1):0;
    $error = '';
	if(!$_SESSION['userID']){
        header("location:".'login.php');
    }
	
    $error='';
    $sUserName ='';
    $action = isset($_REQUEST['action'])? $_REQUEST['action']:'main';
    switch($action){

        case 'main':
			/* Get Customer Group */
			$CustGrp = $mysqli->query("CALL AnatGetCustomerGroup_S");
			while($cg = $CustGrp->fetch_object()){
				$CustGrpArray[$cg->customergroupid] = $cg->customergroup;
			}			
			$CustGrp->close();
            $mysqli->next_result();
			
			/* Get Unassigned Customer Group */
			$UnassignedCust = $mysqli->query("CALL AnatGetGroupCustomer_S(0, 'G')");
			while($ucg = $UnassignedCust->fetch_object()){
				$UnassignedCustArray[$ucg->id] = $ucg->customername;
			}
			$UnassignedCust->close();
            $mysqli->next_result();
			
            include "view/group.html";
            break;
        case 'editgroup':
			$GrpId = $_REQUEST['id'];
			/* Get Customer Group */
			$CustGrp = $mysqli->query("CALL AnatGetCustomerGroup_S");
			while($cg = $CustGrp->fetch_object()){
				$CustGrpArray[$cg->customergroupid] = $cg->customergroup;
			}
			$CustGrp->close();
            $mysqli->next_result();
			
			/* Get Unassigned Customer Group AnatGetUnassignedCustomerForCustomerGroup_S(4) */
			$UnassignedCust = $mysqli->query("CALL AnatGetUnassignedCustomerForCustomerGroup_S(".$GrpId.")");
			while($ucg = $UnassignedCust->fetch_object()){
				$UnassignedCustArray[$ucg->id] = $ucg->customername;
			}
			$UnassignedCust->close();
            $mysqli->next_result();
			
			/* edit customer Group */
			if(!empty($GrpId)){
				$EdCustGrp = $mysqli->query("CALL AnatGetGroupCustomer_S(".$GrpId.", 'G')");
				while($ecg = $EdCustGrp->fetch_object()){
					$GrpCustomers[$ecg->id] = $ecg->customername;
				}
				$EdCustGrp->close();
				$mysqli->next_result();
			}
			
			
            include "view/editgroup.html";
            break;
        case 'savegroup';  
			$group_id = $_REQUEST['group_id'];
			$group_name = $_REQUEST['group_name'];
			$assigned_customers = $_REQUEST['assigned_customers'];
			$assigned_customers = implode(",", $assigned_customers);
			$CustGrp = $mysqli->query("CALL AnatGetCustomerGroup_S");
			
			while($cg = $CustGrp->fetch_object()){
				$CustGrpArray[$cg->customergroupid] = $cg->customergroup;
			}		
			
			$CustGrp->close();
            $mysqli->next_result();
			
			if(!empty($group_id)){
				if($CustGrpArray[$group_id]==$group_name){
					$InsGrp = $mysqli->query("CALL AnatCustomerGroupInfo_IU('".$group_name."',$group_id,'".$assigned_customers."','u')");
					$mysqli->next_result();
					$result['redirect_url'] = ADMINURL.'/group.php?messageCode=1';
					$messageCode=1;
				} else {
					if(in_array(strtolower($group_name), array_map('strtolower', $CustGrpArray))){
						$messagegroup="<span class='status-not-available'> Group name already exists.</span>";
						$error=1;
						$result['messagegroup'] = $messagegroup;
						$result['error'] = $error;
					} else {
						$InsGrp = $mysqli->query("CALL AnatCustomerGroupInfo_IU('".$group_name."',$group_id,'".$assigned_customers."','u')");
						$mysqli->next_result();
						$result['redirect_url'] = ADMINURL.'/group.php?messageCode=1';
						$messageCode=1;
					}
				}

			} else {
					if(in_array(strtolower($group_name), array_map('strtolower', $CustGrpArray))){
						 $messagegroup="<span class='status-not-available'> Group name already exists.</span>";
						$error=1;
						$result['messagegroup'] = $messagegroup;
						$result['error'] = $error;
					} else {
						$InsGrp = $mysqli->query("CALL AnatCustomerGroupInfo_IU('".$group_name."',NULL,'".$assigned_customers."','i')");
						$mysqli->next_result();
						$messageCode=2;
						$result['redirect_url'] = ADMINURL.'/group.php?messageCode=2';
					}
			}
			$result['messageCode'] = $messageCode;
			echo json_encode($result);
			die();
         break;
        case 'default':
            include "view/login.html";
            break;  
    }
    
    
?>
