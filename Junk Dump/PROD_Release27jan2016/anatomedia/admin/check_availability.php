<?php
error_reporting(E_ALL);
	require_once('../php/Logger.php');
	Logger::configure('logconfig.xml');
	$logger = Logger::getLogger('myLogger');
	require_once('../config/config_report.php');

    $page =  isset($_REQUEST['page'])&& $_REQUEST['page']>0?trim((int)$_REQUEST['page']-1):0;
    $error = '';
	if(!$_SESSION['userID']){
        header("location:".'login.php');
    }
	

if(!empty($_POST["username"])) {
  
	$CustGrp = $mysqli->query("CALL AnatGetCustomerGroup_S");
	
	while($cg = $CustGrp->fetch_object()){
		$CustGrpArray[$cg->customergroupid] = $cg->customergroup;
	}		
	
	$CustGrp->close();
	$mysqli->next_result();	
	
	$ResHTML = "";
		if(!empty($_POST['group_id'])){
			if($CustGrpArray[$_POST['group_id']]!=$_POST["username"]){
			
				if(in_array(strtolower($_POST["username"]), array_map('strtolower', $CustGrpArray))){
					$ResHTML .= "<span class='status-not-available'> Group name already exists.</span>";
					//echo "<input type='hidden' name='groupstatus' id='groupstatus' value='1'>";
					$GroupStatus = 1;
				} else {
					$ResHTML .= "<span class='status-available'> Groupname Available.</span>";
					//echo "<input type='hidden' name='groupstatus' id='groupstatus' value='0'>";
					$GroupStatus = 0;
				}
			}
		} else {
			if(in_array(strtolower($_POST["username"]), array_map('strtolower', $CustGrpArray))){
				$ResHTML .= "<span class='status-not-available'> Group name already exists.</span>";
				//echo "<input type='hidden' name='groupstatus' id='groupstatus' value='1'>";
				$GroupStatus = 1;
			} else {
				$ResHTML .= "<span class='status-available'> Groupname Available.</span>";
				//echo "<input type='hidden' name='groupstatus' id='groupstatus' value='0'>";
				$GroupStatus = 0;
			}
		}
} else {
	$ResHTML .= "<span class='status-not-available'>Group name can't be blank!</span>";
}
	
	$res['res_html'] = $ResHTML;
	$res['group_status'] = $GroupStatus;
	
	echo json_encode($res);
	exit();
?>
