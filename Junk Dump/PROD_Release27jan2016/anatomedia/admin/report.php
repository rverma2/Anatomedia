<?php
	error_reporting(E_ALL);
	require_once('../php/Logger.php');
	Logger::configure('logconfig.xml');
	$logger = Logger::getLogger('myLogger');
	require_once('../config/config_report.php');
	require_once(CLASSPATH.'/report.php');

    $page =  isset($_REQUEST['page'])&& $_REQUEST['page']>0?trim((int)$_REQUEST['page']-1):0;
    $error = '';
	if(!$_SESSION['userID']){
        header("location:".'login.php');
    }
	$oReport = new Report;
	
    $error='';
    $sUserName ='';
    $action = isset($_REQUEST['action'])? $_REQUEST['action']:'main';
	$MonthArray = array(1 => 'January',2 => 'February',3 => 'March', 4 => 'April', 5 => 'May', 6 => 'Jun', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December');
    switch($action){
        case 'main':
			/* get reports */
			if(isset($_REQUEST['report_submit'])){	
				$Subscriber = $_REQUEST['subscriber'];
				if($_REQUEST['page']!=''){
					$Customers = $_REQUEST['customers'];
					$Customers = explode(",", $Customers);
				} else {
					$Customers = $_REQUEST['customers'];
				}
				 $Customers = implode(",", $Customers);
				 
				/*get report for date range */
				$ErrorArray = array();
				
				if($_REQUEST['search_type']=='date'){
					$StartDate = $_REQUEST['sStartDate'];
					$EndDate = $_REQUEST['sEndDate'];
					if(empty($StartDate)){ $ErrorArray[] = "Please enter start date."; }
					if(empty($EndDate)){ $ErrorArray[] = "Please enter end date."; }
					/*get stats summary script */
					if(count($ErrorArray)<1){
					$GetStatsReport = $mysqli->query("CALL AnatGetDailyStatSummary_S('".$Customers."','".$StartDate."','".$EndDate."')");
					while($dst = $GetStatsReport->fetch_object()){
						$StatArray[$dst->dailystatdate]['stats'][$dst->module] = $dst->pagecount;
						@$StatArray[$dst->dailystatdate]['html_views'] += $dst->pagecount;
					}
					$GetStatsReport->close();
					$mysqli->next_result();
					
					/*get session summary summary script */
					$GetSessionReport = $mysqli->query("CALL AnatGetDailySessionSummary_S('".$Customers."','".$StartDate."','".$EndDate."')");
					while($dsst = $GetSessionReport->fetch_object()){
						$StatSessArray[$dsst->dailysessiondate] = $dsst->sessioncount;
						$CalcTotalSession += $dsst->sessioncount;
					}
					
					$GetSessionReport->close();
					$mysqli->next_result();
					}
				}
				if($_REQUEST['search_type']=='month'){
					
					/* get stats summary script monthly */
					$MonthFrom = $_REQUEST['month_from'];
					$YearFrom = $_REQUEST['year_from'];
					$MonthTo = $_REQUEST['month_to'];
					$YearTo = $_REQUEST['year_to'];
					if(empty($MonthFrom)){ $ErrorArray[] = "Please select month from."; }
					if(empty($YearFrom)){ $ErrorArray[] = "Please select year from."; }
					if(empty($MonthTo)){ $ErrorArray[] = "Please select month to."; }
					if(empty($YearTo)){ $ErrorArray[] = "Please select year to."; }
					
					if(count($ErrorArray)<1){
					$GetStatsReportMonthly = $mysqli->query("CALL AnatGetMonthlyStatSummary_S('".$Customers."',".$MonthFrom.",".$YearFrom.",".$MonthTo.",".$YearTo.")");
					while($dst = $GetStatsReportMonthly->fetch_object()){
						$StatArray[$dst->month]['stats'][$dst->module] = $dst->pagecount;
						@$StatArray[$dst->month]['html_views'] += $dst->pagecount;
						$StatArray[$dst->month]['year'] = $dst->year;
					}

					$GetStatsReportMonthly->close();
					$mysqli->next_result();

					/*get session summary summary script */
					$GetSessionReportMonthly = $mysqli->query("CALL AnatGetMonthlySessionSummary_S('".$Customers."',".$MonthFrom.",".$YearFrom.",".$MonthTo.",".$YearTo.")");
					while($dsst = $GetSessionReportMonthly->fetch_object()){
						$StatSessArray[$dsst->month] = $dsst->sessioncount;
						$CalcTotalSession += $dsst->sessioncount;
					}
					$GetSessionReportMonthly->close();
					$mysqli->next_result();
					}
				}
				$totviews = "";
				$TotalHTMLViews = "";
				foreach($StatArray as $sa){
					foreach($sa['stats'] as $ask=>$ast){
						$totviews[$ask] += $ast;
					}
					$TotalHTMLViews += $sa['html_views'];
				}

				$page = ! empty( $_GET['page'] ) ? (int) $_GET['page'] : 1;
				$total = count( $StatArray ); //total items in array    
				$limit = 10; //per page    
				$totalPages = ceil( $total/ $limit ); //calculate total pages
				$page = max($page, 1); //get 1 page when $_GET['page'] <= 0
				$page = min($page, $totalPages); //get last page when $_GET['page'] > $totalPages
				$offset = ($page - 1) * $limit;
				if( $offset < 0 ) $offset = 0;
				$StatArray = $oReport->paginate($StatArray, $limit, $_GET['page']);
				$pagination_html = $oReport->pagination_html($totalPages);
			/* get checked customers in the right box on search- chages by Amit for All customer case. */
				if($Subscriber!=''){
					$EdCustGrp = $mysqli->query("CALL AnatGetGroupCustomer_S(".$Subscriber.", 'R')");
					while($ecg = $EdCustGrp->fetch_object()){
						$GrpCustomers[$ecg->id] = $ecg->customername;
					}
					$EdCustGrp->close();
					$mysqli->next_result();
				}
			} else {
				$DashBrdMsg = "<h4>Please select options and click on search to get report.";
			}
			
			$ModuleRes = $mysqli->query("CALL AnatGetModuleName_S");
			while($md = $ModuleRes->fetch_object()){
				$ModuleArray[] = $md->Module;
			}
			$ModuleRes->close();
            $mysqli->next_result();
			
			/* Get Customer Group */
			$CustGrp = $mysqli->query("CALL AnatGetCustomerGroup_S");
			while($cg = $CustGrp->fetch_object()){
				$CustGrpArray[$cg->customergroupid] = $cg->customergroup;
			}			
			$CustGrp->close();
            $mysqli->next_result();
			
            include "view/report.html";
            break;
		case 'get_print':
				$Subscriber = $_REQUEST['subscriber'];
				$Customers = $_REQUEST['customers'];
				/*get report for date range */
				if($_REQUEST['report_type']=='date'){
					$StartDate = $_REQUEST['sStartDate'];
					$EndDate = $_REQUEST['sEndDate'];
					/*get stats summary script */
					$GetStatsReport = $mysqli->query("CALL AnatGetDailyStatSummary_S('".$Customers."','".$StartDate."','".$EndDate."')");
					while($dst = $GetStatsReport->fetch_object()){
						$StatArray[$dst->dailystatdate]['stats'][$dst->module] = $dst->pagecount;
						@$StatArray[$dst->dailystatdate]['html_views'] += $dst->pagecount;
					}
					
					
					$GetStatsReport->close();
					$mysqli->next_result();
					
					/*get session summary summary script */
					$GetSessionReport = $mysqli->query("CALL AnatGetDailySessionSummary_S('".$Customers."','".$StartDate."','".$EndDate."')");
					while($dsst = $GetSessionReport->fetch_object()){
						$StatSessArray[$dsst->dailysessiondate] = $dsst->sessioncount;
					}
					$GetSessionReport->close();
					$mysqli->next_result();
				}
				if($_REQUEST['report_type']=='month'){
					/*get stats summary script monthly */
					$MonthFrom = $_REQUEST['month_from'];
					$YearFrom = $_REQUEST['year_from'];
					$MonthTo = $_REQUEST['month_to'];
					$YearTo = $_REQUEST['year_to'];
					
					$GetStatsReportMonthly = $mysqli->query("CALL AnatGetMonthlyStatSummary_S('".$Customers."',".$MonthFrom.",".$YearFrom.",".$MonthTo.",".$YearTo.")");
					while($dst = $GetStatsReportMonthly->fetch_object()){
						$StatArray[$dst->month]['stats'][$dst->module] = $dst->pagecount;
						@$StatArray[$dst->month]['html_views'] += $dst->pagecount;
						$StatArray[$dst->month]['year'] = $dst->year;
					}

					$GetStatsReportMonthly->close();
					$mysqli->next_result();

					/*get session summary summary script */
					$GetSessionReportMonthly = $mysqli->query("CALL AnatGetMonthlySessionSummary_S('".$Customers."',".$MonthFrom.",".$YearFrom.",".$MonthTo.",".$YearTo.")");
					while($dsst = $GetSessionReportMonthly->fetch_object()){
						$StatSessArray[$dsst->month] = $dsst->sessioncount;
					}
					$GetSessionReportMonthly->close();
					$mysqli->next_result();
				}
				
// creating table
			/* get subscriber name */
			$CustGrp = $mysqli->query("CALL AnatGetCustomerGroup_S");
			while($cg = $CustGrp->fetch_object()){
				$CustGrpArray[$cg->customergroupid] = $cg->customergroup;
			}			
			$CustGrp->close();
            $mysqli->next_result();
			
			
			/* get module name */
			$ModuleRes = $mysqli->query("CALL AnatGetModuleName_S");
			while($md = $ModuleRes->fetch_object()){
				$ModuleArray[] = $md->Module;
			}
			$ModuleRes->close();
            $mysqli->next_result();
			
			$MC = count($ModuleArray)+3;
			$blank_row = "<tr><td colspan='".$MC."' style='background: none; color: #000;'>&nbsp;</td></tr>";
			
			$TableHtml = "<table class='report-table'>";
			
			
			$customer_name = $CustGrpArray[$Subscriber];
			if($_REQUEST['report_type']=='month'){ 
				$report_type_text = "Monthly Report - ";
				$report_period = substr($MonthArray[$_REQUEST['month_from']],0,3)."-".$_REQUEST['year_from']." to ".substr($MonthArray[$_REQUEST['month_to']],0,3)."-".$_REQUEST['year_to'];
			}
			if($_REQUEST['report_type']=='date'){ 
				$report_type_text = "Daily Report - "; 
				$report_period = $_REQUEST['sStartDate']." to ".$_REQUEST['sEndDate'];
			}
			$reporting_field = $report_type_text.$report_period;
			$TableHtml .= "<tr><td colspan='".$MC."' style='background: none; color: #000;'>".$customer_name."</td></tr>";
			$TableHtml .= "<tr><td colspan='".$MC."' style='background: none; color: #000;'>".$reporting_field."</td></tr>";
			$TableHtml .= $blank_row;

			$headings = array(ucfirst($_REQUEST['report_type']), 'HTML Page Views', 'Unique User Sessions');
			$headinArray = array_map('ucfirst', $ModuleArray);
			$headings = array_merge($headings, $headinArray);
			$headtd="";
			foreach($headings as $heading){
				$headtd .= "<th>".$heading."</th>";
			}
			$TableHtml .= "<tr>".$headtd."</tr>";
			
			if(@is_array($StatArray)){
				foreach($StatArray as $sk=>$st){
					ksort($st['stats']);
					$stats = $st['stats'];
					
					if($_REQUEST['report_type']=='date'){
						$timest = strtotime($sk);
						$DateNewFormat = date('M d, Y',$timest);
					} else {
						$myear = ($st['year'] !='' ? ", ".$st['year'] : "");
						$DateNewFormat = substr($MonthArray[$sk], 0,3).$myear;
					}
					
					if(array_key_exists($sk, $StatSessArray)){
						$SessionCount = $StatSessArray[$sk];
						$TotalSessionCount += $StatSessArray[$sk];
					} else {
						$SessionCount = "";
					}
					
					$gotal['total']['html_views'] += $StatArray[$sk]['html_views'];
					
					$ModData = array();
					foreach($ModuleArray as $md){
						$gotal['total'][$md] += $StatArray[$sk]['stats'][$md];
						$ModData[] = $stats[$md];
					}
					
					
					$csv_array_data = array($DateNewFormat,$st['html_views'],$SessionCount);
					$csv_array_data = array_merge($csv_array_data, $ModData);					
					$viewsessiontd="";
					foreach($csv_array_data as $cad){
						$viewsessiontd .= "<td>".$cad."</td>";
					}
					$TableHtml .= "<tr>".$viewsessiontd."</tr>";
				}
				$gtotal_array = array('G.Total', $gotal['total']['html_views'], $TotalSessionCount);
				$gtotalmod = array();
				foreach($ModuleArray as $md){
					$gtotalmod[] = $gotal['total'][$md];
				}
				$gtotal = array_merge($gtotal_array, $gtotalmod);
				fputcsv($handle, $gtotal);
				$gtotaltd="";
				foreach($gtotal as $tot){
					$gtotaltd .= "<td class='bold'>".$tot."</td>";
				}
				$TableHtml .= "<tr>".$gtotaltd."</tr>";
			}
			$TableHtml .= "</table>";
			
				/*$result['table_html'] = stripslashes($TableHtml);
				$result['status'] = 'success';
				echo json_encode($result);*/
				echo $TableHtml;
				die();	
			
	
			break;
			case 'get_csv':
				$Subscriber = $_REQUEST['subscriber'];
				$Customers = $_REQUEST['customers'];
				/*get report for date range */
				if($_REQUEST['report_type']=='date'){
					$StartDate = $_REQUEST['sStartDate'];
					$EndDate = $_REQUEST['sEndDate'];
					/*get stats summary script */
					$GetStatsReport = $mysqli->query("CALL AnatGetDailyStatSummary_S('".$Customers."','".$StartDate."','".$EndDate."')");
					while($dst = $GetStatsReport->fetch_object()){
						$StatArray[$dst->dailystatdate]['stats'][$dst->module] = $dst->pagecount;
						@$StatArray[$dst->dailystatdate]['html_views'] += $dst->pagecount;
					}
					
					
					$GetStatsReport->close();
					$mysqli->next_result();
					
					/*get session summary summary script */
					$GetSessionReport = $mysqli->query("CALL AnatGetDailySessionSummary_S('".$Customers."','".$StartDate."','".$EndDate."')");
					while($dsst = $GetSessionReport->fetch_object()){
						$StatSessArray[$dsst->dailysessiondate] = $dsst->sessioncount;
					}
					$GetSessionReport->close();
					$mysqli->next_result();
				}
				if($_REQUEST['report_type']=='month'){
					/*get stats summary script monthly */
					$MonthFrom = $_REQUEST['month_from'];
					$YearFrom = $_REQUEST['year_from'];
					$MonthTo = $_REQUEST['month_to'];
					$YearTo = $_REQUEST['year_to'];
					
					$GetStatsReportMonthly = $mysqli->query("CALL AnatGetMonthlyStatSummary_S('".$Customers."',".$MonthFrom.",".$YearFrom.",".$MonthTo.",".$YearTo.")");
					while($dst = $GetStatsReportMonthly->fetch_object()){
						$StatArray[$dst->month]['stats'][$dst->module] = $dst->pagecount;
						@$StatArray[$dst->month]['html_views'] += $dst->pagecount;
						$StatArray[$dst->month]['year'] = $dst->year;
					}

					$GetStatsReportMonthly->close();
					$mysqli->next_result();

					/*get session summary summary script */
					$GetSessionReportMonthly = $mysqli->query("CALL AnatGetMonthlySessionSummary_S('".$Customers."',".$MonthFrom.",".$YearFrom.",".$MonthTo.",".$YearTo.")");
					while($dsst = $GetSessionReportMonthly->fetch_object()){
						$StatSessArray[$dsst->month] = $dsst->sessioncount;
					}
					$GetSessionReportMonthly->close();
					$mysqli->next_result();
				}
	/* download csv programming */
	setlocale(LC_MONETARY, 'en_US');
	$rt = $_REQUEST['report_type'];
	
		$filename = "anat-".$rt."-".$Subscriber."-".time().".csv";
		$file_path = UPLOADDIR."/csv/".$filename;
		$file_url = UPLOADURL."/csv/".$filename;
		if(count($StatArray)>0){
			
		$files = "anat-$rt-$Subscriber-*.csv";		

		$filenamer = UPLOADDIR.'/csv/'.$files;		
		$filezise = glob("$filenamer");
		if(!empty($filezise)){
			$out = array_map('unlink', glob("$filenamer")); 
		}
			
			$handle = fopen( $file_path , 'w+');
			
			/* get subscriber name */
			$CustGrp = $mysqli->query("CALL AnatGetCustomerGroup_S");
			while($cg = $CustGrp->fetch_object()){
				$CustGrpArray[$cg->customergroupid] = $cg->customergroup;
			}			
			$CustGrp->close();
            $mysqli->next_result();
			
			
			/* get module name */
			$ModuleRes = $mysqli->query("CALL AnatGetModuleName_S");
			while($md = $ModuleRes->fetch_object()){
				$ModuleArray[] = $md->Module;
			}
			$ModuleRes->close();
            $mysqli->next_result();
			
			$MC = count($ModuleArray);
			$blank_row = array_fill(0,$MC-1,'');
			fputcsv($handle, $blank_row);
			$customer_name = array_merge(array($CustGrpArray[$Subscriber],'','',''), $blank_row);
			if($_REQUEST['report_type']=='month'){ 
				$report_type_text = "Monthly Report - ";
				$report_period = substr($MonthArray[$_REQUEST['month_from']],0,3)."-".$_REQUEST['year_from']." to ".substr($MonthArray[$_REQUEST['month_to']],0,3)."-".$_REQUEST['year_to'];
			}
			if($_REQUEST['report_type']=='date'){ 
				$report_type_text = "Daily Report - "; 
				$report_period = $_REQUEST['sStartDate']." to ".$_REQUEST['sEndDate'];
			}
			$reporting_field = array_merge(array($report_type_text, $report_period,'',''), $blank_row);
			fputcsv($handle, $customer_name);
			fputcsv($handle, $reporting_field);
			fputcsv($handle, $blank_row);
			$headings = array(ucfirst($_REQUEST['report_type']), 'HTML Page Views', 'Unique User Sessions');
			$headinArray = array_map('ucfirst', $ModuleArray);
			$headings = array_merge($headings, $headinArray);
			fputcsv($handle, $headings);
			
			if(@is_array($StatArray)){
				foreach($StatArray as $sk=>$st){
					ksort($st['stats']);
					$stats = $st['stats'];
					
					if($_REQUEST['report_type']=='date'){
						$timest = strtotime($sk);
						$DateNewFormat = date('M d, Y',$timest);
					} else {
						$myear = ($st['year'] !='' ? ", ".$st['year'] : "");
						$DateNewFormat = substr($MonthArray[$sk], 0,3).$myear;
					}
					
					if(array_key_exists($sk, $StatSessArray)){
						$SessionCount = $StatSessArray[$sk];
						$TotalSessionCount += $StatSessArray[$sk];
					} else {
						$SessionCount = "";
					}
					
					$gotal['total']['html_views'] += $StatArray[$sk]['html_views'];
					
					$ModData = array();
					foreach($ModuleArray as $md){
						$gotal['total'][$md] += $StatArray[$sk]['stats'][$md];
						$ModData[] = $stats[$md];
					}
					
					
					$csv_array_data = array($DateNewFormat,$st['html_views'],$SessionCount);
					$csv_array_data = array_merge($csv_array_data, $ModData);
					fputcsv($handle, $csv_array_data);
				}
				$gtotal_array = array('G.Total', $gotal['total']['html_views'], $TotalSessionCount);
				$gtotalmod = array();
				foreach($ModuleArray as $md){
					$gtotalmod[] = $gotal['total'][$md];
				}
				$gtotal = array_merge($gtotal_array, $gtotalmod);
				fputcsv($handle, $gtotal);
			}
			fclose($handle);
			
			if( file_exists($file_path) ){
				$result['file_path'] = $file_url;
				$result['status'] = 'true';
				$result['msg'] = "CSV File generated successfully!";
				$result['error'] =  '';
				echo json_encode($result);
				exit();		
			} else {
				$result['status'] = 'false';
				$result['msg'] = "File Could not be created OR permission denied!";
				$result['error'] =  '';
				echo json_encode($result);
				exit();
			}	
		}
			break;
		case 'get_customers_by_group':
			$group_id = $_POST['group_id'];
			//Amit Sharma change condition 
			if($group_id!=''){
				$EdCustGrp = $mysqli->query("CALL AnatGetGroupCustomer_S(".$group_id.", 'R')");
				$CustHTML = "";
				$ic=0;
				while($ecg = $EdCustGrp->fetch_object()){
					$CustHTML .= "<div class='cust-box'>";
					$CustHTML .= "<div class='left-text'>".$ecg->customername."</div>";
					$CustHTML .= "<div class='right-check'><input type='checkbox' class='cust-chk' id='cust-".$ecg->id."' name='customers[]' value='".$ecg->id."'></div></div>";
					$ic++;
				}
				$EdCustGrp->close();
				$mysqli->next_result();
				if($ic > 0){
					$result['status'] = "success";
					$result['msg'] = $ic." Record Found";
					$result['cust_html'] = $CustHTML;
				} else {
					$result['status'] = "fail";
					$result['msg'] = "No Record Found";
				}
			} else {
				$result['status'] = "fail";
				$result['error'] = "Invalid Group";
			}
			echo json_encode($result);
			exit();
			break; 
        case 'default':
            include "view/login.html";
            break;  
    }
    
    
?>
