#/bin/bash
DB_DUMP_DIR=/opt/test                                                                          # Log file home location path
LogTimestamp=/var/log/amit.txt                                                                 # Location of Timestamp file
LogAnalysisPath=$(cat /var/path.txt)                                                           # Path of that central file where final analysis file location is stored.
LogErrorPath=/var/log/logerror.txt                                                             # Location of Eroor log file
KEEP=7                                                                                         # Max files to be kept
if [ -d "$DB_DUMP_DIR" ]
then
find ${DB_DUMP_DIR} -type f -printf '%T@ %p\n' | sort -n > $LogTimestamp                       # Sorted Timestamp information of oldest file
find ${DB_DUMP_DIR} -type f -printf '%T@ %p\n' | sort -n  | head --lines=-${KEEP} | while read date filename ;
do
find $filename -mtime +8 -delete                                                               # file to be kept and deleted in number of days
done
if [ $? = 0 ]
then
a=$(echo "file deleted on $(date +%Y-%m-%d-%T)")
DD=`cat $LogTimestamp |head -1 | awk '{print $1}'`
b=$(echo "Last folder deleted was $(date -d @$DD)")
c=$(echo $?)
d="$a $b $c"
echo $d >> $LogAnalysisPath                                                                   # location where Log Analysis Result
else
echo "There is some error while running the script $(date +%Y-%m-%d))" >> $LogErrorPath
fi
else
echo "path not exist $(date +%Y-%m-%d-%T))" >> $LogErrorPath
fi
