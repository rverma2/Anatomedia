DELIMITER $$

USE `AnatomediaSRT`$$

DROP PROCEDURE IF EXISTS `AnatGetMonthlyStatSummary_S`$$

CREATE DEFINER=`srtuser`@`localhost` PROCEDURE `AnatGetMonthlyStatSummary_S`(custid VARCHAR(50),MonthFrom INT(2), yearFrom INT(4),MonthTo INT(2), yearTo INT(4))

BEGIN

DECLARE strtdt VARCHAR(20);

DECLARE enddt VARCHAR(20);

SET strtdt=CONCAT(01,",",MonthFrom,",",yearFrom);

SET enddt=CONCAT(01,",",MonthTo,",",yearTo);

-- SELECT MONTH(dailystatdate) as 'month',year(dailystatdate) as 'year',module,SUM(pagecount) as 'pagecount' FROM AnatomediaSRT.Anatdailystatsummary WHERE customerid = custid AND dailystatdate BETWEEN STR_TO_DATE(strtdt,'%d,%m,%Y') AND LAST_DAY(STR_TO_DATE(enddt,'%d,%m,%Y')) GROUP BY module,MONTH(dailystatdate),YEAR(dailystatdate);  #### old logid #####
SELECT MONTHNAME AS 'month',YEAR AS 'year',module,SUM(pagecount) AS 'pagecount' FROM AnatomediaSRT.Anatmonthlystatsummary WHERE customerid IN(custid) AND MONTHNAME BETWEEN MonthFrom AND MonthTo AND YEAR BETWEEN yearFrom AND yearTo GROUP BY module,MONTHNAME,YEAR;
END$$

DELIMITER ;