DELIMITER $$

USE `AnatomediaSRT`$$

DROP PROCEDURE IF EXISTS `AnatGetUnassignedCustomer_S`$$

CREATE DEFINER=`srtuser`@`localhost` PROCEDURE `AnatGetUnassignedCustomer_S`()
SELECT DISTINCT c.id,c.customername FROM AnatomediaSRT.AnatCustomer c LEFT JOIN AnatomediaSRT.CustomerGroupCustomerMap cm ON c.id = cm.anatcustomerid WHERE cm.anatcustomerid IS NULL ORDER BY c.customername$$

DELIMITER ;