DELIMITER $$

USE `AnatomediaSRT`$$

DROP PROCEDURE IF EXISTS `AnatGetDailyStatSummary_S`$$

CREATE DEFINER=`srtuser`@`localhost` PROCEDURE `AnatGetDailyStatSummary_S`(custid VARCHAR(50),startdate DATETIME, enddate DATETIME)
SELECT dailystatdate,module,pagecount  FROM AnatomediaSRT.Anatdailystatsummary  WHERE customerid IN(custid )AND dailystatdate  BETWEEN startdate AND enddate GROUP BY module ORDER BY dailystatdate, customername$$

DELIMITER ;