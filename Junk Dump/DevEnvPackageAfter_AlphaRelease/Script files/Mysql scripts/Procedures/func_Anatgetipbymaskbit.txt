DELIMITER $$

USE `AnatomediaSRT`$$

DROP FUNCTION IF EXISTS `Anatgetipbymaskbit`$$

CREATE DEFINER=`srtuser`@`localhost` FUNCTION `Anatgetipbymaskbit`(`ipadd` VARCHAR(20),`maskbit` INT) RETURNS VARCHAR(50) CHARSET utf8
    DETERMINISTIC
    COMMENT 'Converts a CIDR suffix (integer between 0 and 32) to an IPv4 netmask in dotted decimal notation'
BEGIN
    DECLARE `numaddrs`  INT UNSIGNED;
    DECLARE `ipaddr`    INT UNSIGNED;
    DECLARE `netmask`   INT UNSIGNED;
    DECLARE `wildcard`  INT UNSIGNED;
    DECLARE `network`   INT UNSIGNED;
    DECLARE `broadcast` INT UNSIGNED;
    DECLARE `numhosts`  INT UNSIGNED;
    DECLARE iplist VARCHAR(50);
    SET `ipaddr` = INET_ATON(`ipadd`);
    IF (`ipaddr` IS NULL) OR (`maskbit` < 1) OR (`maskbit` > 30)
    THEN
        RETURN NULL;
    ELSE
        SET `numaddrs`  = POW( 2, (32-`maskbit`) );
        SET `numhosts`  = `numaddrs` - 2;
        SET `netmask`   = 0xFFFFFFFF - (`numaddrs` - 1);
        SET `wildcard`  = 0xFFFFFFFF & (~`netmask`);
        SET `network`   = `ipaddr` & `netmask`;
        SET `broadcast` = `ipaddr` | `wildcard`;
        
        SET iplist = CONCAT_WS('-',INET_NTOA(`network`),INET_NTOA(`broadcast`));
        RETURN iplist;
    END IF;
END$$

DELIMITER ;