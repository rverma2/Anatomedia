DELIMITER $$

USE `AnatomediaSRT`$$

DROP PROCEDURE IF EXISTS `AnatMonthlyRollup_I`$$

CREATE DEFINER=`srtuser`@`localhost` PROCEDURE `AnatMonthlyRollup_I`(executionid INT,curfiledate DATETIME)
BEGIN
DECLARE counter INT;
DECLARE cur_statsrollup CURSOR FOR SELECT customerid,module,curmonth,curyear,frmdailycount FROM (
 SELECT d.customerid,d.module,MONTH(curfiledate) AS curmonth,YEAR(curfiledate) AS curyear,SUM(d.pagecount) frmdailycount,m.pagecount monthcount FROM AnatomediaSRT.Anatdailystatsummary d LEFT JOIN AnatomediaSRT.Anatmonthlystatsummary m ON d.customerid = m.customerid AND d.module = m.module WHERE MONTH(d.dailystatdate) = MONTH(curfiledate) AND m.customerid IS NOT NULL GROUP BY d.customerid,d.module) a WHERE frmdailycount <> monthcount;
 
DECLARE cur_sessionrollup CURSOR FOR SELECT customerid,curmonth,curyear,sessioncount FROM (
 SELECT d.customerid,MONTH(curfiledate) AS curmonth,YEAR(curfiledate) AS curyear,SUM(d.sessioncount) AS sessioncount,m.sessioncount AS monthsessioncount FROM AnatomediaSRT.Anatdailysessionsummaryonget d LEFT JOIN AnatomediaSRT.Anatmonthlysessionsummary m ON d.customerid = m.customerid WHERE MONTH(d.dailysessiondate) = MONTH(curfiledate) AND m.customerid IS NOT NULL GROUP BY d.customerid) a WHERE sessioncount <> monthsessioncount;
 
DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
 BEGIN
     ROLLBACK;
    -- EXIT PROCEDURE;
END;
UPDATE AnatomediaSRT.Anatexecutionhistory SET currentprocess="monthly rollup"  WHERE executionhistoryid = executionid;
SELECT COUNT(1) INTO counter FROM AnatomediaSRT.Anatmonthlystatsummary WHERE MONTHNAME = MONTH(curfiledate);
START TRANSACTION;
IF counter > 0
THEN
DELETE FROM AnatomediaSRT.Anatmonthlystatsummary WHERE MONTHNAME = MONTH(curfiledate);
DELETE FROM AnatomediaSRT.Anatmonthlysessionsummary WHERE MONTHNAME = MONTH(curfiledate);
END IF;
 
 
 
 
OPEN cur_statsrollup;
BEGIN
DECLARE done INT DEFAULT FALSE;
DECLARE custid INT;
DECLARE moduleinfo VARCHAR(15);
DECLARE curmonth INT;
DECLARE curyear INT;
DECLARE pcount INT;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
     INSERT INTO AnatomediaSRT.Anatmonthlystatsummary(customerid,module,MONTHNAME,YEAR,pagecount)
SELECT d.customerid,d.module,MONTH(curfiledate),YEAR(curfiledate),SUM(d.pagecount) FROM AnatomediaSRT.Anatdailystatsummary d LEFT JOIN AnatomediaSRT.Anatmonthlystatsummary m ON d.customerid = m.customerid AND d.module = m.module WHERE MONTH(d.dailystatdate) = MONTH(curfiledate) AND m.customerid IS NULL GROUP BY d.customerid,d.module;
  read_loop: LOOP
    FETCH cur_statsrollup INTO custid,moduleinfo,curmonth,curyear,pcount;
     IF done THEN
      LEAVE read_loop;
    END IF;
	UPDATE AnatomediaSRT.Anatmonthlystatsummary SET pagecount= pcount WHERE customerid = custid AND module = moduleinfo;    
     END LOOP read_loop;
     END;
  CLOSE cur_statsrollup;
OPEN cur_sessionrollup;
BEGIN
DECLARE done INT DEFAULT FALSE;
DECLARE custid INT;
DECLARE curmonth INT;
DECLARE curyear INT;
DECLARE pcount INT;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
INSERT INTO AnatomediaSRT.Anatmonthlysessionsummary(customerid,MONTHNAME,YEAR,sessioncount)
SELECT d.customerid,MONTH(curfiledate),YEAR(curfiledate) ,SUM(d.sessioncount) FROM AnatomediaSRT.Anatdailysessionsummaryonget d LEFT JOIN AnatomediaSRT.Anatmonthlysessionsummary m ON d.customerid = m.customerid WHERE MONTH(d.dailysessiondate) = MONTH(curfiledate) AND m.customerid IS NULL GROUP BY d.customerid;
  read_loop: LOOP
    FETCH cur_sessionrollup INTO custid,curmonth,curyear,pcount;
     IF done THEN
      LEAVE read_loop;
    END IF;
	UPDATE AnatomediaSRT.Anatmonthlysessionsummary SET sessioncount = pcount WHERE customerid = custid;    
     END LOOP read_loop;
     END;
  CLOSE cur_sessionrollup;
  
  
UPDATE AnatomediaSRT.Anatexecutionhistory SET executionstatus= "s", executionenddt = CURRENT_TIMESTAMP  WHERE executionhistoryid=executionid;
COMMIT;
			
END$$

DELIMITER ;