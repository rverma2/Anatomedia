CREATE DATABASE `AnatomediaSRT` /*!40100 DEFAULT CHARACTER SET latin1 */

GRANT ALL PRIVILEGES ON AnatomediaSRT.* TO 'srtuser'@'localhost' IDENTIFIED BY 'anatlog';
GRANT USAGE,EXECUTE ON AnatomediaSRT.* TO 'srtreportuser'@'localhost' IDENTIFIED BY 'tech2c@st';

FLUSH PRIVILEGES;

use AnatomediaSRT;

CREATE TABLE `Anatcontrolfileinfo` (
  `controlfileinfoid` INT(11) NOT NULL AUTO_INCREMENT,
  `executionhistoryid` INT(11) NOT NULL,
  `controlfilename` VARCHAR(500) NOT NULL,
  `logfileprocessed` VARCHAR(500) NOT NULL,
  `rowsread` INT(11) NOT NULL,
  `rowswritten` INT(11) NOT NULL,
  `jobenddate` DATETIME DEFAULT NULL,
  `insertiondt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `controlfilestatus` CHAR(1) NOT NULL,
  `comments` VARCHAR(4000) DEFAULT NULL,
  PRIMARY KEY (`controlfileinfoid`)
) ENGINE=INNODB  DEFAULT CHARSET=utf8;



CREATE TABLE `AnatCustomer` (
  `IPAddress` VARCHAR(20) DEFAULT NULL,
  `id` INT(4) NOT NULL AUTO_INCREMENT,
  `CustomerName` VARCHAR(50) DEFAULT NULL,
  `CustomerType` VARCHAR(10) DEFAULT NULL,
  `Customerid` INT(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Customer` (`CustomerName`,`Customerid`),
  KEY `AnatCustomer_customerid_Ipaddress_idx` (`IPAddress`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE `Anatdailyrawlogs` (
  `dailyrawlogsid` INT(10) NOT NULL AUTO_INCREMENT,
  `rawlogdata` VARCHAR(8000) NOT NULL,
  PRIMARY KEY (`dailyrawlogsid`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;



CREATE TABLE `Anatdailysessionsummary` (
  `dailysessionsummaryid` INT(10) NOT NULL AUTO_INCREMENT,
  `customerid` INT(50) NOT NULL,
  `dailysessiondate` DATETIME NOT NULL,
  `sessioncount` INT(8) NOT NULL,
  `dailysessioncreatedate` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `CustomerName` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`dailysessionsummaryid`)
) ENGINE=INNODB  DEFAULT CHARSET=utf8;


CREATE TABLE `Anatdailysessionsummaryonget` (
  `dailysessionsummaryid` INT(10) NOT NULL AUTO_INCREMENT,
  `customerid` INT(50) NOT NULL,
  `dailysessiondate` DATETIME NOT NULL,
  `sessioncount` INT(8) NOT NULL,
  `dailysessioncreatedate` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `CustomerName` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`dailysessionsummaryid`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;


CREATE TABLE `Anatdailystatsummary` (
  `dailystatsummaryid` INT(10) NOT NULL AUTO_INCREMENT,
  `customerid` INT(50) NOT NULL,
  `module` VARCHAR(50) NOT NULL,
  `dailystatdate` DATETIME NOT NULL,
  `pagecount` INT(8) NOT NULL,
  `dailystatcreatedate` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `CustomerName` VARCHAR(100) NOT NULL,
  `module_id` SMALLINT(6) DEFAULT NULL,
  PRIMARY KEY (`dailystatsummaryid`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;



CREATE TABLE `Anatexecutionhistory` (
  `executionhistoryid` INT(11) NOT NULL AUTO_INCREMENT,
  `FileProcessed` VARCHAR(500) DEFAULT NULL,
  `currentprocess` VARCHAR(30) NOT NULL,
  `executionstartdt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `executionenddt` TIMESTAMP,
  `executionstatus` CHAR(1) NOT NULL DEFAULT 'I',
  `comments` VARCHAR(4000) DEFAULT NULL,
  PRIMARY KEY (`executionhistoryid`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;



CREATE TABLE `Anatmodule` (
  `moduleid` SMALLINT(6) NOT NULL AUTO_INCREMENT,
  `modulename` VARCHAR(50) DEFAULT NULL,
  `moduledesc` VARCHAR(50) DEFAULT NULL,
  PRIMARY KEY (`moduleid`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;


CREATE TABLE `Anatmonthlysessionsummary` (
  `monthlysessionsummaryid` INT(10) NOT NULL AUTO_INCREMENT,
  `customerid` VARCHAR(50) NOT NULL,
  `monthname` VARCHAR(30) NOT NULL,
  `sessioncount` INT(8) NOT NULL,
  `monthlysessioncreatedate` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`monthlysessionsummaryid`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE `Anatmonthlystatsummary` (
  `monthlystatsummaryid` INT(10) NOT NULL AUTO_INCREMENT,
  `client` VARCHAR(50) NOT NULL,
  `module` VARCHAR(50) NOT NULL,
  `monthname` VARCHAR(30) NOT NULL,
  `pagecount` INT(8) NOT NULL,
  `monthlystatcreatedate` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`monthlystatsummaryid`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;



CREATE TABLE `Anatparsedlogdata` (
  `parsedlogdataid` INT(11) NOT NULL AUTO_INCREMENT,
  `ipaddress` VARCHAR(50) DEFAULT NULL,
  `logdate` DATETIME DEFAULT NULL,
  `requeststring` VARCHAR(4000) DEFAULT NULL,
  `calledfrom` VARCHAR(1000) DEFAULT NULL,
  `webbrowser` VARCHAR(1000) DEFAULT NULL,
  `customerid` INT(10) DEFAULT NULL,
  `code` VARCHAR(1000) DEFAULT NULL,
  `size` VARCHAR(100) DEFAULT NULL,
  `parsedstatus` CHAR(1) DEFAULT NULL,
  `customername` VARCHAR(50) DEFAULT NULL,
  `sessionid` VARCHAR(100) DEFAULT NULL,
  PRIMARY KEY (`parsedlogdataid`),
  KEY `idx_ipaddress` (`ipaddress`),
  KEY `IDX_AnatParcedLog_ipaddress` (`ipaddress`),
  KEY `code` (`requeststring`(255),`code`(255))
) ENGINE=INNODB DEFAULT CHARSET=utf8;



CREATE TABLE `Anatprocesstime` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `FileProcessed` VARCHAR(500) DEFAULT NULL,
  `currentprocess` VARCHAR(30) NOT NULL,
  `executionstartdt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `executionstatus` CHAR(1) NOT NULL DEFAULT 's',
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;