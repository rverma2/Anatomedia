The Deployment Steps are as follows:
1.Create following Directory Structure on Environment

/srt/data,/srt/log,/srt/scripts

2 Place Clickstream data in /srt/data/

3 place shell scripts,From folder "TestEnvPackage\Code\Script files",Anatdailylogextraction.sh, Anatlogmaintenance.sh, AnatTransformationLoading.sh in /srt/scripts/
4.a) Grant execute permission on all files placed step 3 through chmod +x *.*.
b) sed -i -e 's/\r$//' <scriptname> for all scripts mentioned in step 3.

5. Create DB AnatomediaSRT & tables

Run the Commands from TestEnvPackage\Code\Script files\Mysql\sql_db_creation_cmd.txt

6.Create Stored proc

Copy TestEnvPackage\Code\Script files\Mysql\Procedures folder to /srt/scripts.

Grant execute permission on CreateProc.sh files in folder /srt/scripts/Procedures

Execute CreateProc.sh

7.Populate tables AnatCustomer & AnatModule through population Script.

Copy folder TestEnvPackage\Code\Script files\Mysql\DataPopulationSql in /srt/scripts

Grant execute permission on CustomerLoad.sh files in folder /srt/scripts/DataPopulationSql

Execute CustomerLoad.sh in folder /srt/scripts/DataPopulationSql

Run anat_module.sql in Mysql DB AnatomediaSRT.

8.Place Cron Job Scripts from "TestEnvPackage\Code\CronJobs" CronAnatdailylogextraction.txt & CronAnatTransformationLoading.txt to Environment

9.Place PHP Code from "TestEnvPackage\Code\PHP Code" to Environment

----------------------------------------------------------------------
Steps in Re deployment case before Step 1.


1.Cleaning all files from /srt/data,/srt/log,/srt/scripts
2 Drop DB AnatomediaSRT