DELIMITER $$

USE `AnatomediaSRT`$$

DROP PROCEDURE IF EXISTS `AnatGetMonthlySessionSummary_S`$$

CREATE DEFINER=`srtuser`@`localhost` PROCEDURE `AnatGetMonthlySessionSummary_S`(custid INT,MonthFrom INT(2), yearFrom INT(4),MonthTo INT(2), yearTo INT(4))
BEGIN
DECLARE strtdt VARCHAR(20);
DECLARE enddt VARCHAR(20);
SET strtdt=CONCAT(01,",",MonthFrom,",",yearFrom);
SET enddt=CONCAT(01,",",MonthTo,",",yearTo);
-- SELECT MONTH(dailysessiondate) as 'month',year(dailysessiondate) as 'year',SUM(sessioncount) as 'sessioncount' FROM AnatomediaSRT.Anatdailysessionsummaryonget WHERE customerid = custid AND dailysessiondate BETWEEN STR_TO_DATE(strtdt,'%d,%m,%Y') AND LAST_DAY(STR_TO_DATE(enddt,'%d,%m,%Y')) GROUP BY MONTH(dailysessiondate), year(dailysessiondate);  ####old logic
SELECT MONTHNAME AS 'month',YEAR AS 'year',SUM(sessioncount) AS 'sessioncount' FROM AnatomediaSRT.Anatmonthlysessionsummary WHERE customerid = custid AND MONTHNAME BETWEEN MonthFrom AND MonthTo AND YEAR BETWEEN yearFrom AND yearTo GROUP BY MONTHNAME, YEAR;
END$$

DELIMITER ;