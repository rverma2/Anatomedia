DELIMITER $$

USE `anatomedia_test`$$

DROP TRIGGER /*!50032 IF EXISTS */ `update_anatcustomer`$$

CREATE
    /*!50017 DEFINER = 'srtuser'@'localhost' */
    TRIGGER `update_anatcustomer` AFTER UPDATE ON `customer` 
    FOR EACH ROW BEGIN
 CALL AnatomediaSRT.Anatimmediatecustomersync(new.id,"U");
    END;
$$

DELIMITER ;