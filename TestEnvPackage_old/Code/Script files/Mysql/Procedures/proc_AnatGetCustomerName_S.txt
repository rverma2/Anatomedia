DELIMITER $$

USE `AnatomediaSRT`$$

DROP PROCEDURE IF EXISTS `AnatGetCustomerName_S`$$

CREATE DEFINER=`srtuser`@`localhost` PROCEDURE `AnatGetCustomerName_S`()
SELECT DISTINCT CustomerId,CustomerName FROM AnatomediaSRT.AnatCustomer WHERE customername NOT LIKE '%.%' ORDER BY CustomerName$$

DELIMITER ;