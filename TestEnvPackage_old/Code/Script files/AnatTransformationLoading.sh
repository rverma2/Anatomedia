#!/bin/bash
curdate=$(date +"%Y%m%d")
filepath="/srt/data/"
fileinitname="anatlog"
filetoprocess=$filepath$fileinitname
filepathlen=${#filetoprocess}
firstfileprocess=""
lastsuccessfileprocessed=""
curfileprocess=""
curfileprocessdt=""
nextfiletoprocessdt=""
ctrlfilewrittencount=""
executionid=""
lastfileprocessed=$(mysql -usrtuser -panatlog -se "SELECT fileprocessed FROM AnatomediaSRT.Anatexecutionhistory WHERE executionstatus = 's' ORDER BY executionenddt DESC LIMIT 1;")

#if [ $? = 1 ]
#then
#echo "error"
#else
#echo "no error"
#fi

controlfiletask () {
     controlfilename=$filepath"control"$2
     if [ -f $controlfilename ]
     then
	counter=""
	rowsread=`awk '{print $2}' $controlfilename`
	rowswritten=`awk '{print $4}' $controlfilename`
	ctrlstatus=`awk '{print $6}' $controlfilename`
	filename=`awk '{print $8}' $controlfilename`
	jobenddate=`awk '{print $10}' $controlfilename`
    d1=`awk '{print $10}' $controlfilename`
    t1=`awk '{print $11}' $controlfilename`
jobenddate=$d1" "$t1
	
	mysql -usrtuser -panatlog -e "INSERT INTO AnatomediaSRT.Anatprocesstime(fileprocessed,currentprocess,executionstartdt,executionstatus) VALUES('$curfileprocess','rawdata',CURRENT_TIMESTAMP,'s');"
	if mysql -usrtuser -panatlog -e "CALL AnatomediaSRT.Anatcontrolfileinfo_I(1,'$controlfilename',$rowsread,$rowswritten,$ctrlstatus,$filename,$jobenddate,'$curfileprocess',@counter);"	
	then 
	  if [ "$ctrlstatus" = "'s'" ]
	  then
		mysql -usrtuser -panatlog -e "LOAD DATA LOCAL INFILE '$file' INTO TABLE AnatomediaSRT.Anatdailyrawlogs FIELDS ENCLOSED BY '' TERMINATED BY '' (rawlogdata);"
		mysqlwarnings=$(mysql -usrtuser -panatlog -e "show warnings;")
		mysqlerrors=$(mysql -usrtuser -panatlog -e "show errors;")
	mysql -usrtuser -panatlog -e "INSERT INTO AnatomediaSRT.Anatprocesstime(fileprocessed,currentprocess,executionstartdt,executionstatus) VALUES('$curfileprocess','rawdata',CURRENT_TIMESTAMP,'e');"
		if [ "$mysqlwarnings" = "" ] || [ "$mysqlerrors" = "" ]
		then
	mysql -usrtuser -panatlog  -e "INSERT INTO AnatomediaSRT.Anatprocesstime(fileprocessed,currentprocess,executionstartdt,executionstatus) VALUES('$curfileprocess','parsing',CURRENT_TIMESTAMP,'s');"
			executionid=$(mysql -usrtuser -panatlog -se "SELECT MAX(executionhistoryid) FROM AnatomediaSRT.Anatexecutionhistory WHERE fileprocessed = '$curfileprocess';")
			if mysql -usrtuser -panatlog -e "CALL AnatomediaSRT.Anatparsedata_I($executionid,$rowswritten,'$curfileprocessdt',@parsecounter);"
			then
	mysql -usrtuser -panatlog -e "INSERT INTO AnatomediaSRT.Anatprocesstime(fileprocessed,currentprocess,executionstartdt,executionstatus) VALUES('$curfileprocess','parsing',CURRENT_TIMESTAMP,'e');"
	mysql -usrtuser -panatlog -e "INSERT INTO AnatomediaSRT.Anatprocesstime(fileprocessed,currentprocess,executionstartdt,executionstatus) VALUES('$curfileprocess','daily stats',CURRENT_TIMESTAMP,'s');"
				if mysql -usrtuser -panatlog -e "CALL AnatomediaSRT.AnatDailyInfo_I($executionid,'$curfileprocessdt');"
				then
	mysql -usrtuser -panatlog -e "INSERT INTO AnatomediaSRT.Anatprocesstime(fileprocessed,currentprocess,executionstartdt,executionstatus) VALUES('$curfileprocess','daily stats',CURRENT_TIMESTAMP,'e');"
					return 1
				else
					return 0
				fi
			else
				return 0
			fi
		else
			mysql -usrtuser -panatlog -e "update AnatomediaSRT.Anatexecutionhistory set executionstatus=\"f\" where fileprocessed='$curfileprocess';"
			return 0
		fi
	  else
		return 0
	  fi
	else
		return 0
	fi
     else
	echo "no file found $controlfilename"
	mysql -usrtuser -panatlog -e "INSERT INTO AnatomediaSRT.Anatexecutionhistory(FileProcessed,currentprocess,comments,executionenddt,executionstatus) VALUES('$curfileprocess','raw_data','no control file found. $controlfilename',CURRENT_TIMESTAMP,'f');"
	return 0
     fi
}



for file in $filepath$fileinitname*
do	
	curfileprocess=$file
	curfileprocessdt=${file:$filepathlen}
	if [ "$lastfileprocessed" = ""  ]
	then 
		if  [ ${file:$filepathlen} -lt $curdate ]
		then
			if [[ "$firstfileprocess" = "" ]] || [[ "$nextfiletoprocessdt" = ${file:$filepathlen} ]]
			then
				controlfiletask 1 ${file:$filepathlen}
				returnval=$?
				[ $returnval = 0 ] && break
				
				echo "file processed ${file:$filepathlen}"

				firstfileprocess=$file
				nextfiletoprocessdt=$(date --date="${file:$filepathlen} + 1 day" +"%Y%m%d")
			else
				break
			fi
		fi
	else
		#echo $lastfileprocessed
		if [ ${file:$filepathlen} -le ${lastfileprocessed:$filepathlen} ]; then
		continue
		fi


		if [ "$nextfiletoprocessdt" = "" ]
                then
                        nextfiletoprocessdt=$(date --date="${lastfileprocessed:$filepathlen} + 1 day" +"%Y%m%d")
                else
                        nextfiletoprocessdt=$(date --date="$nextfiletoprocessdt + 1 day" +"%Y%m%d")
                fi


		if [[ ${file:$filepathlen} -gt ${lastfileprocessed:$filepathlen} ]]  && [[ ${file:$filepathlen} -lt $curdate ]]
		then
			if [ "$nextfiletoprocessdt" = ${file:$filepathlen} ]
                        then
				controlfiletask 1 ${file:$filepathlen}
                                returnval=$?
                                [ $returnval = 0 ] && break

				echo "file processed $nextfiletoprocessdt"
			else
				break
			fi
		else
			break
		fi
	fi
done
