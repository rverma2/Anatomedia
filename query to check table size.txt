SELECT 
    table_name AS `Table`, 
    ROUND(((data_length + index_length) / 1024 / 1024), 2) `Size in MB`,CURDATE() 
FROM information_schema.TABLES 
WHERE table_schema = 'AnatomediaSRT'
    