DELIMITER $$

USE `AnatomediaSRT`$$

DROP PROCEDURE IF EXISTS `AnatGetModuleName_S`$$

CREATE DEFINER=`srtuser`@`localhost` PROCEDURE `AnatGetModuleName_S`()
SELECT Modulename AS Module,moduleid FROM AnatomediaSRT.Anatmodule ORDER BY Modulename$$

DELIMITER ;