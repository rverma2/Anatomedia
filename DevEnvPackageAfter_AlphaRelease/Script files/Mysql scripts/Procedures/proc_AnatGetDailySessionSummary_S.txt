DELIMITER $$

USE `AnatomediaSRT`$$

DROP PROCEDURE IF EXISTS `AnatGetDailySessionSummary_S`$$

CREATE DEFINER=`srtuser`@`localhost` PROCEDURE `AnatGetDailySessionSummary_S`(custid INT,startdate DATETIME, enddate DATETIME)
SELECT dailysessiondate,sessioncount FROM AnatomediaSRT.Anatdailysessionsummaryonget WHERE customerid = custid AND dailysessiondate BETWEEN startdate AND enddate$$

DELIMITER ;